import mongooseLib from 'mongoose';
import Categories from './seeders/categories.seeder';
import Users from './seeders/users.seeder';
import Languages from './seeders/languages.seeder';
import Countries from './seeders/countries.seeder';
import Roles from './seeders/roles.seeder';
import config from 'config';

mongooseLib.Promise = global.Promise;

// Export the mongoose lib
export const mongoose = mongooseLib;

// Export the mongodb url
export const mongoURL =
  process.env.NODE_ENV === 'production'
    ? process.env.MONGO_URI
    : config.mongo.host || 'mongodb://localhost:27017/dev';

/*
  Seeders List
  ------
  order is important
*/
export const seedersList = {
  Roles,
  Categories,
  Users,
  Languages,
  Countries
};
