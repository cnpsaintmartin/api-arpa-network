import { Seeder } from 'mongoose-data-seed';
const Role = require('../server/models/role');
const User = require('../server/models/user');

const data = [
  {
    name: 'Professional',
    slug: 'professional'
  },
  {
    name: 'Moderator',
    slug: 'moderator'
  },
  {
    name: 'Committee member',
    slug: 'committee_member'
  },
  {
    name: 'Native committee member',
    slug: 'native_committee_member'
  },
  {
    name: 'Administrator',
    slug: 'admin'
  },
  {
    name: 'Super administrator',
    slug: 'super_admin'
  }
];

class RolesSeeder extends Seeder {
  async shouldRun() {
    return Role.countDocuments()
      .exec()
      .then(count => count === 0);
  }

  async run() {
    await Role.create(data);
    const isProduction = process.env.NODE_ENV === 'production';
    if (!isProduction) {
      const roleProfessional = await Role.findOne({
        slug: 'professional'
      }).lean();
      const roleSuperAdmin = await Role.findOne({ slug: 'super_admin' }).lean();
      const roleAdmin = await Role.findOne({ slug: 'admin' }).lean();
      const tmp = {
        firstname: 'bar',
        lastname: 'foo',
        email: 'admin@admin.com',
        password: 'admin',
        roles: [roleSuperAdmin._id, roleAdmin._id, roleProfessional]
      };
      await User.create(tmp);
      const roleNatif = await Role.findOne({
        slug: 'native_committee_member'
      }).lean();
      const tmp1 = {
        firstname: 'bar',
        lastname: 'foo',
        email: 'natif@natif.com',
        password: 'natif',
        roles: [roleNatif._id, roleProfessional]
      };
      await User.create(tmp1);
      const roleCommittee = await Role.findOne({
        slug: 'committee_member'
      }).lean();
      const tmp2 = {
        firstname: 'bar',
        lastname: 'foo',
        email: 'committee@committee.com',
        password: 'committee',
        roles: [roleCommittee._id, roleProfessional]
      };
      await User.create(tmp2);
    }
  }
}

export default RolesSeeder;
