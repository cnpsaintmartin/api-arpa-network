import { Seeder } from 'mongoose-data-seed';
const User = require('../server/models/user');
const Role = require('../server/models/role');

const data = [
  {
    firstname: 'bar',
    lastname: 'foo',
    email: 'visitor@visitor.com',
    password: 'fizz'
  },
  {
    firstname: 'bar',
    lastname: 'foo',
    email: 'jeanne@jeanne.com',
    password: 'fizz'
  }
];

class UsersSeeder extends Seeder {
  async beforeRun() {
    this.roles = await Role.find({ name: 'Administrator' }).exec();
  }
  async shouldRun() {
    return User.countDocuments()
      .exec()
      .then(count => count === 0);
  }

  async run() {
    if (process.env.NODE_ENV !== 'production') {
      return User.create(data);
    }
  }
  // _generateUsers() {
  //   return Array.apply(null, Array(10)).map(() => {
  //     const randomUser = faker.random.arrayElement(this.users);
  //
  //     const randomTagsCount = faker.random.number({
  //       min: 0,
  //       max: 5,
  //       precision: 1,
  //     });
  //     const randomTags = Array.apply(null, Array(randomTagsCount))
  //       .map(() => faker.random.arrayElement(TAGS))
  //       .join(',');
  //
  //     return {
  //       author: randomUser._id,
  //       title: faker.lorem.words(),
  //       body: faker.lorem.paragraphs(),
  //       tags: randomTags,
  //     };
  //   });
  // }
}

export default UsersSeeder;
