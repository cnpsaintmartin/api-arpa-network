import { Seeder } from 'mongoose-data-seed';
const Category = require('../server/models/category');

const data = [
  {
    name: 'legislative_and_laws',
    isShowInHomePage: true,
    backgroundColor: '#448ef6',
    fontColor: '#fdfce9',
    number: 4
  },
  {
    name: 'organizations',
    isShowInHomePage: true,
    backgroundColor: '#009f9d',
    fontColor: '#cdffeb',
    number: 5
  },
  {
    name: 'good_practices',
    isShowInHomePage: true,
    backgroundColor: '#eea341',
    fontColor: '#f2f2f2',
    number: 2
  },
  {
    name: 'bibliography',
    isShowInHomePage: true,
    backgroundColor: '#ef5a5a',
    fontColor: '#feffdf',
    number: 6
  },
  {
    name: 'socio_demographic_data',
    isShowInHomePage: true,
    backgroundColor: '#5c3b6f',
    fontColor: '#fdbbbb',
    number: 3
  },
  {
    name: 'training',
    isShowInHomePage: true,
    backgroundColor: '#f9ed69',
    fontColor: '#6a2c70',
    number: 7
  },
  {
    name: 'news',
    isShowInHomePage: true,
    backgroundColor: '#ffcd3c',
    fontColor: '#000000',
    number: 1
  }
];

class CategoriesSeeder extends Seeder {
  async shouldRun() {
    return Category.countDocuments()
      .exec()
      .then(count => count === 0);
  }

  async run() {
    return Category.create(data);
  }
}

export default CategoriesSeeder;
