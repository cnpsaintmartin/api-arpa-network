const apiDocV1 = {
  swagger: '2.0',
  basePath: '/api',
  info: {
    title: 'API for ARPA project',
    description: 'API for ARPA project',
    version: '1.0.0'
  },
  securityDefinitions: {
    Bearer: {
      type: 'apiKey',
      in: 'header',
      name: 'Authorization'
    }
  },
  security: [
    {
      Bearer: []
    }
  ],
  definitions: {
    Resource: {
      type: 'object',
      required: ['title', 'idCategory', 'image', 'mainContent'],
      properties: {
        title: {
          type: 'string'
        },
        idCategory: {
          type: 'string'
        },
        tags: {
          type: 'array',
          items: {
            type: 'string'
          }
        },
        mainContent: {
          type: 'string'
        },
        image: {
          type: 'string'
        },
        idLanguage: {
          type: 'string'
        },
        commentIsAuthorize: {
          type: 'boolean',
          default: true
        },
        isDraft: {
          type: 'boolean'
        },
        surveyTitle: {
          type: 'string'
        },
        surveyAnswers: {
          type: 'array',
          items: {
            type: 'string'
          }
        },
        surveyDuration: {
          type: 'integer',
          minimum: 1,
          maximum: 30,
          description: 'survey duration, between 1 and 30 days'
        },
        startDate: {
          type: 'string'
        },
        endDate: {
          type: 'string'
        },
        attachments: {
          type: 'array'
        }
      }
    },
    User: {
      type: 'object',
      required: ['lastname', 'firstname', 'email', 'password'],
      properties: {
        lastname: {
          type: 'string'
        },
        firstname: {
          type: 'string'
        },
        email: {
          type: 'string'
        },
        password: {
          type: 'string'
        },
        isProfessional: {
          type: 'boolean',
          default: false
        },
        professionalDocument: {
          type: 'string'
        },
        isAgreeAppearProfessionalContactBook: {
          type: 'boolean',
          default: false
        },
        isAgreeWithContactRequest: {
          type: 'boolean',
          default: false
        },
        description: {
          type: 'string'
        },
        positionHeld: {
          type: 'string'
        },
        country: {
          type: 'string'
        },
        spokenLanguages: {
          type: 'array',
          items: {
            type: 'string'
          }
        }
      }
    },
    Mail: {
      type: 'object',
      required: ['subject', 'content', 'template'],
      properties: {
        group: {
          type: 'array',
          items: {
            type: 'string'
          }
        },
        subject: {
          type: 'string'
        },
        content: {
          type: 'object'
        },
        template: {
          type: 'string'
        }
      }
    }
  },
  paths: {}
};

export default apiDocV1;
