import logger from '../services/logger/logger';

const mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  uniqueValidator = require('mongoose-unique-validator'),
  mongoosePaginate = require('mongoose-paginate-v2');
/**
 * Tag Schema
 */
const TagSchema = new Schema({
  name: { type: String, required: true, unique: true },
  numberOfUsage: { type: Number, default: 1 }
});

TagSchema.plugin(uniqueValidator);

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
TagSchema.statics = {
  async addUsage(id) {
    const tag = await this.findById(id);
    if (!tag) return 'notFound';
    if (isNaN(tag.numberOfUsage)) {
      tag.numberOfUsage = 1;
    } else {
      tag.numberOfUsage = tag.numberOfUsage + 1;
    }
    try {
      await tag.save();
      return true;
    } catch (e) {
      logger.error(e);
      return 'error';
    }
  },
  async removeUsage(id) {
    const tag = await this.findById(id);
    if (!tag) return 'notFound';
    tag.numberOfUsage = tag.numberOfUsage - 1;
    try {
      await tag.save();
      return true;
    } catch (e) {
      logger.error(e);
      return 'error';
    }
  }
};

/**
 * Statics
 */

/**
 * @typedef Tag
 */
TagSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('Tag', TagSchema);
