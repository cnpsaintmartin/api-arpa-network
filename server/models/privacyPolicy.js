const mongoose = require('mongoose'),
  Schema = mongoose.Schema;
/**
 * PrivacyPolicy Schema
 */
const PrivacyPolicySchema = new Schema({
  content: { type: String, required: true }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */

/**
 * Statics
 */

/**
 * @typedef PrivacyPolicy
 */
module.exports = mongoose.model('PrivacyPolicy', PrivacyPolicySchema);
