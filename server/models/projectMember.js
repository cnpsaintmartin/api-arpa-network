const mongoose = require('mongoose'),
  Schema = mongoose.Schema;
/**
 * ProjectMember Schema
 */
const ProjectMemberSchema = new Schema({
  institution: { type: String, required: true },
  firstname: { type: String, required: true },
  lastname: { type: String, required: true },
  email: { type: String, required: true },
  website: { type: String, required: false }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */

/**
 * Statics
 */

/**
 * @typedef ProjectMember
 */
module.exports = mongoose.model('ProjectMember', ProjectMemberSchema);
