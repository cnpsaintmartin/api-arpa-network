import logger from '../services/logger/logger';

const Promise = require('bluebird'),
  mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  mongoosePaginate = require('mongoose-paginate-v2'),
  slug = require('mongoose-slug-updater');
/**
 * Optional resource schema
 */
import moment from 'moment';

const resourceTmp = new Schema(
  {
    title: { type: String },
    category: {
      type: mongoose.Schema.ObjectId,
      ref: 'Category',
      autopopulate: true
    },
    tags: [{ type: mongoose.Schema.ObjectId, ref: 'Tag', autopopulate: true }],
    shortDescription: { type: String, required: false },
    mainContent: { type: String, required: true },
    image: { type: String, required: true },
    language: { type: String, required: true },
    estimateReadingTime: { type: Number, required: true },
    commentIsAuthorize: { type: Boolean, default: true },
    startDate: {
      type: Date,
      required: false
    },
    endDate: {
      type: Date,
      required: false
    }
  },
  { _id: false }
);

/**
 * Resource Schema
 */
const ResourceSchema = new Schema({
  title: { type: String, required: true },
  slug: { type: String, slug: 'title', unique: true },
  category: {
    type: mongoose.Schema.ObjectId,
    ref: 'Category',
    autopopulate: true
  },
  shortDescription: { type: String, required: false },
  estimateReadingTime: { type: Number, required: true },
  mainContent: { type: String, required: true },
  image: { type: String, required: true },
  language: { type: String, required: true },
  tags: [{ type: mongoose.Schema.ObjectId, ref: 'Tag', autopopulate: true }],
  submittedDate: { type: Date, default: moment.utc() },
  publishedDate: { type: Date, required: false },
  author: { type: mongoose.Schema.ObjectId, ref: 'User' },
  comments: [{ type: mongoose.Schema.ObjectId, ref: 'Comment' }],
  numberOfLikes: { type: Number, defaultValue: 0 },
  commentIsAuthorize: { type: Boolean, default: true },
  isDraft: { type: Boolean, default: false },
  // resourceType -> 1 article / 2 survey / 3 event
  resourceType: { type: Number, enum: [1, 2, 3], default: 1 },
  // resourceStatus -> 1 waiting committee validation / 2 rejected / 3 approved / 4 wainting edit approve / 5 reject after editing
  resourceStatus: { type: Number, enum: [1, 2, 3, 4, 5], default: 1 },
  survey: { type: mongoose.Schema.ObjectId, ref: 'Survey' },
  surveyDuration: Number,
  // need to be 2 before published (3 committee members must be agree to publish the survey)
  surveyIsReadyToBePublished: [{ type: mongoose.Schema.ObjectId, ref: 'User' }],
  attachments: [
    {
      url: { type: String, required: false },
      name: { type: String, required: false },
      required: false
    }
  ],
  startDate: {
    type: Date,
    required: false
  },
  endDate: {
    type: Date,
    required: false
  },
  updatedAt: {
    type: Date,
    default: moment.utc()
  },
  deletedAt: {
    type: Date,
    required: false
  },
  isReportedBy: [{ type: mongoose.Schema.ObjectId, ref: 'User' }],
  resourceTmp: resourceTmp
});

ResourceSchema.pre('save', function(next) {
  this.updatedAt = moment.utc();
  next();
});
/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */

/**
 * Statics
 */
ResourceSchema.statics = {
  async surveyAddOneAgree(id, idUser) {
    const resource = await this.findById(id);
    if (!resource) return 'error';
    let tab = resource.surveyIsReadyToBePublished.map(String);
    const index = tab.indexOf(idUser.toString());
    if (index !== -1) return 'alreadyAccepted';
    if (tab.length === 2) {
      resource.set({ resourceStatus: 3 });
      try {
        await resource.save();
        return 'resourcePublished';
      } catch (e) {
        logger.error(e);
        return 'error';
      }
    } else {
      resource.surveyIsReadyToBePublished.push(idUser);
      try {
        await resource.save();
        return 'needMoreValidation';
      } catch (e) {
        logger.error(e);
        return 'error';
      }
    }
  },
  async report(id, idUser) {
    const resource = await this.findById(id);
    if (!resource) return 'notFound';
    if (resource.author.toString() === idUser.toString()) return 'sameAuthor';
    let tab = resource.isReportedBy.map(String);
    const index = tab.indexOf(idUser.toString());
    if (index !== -1) return 'alreadyReport';
    resource.isReportedBy.push(idUser);
    try {
      await resource.save();
      return true;
    } catch (e) {
      logger.error(e);
      return 'error';
    }
  },
  async removeReport(id, idUser) {
    const resource = await this.findById(id);
    if (!resource) return 'notFound';
    let tab = resource.isReportedBy.map(String);
    const index = tab.indexOf(idUser.toString());
    tab.splice(index, 1);
    resource.set({ isReportedBy: tab });
    try {
      await resource.save();
      return true;
    } catch (e) {
      logger.error(e);
      return 'error';
    }
  },
  async hide(id) {
    const resource = await this.findById(id);
    if (!resource) return 'notFound';
    resource.set({ deletedAt: moment.utc() });
    try {
      await resource.save();
      return true;
    } catch (e) {
      logger.error(e);
      return 'error';
    }
  },
  async comment(id, comment) {
    const resource = await this.findById(id);
    if (!resource) return 'notFound';
    resource.comments.push(comment);
    try {
      await resource.save();
      return true;
    } catch (e) {
      logger.error(e);
      return 'error';
    }
  },
  async removeComment(id, idComment) {
    const resource = await this.findById(id);
    if (!resource) return 'notFound';
    let tab = resource.comments.map(String);
    const index = tab.indexOf(idComment.toString());
    if (index === -1) return 'noCommentToDelete';
    tab.splice(index, 1);
    resource.set({ comments: tab });
    try {
      await resource.save();
      return true;
    } catch (e) {
      logger.error(e);
      return 'error';
    }
  },

  async like(id) {
    const resource = await this.findById(id);
    if (!resource) return 'notFound';
    if (isNaN(resource.numberOfLikes)) {
      resource.numberOfLikes = 1;
    } else {
      resource.numberOfLikes = resource.numberOfLikes + 1;
    }
    try {
      await resource.save();
      return true;
    } catch (e) {
      logger.error(e);
      return 'error';
    }
  },
  async unlike(id) {
    const resource = await this.findById(id);
    if (!resource) return 'notFound';
    resource.numberOfLikes = resource.numberOfLikes - 1;
    try {
      await resource.save();
      return true;
    } catch (e) {
      logger.error(e);
      return 'error';
    }
  },
  async validation(id, status) {
    const resource = await this.findById(id);
    if (!resource) return 'notFound';
    resource.resourceStatus = status;
    if (status === 3) {
      resource.publishedDate = moment.utc();
      if (resource.resourceType === 2) {
        const endDate = moment().add(resource.surveyDuration, 'days');
        resource.startDate = moment.utc();
        resource.endDate = endDate;
      }
    }
    try {
      await resource.save();
      return true;
    } catch (e) {
      logger.error(e);
      return 'error';
    }
  }
};
/**
 * @typedef Resource
 */

ResourceSchema.plugin(mongoosePaginate);
ResourceSchema.plugin(slug);
ResourceSchema.plugin(require('mongoose-autopopulate'));
module.exports = mongoose.model('Resource', ResourceSchema);
