const mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  mongoosePaginate = require('mongoose-paginate-v2');
/**
 * Sponsor Schema
 */
const SponsorSchema = new Schema({
  image: { type: String, required: true },
  name: { type: String, required: true },
  shortDescription: { type: String, required: true },
  website: { type: String, required: false }
});

SponsorSchema.plugin(mongoosePaginate);
/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */

/**
 * Statics
 */

/**
 * @typedef Sponsor
 */
module.exports = mongoose.model('Sponsor', SponsorSchema);
