import logger from '../services/logger/logger';
import moment from 'moment';

const mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  mongoosePaginate = require('mongoose-paginate-v2');
/**
 * Comment Schema
 */
const CommentSchema = new Schema({
  text: { type: String, required: true },
  author: { type: mongoose.Schema.ObjectId, ref: 'User' },
  publishedDate: { type: Date, default: moment.utc() },
  resource: { type: mongoose.Schema.ObjectId, ref: 'Resource' },
  isReportedBy: [{ type: mongoose.Schema.ObjectId, ref: 'User' }],
  isReportedByModerator: { type: Boolean }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
CommentSchema.statics = {
  async report(id, idUser, isReportedByModerator = null) {
    const comment = await this.findById(id);
    if (!comment) return 'notFound';
    if (comment.author.toString() === idUser.toString()) return 'sameAuthor';
    let tab = comment.isReportedBy.map(String);
    const index = tab.indexOf(idUser.toString());
    if (index !== -1) return 'alreadyReport';
    comment.isReportedBy.push(idUser);
    if (isReportedByModerator === true) comment.isReportedByModerator = true;
    try {
      await comment.save();
      return true;
    } catch (e) {
      logger.error(e);
      return 'error';
    }
  },
  async removeReport(id, idUser) {
    const comment = await this.findById(id);
    if (!comment) return 'notFound';
    let tab = comment.isReportedBy.map(String);
    const index = tab.indexOf(idUser.toString());
    tab.splice(index, 1);
    comment.set({ isReportedBy: tab });
    try {
      await comment.save();
      return true;
    } catch (e) {
      logger.error(e);
      return 'error';
    }
  }
};

/**
 * Methods
 */

/**
 * Statics
 */

/**
 * @typedef Comment
 */
CommentSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('Comment', CommentSchema);
