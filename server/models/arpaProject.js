import logger from '../services/logger/logger';

const mongoose = require('mongoose'),
  Schema = mongoose.Schema;
require('../models/projectMember');
require('./projectReport');
/**
 * ArpaProject Schema
 */
const ArpaProjectSchema = new Schema({
  content: { type: String, required: true },
  members: [{ type: mongoose.Schema.ObjectId, ref: 'ProjectMember' }],
  reports: [{ type: mongoose.Schema.ObjectId, ref: 'ProjectReport' }]
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
ArpaProjectSchema.statics = {
  async addMember(id, member) {
    const arpa = await this.findById(id);
    if (!arpa) return;
    arpa.members.push(member);
    try {
      await arpa.save();
      return true;
    } catch (e) {
      logger.error(e);
      return 'error';
    }
  },
  async addReport(id, report) {
    const arpa = await this.findById(id);
    if (!arpa) return;
    arpa.reports.push(report);
    try {
      await arpa.save();
      return true;
    } catch (e) {
      logger.error(e);
      return 'error';
    }
  }
};

/**
 * Statics
 */

/**
 * @typedef ArpaProject
 */
module.exports = mongoose.model('ArpaProject', ArpaProjectSchema);
