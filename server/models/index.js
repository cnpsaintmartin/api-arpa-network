import mongoose from 'mongoose';
import util from 'util';
import { exec } from 'child_process';
import dummy from 'mongoose-dummy';
import consola from 'consola';
import { Leader } from 'mongo-leader';

import schedule from '../services/schedule';

// config should be imported before importing any other file
import config from 'config';

const debug = require('debug')('express-mongoose-es6-rest-api:index');

// make bluebird default Promise
Promise = require('bluebird'); // eslint-disable-line no-global-assign

// plugin bluebird promise in mongoose
mongoose.Promise = Promise;

// connect to mongo db
const mongoUri =
  process.env.NODE_ENV === 'production'
    ? process.env.MONGO_URI
    : config.mongo.host;

let grid;
let database;

const launch = (force = false) =>
  new Promise(resolve => {
    const insertSeeders = () =>
      exec('md-seed run', (err, stdout) => {
        console.log(stdout);
        if (err)
          consola.ready({
            message: `seeders already insert into the database`,
            badge: true
          });
        else
          consola.ready({
            message: `seeders inserted`,
            badge: true
          });
        return resolve(stdout);
      });
    const db = mongoose.connect(
      mongoUri,
      {
        useCreateIndex: true,
        useNewUrlParser: true
      },
      err => {
        if (err) throw err;
        const { db } = mongoose.connection;
        database = db;
        if (process.env.NODE_ENV === 'test' && force === true) {
          mongoose.connection.db.dropDatabase();
          consola.success('database is clean');
          insertSeeders();
        }

        /*
        * GridFSBucket
        * */
        grid = new mongoose.mongo.GridFSBucket(db, {
          chunkSizeBytes: 2048,
          bucketName: 'images'
        });
        /*
        * Cluster leader management
        * */
        const leader = new Leader(db, { ttl: 5000, wait: 1000 });
        leader.on('elected', async () => {
          consola.info('the instance has been elected leader of the cluster');
          await schedule.init(db);
          schedule.start();
        });
        leader.on('revoked', () => {
          consola.info('the instance has revoked from his leadership');
        });
      }
    );

    if (process.env.NODE_ENV !== 'test')
      mongoose.connection.once('connected', async () => {
        consola.success('database is ready');
        insertSeeders();
      });

    mongoose.connection.on('error', () => {
      throw new Error(`unable to connect to database: ${mongoUri}`);
    });
    if (process.env.NODE_ENV !== 'production') {
      // generate dummy data
      // const t = require('./resource');
      // for (let i = 0; i < 300; i++) {
      //   const ignoredFields = ['_id', 'created_at', '__v', /detail.*_info/];
      //   let randomObject = dummy(t, {
      //     returnDate: true,
      //     ignore: ignoredFields,
      //     force: {
      //       resourceType: Math.round(Math.random() * (3 - 1) + 1),
      //       language: 'en'
      //     }
      //   });
      //   new t(randomObject).save();
      // }
    }

    // print mongoose logs in dev env
    if (config.mongooseDebug) {
      mongoose.set('debug', (collectionName, method, query, doc) => {
        debug(
          `${collectionName}.${method}`,
          util.inspect(query, false, 20),
          doc
        );
      });
    }
    return db;
  });

export { launch, grid, database };
