const mongoose = require('mongoose'),
  Schema = mongoose.Schema;
/**
 * Committee code conduct Schema
 */
const CommitteeCodeConductSchema = new Schema({
  content: { type: String, required: true }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */

/**
 * Statics
 */

/**
 * @typedef CommitteeCodeConductSchema
 */
module.exports = mongoose.model(
  'committeeCodeConduct',
  CommitteeCodeConductSchema
);
