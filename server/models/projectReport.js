import moment from 'moment';

const mongoose = require('mongoose'),
  Schema = mongoose.Schema;
/**
 * ProjectReport Schema
 */
const ProjectReportSchema = new Schema({
  title: { type: String, required: true },
  mainContent: { type: String, required: true },
  image: { type: String, required: true },
  isInNavbar: { type: Boolean, default: false },
  attachments: [
    {
      url: { type: String, required: false },
      name: { type: String, required: false },
      required: false
    }
  ],
  createdAt: { type: Date, default: moment.utc() }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */

/**
 * Statics
 */

/**
 * @typedef ProjectReport
 */
module.exports = mongoose.model('ProjectReport', ProjectReportSchema);
