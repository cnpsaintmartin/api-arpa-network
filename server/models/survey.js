import logger from '../services/logger/logger';

const mongoose = require('mongoose'),
  Schema = mongoose.Schema;
/**
 * Survey Schema
 */
const SurveySchema = new Schema({
  topic: { type: String, required: true },
  choices: [{ type: mongoose.Schema.ObjectId, ref: 'SurveyChoice' }],
  vote: [{ type: mongoose.Schema.ObjectId, ref: 'User' }]
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
SurveySchema.statics = {
  async vote(id, idUser) {
    const survey = await this.findById(id);
    if (!survey) return 'notFound';
    const result = survey.vote.find(
      author => author.toString() === idUser.toString()
    );
    if (result) {
      return 'two_votes';
    }
    survey.vote.push(idUser);
    try {
      await survey.save();
      return true;
    } catch (e) {
      logger.error(e);
      return 'error';
    }
  }
};

/**
 * Statics
 */

/**
 * @typedef Survey
 */
module.exports = mongoose.model('Survey', SurveySchema);
