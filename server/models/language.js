const mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  mongoosePaginate = require('mongoose-paginate-v2');
/**
 * Language Schema
 */
const LanguageSchema = new Schema({
  code: { type: String, required: true },
  name: { type: String, required: true }
});

LanguageSchema.plugin(mongoosePaginate);
/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */

/**
 * Statics
 */

/**
 * @typedef Language
 */
module.exports = mongoose.model('Language', LanguageSchema);
