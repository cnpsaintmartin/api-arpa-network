const mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  mongoosePaginate = require('mongoose-paginate-v2');
/**
 * SatisfactionSurvey Schema
 */
const SatisfactionSurveySchema = new Schema({
  lang: { type: String, required: true },
  questionOne: { type: Number, required: true },
  questionTwo: { type: Number, required: true },
  questionThree: { type: Boolean, required: true },
  questionFour: { type: Number, required: true },
  questionFive: { type: String, required: false },
  user: { type: mongoose.Schema.ObjectId, ref: 'User', required: false }
});

SatisfactionSurveySchema.plugin(mongoosePaginate);
/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */

/**
 * Statics
 */

/**
 * @typedef SatisfactionSurvey
 */
module.exports = mongoose.model('SatisfactionSurvey', SatisfactionSurveySchema);
