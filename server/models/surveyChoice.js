import logger from '../services/logger/logger';

const mongoose = require('mongoose'),
  Schema = mongoose.Schema;
/**
 * SurveyChoiceSchema Schema
 */

const SurveyChoiceSchema = new Schema({
  answer: { type: String, required: true },
  numberOfVote: { type: Number, default: 0 }
});
/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */

/**
 * Statics
 */
SurveyChoiceSchema.statics = {
  async vote(id) {
    const choice = await this.findById(id);
    if (!choice) return 'notFound';
    if (isNaN(choice.numberOfVote)) {
      choice.numberOfVote = 1;
    } else {
      choice.numberOfVote = choice.numberOfVote + 1;
    }
    try {
      await choice.save();
      return true;
    } catch (e) {
      logger.error(e);
      return 'error';
    }
  }
};

/**
 * @typedef SurveyChoice
 */
module.exports = mongoose.model('SurveyChoice', SurveyChoiceSchema);
