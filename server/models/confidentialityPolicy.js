const mongoose = require('mongoose'),
  Schema = mongoose.Schema;
/**
 * ConfidentialityPolicySchema
 */
const ConfidentialityPolicySchema = new Schema({
  content: { type: String, required: true }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */

/**
 * Statics
 */

/**
 * @typedef ConfidentialityPolicySchema
 */
module.exports = mongoose.model(
  'confidentialityPolicy',
  ConfidentialityPolicySchema
);
