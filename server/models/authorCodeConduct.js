const mongoose = require('mongoose'),
  Schema = mongoose.Schema;
/**
 * Author code conduct Schema
 */
const AuthorCodeConductSchema = new Schema({
  content: { type: String, required: true }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */

/**
 * Statics
 */

/**
 * @typedef AuthorCodeConductSchema
 */
module.exports = mongoose.model('authorCodeConduct', AuthorCodeConductSchema);
