const mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  mongoosePaginate = require('mongoose-paginate-v2');
/**
 * Language Schema
 */
const CountrySchema = new Schema({
  name: { type: String, required: true }
});

CountrySchema.plugin(mongoosePaginate);
/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */

/**
 * Statics
 */

/**
 * @typedef Language
 */
module.exports = mongoose.model('Country', CountrySchema);
