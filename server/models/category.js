const mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  mongoosePaginate = require('mongoose-paginate-v2');
/**
 * Category Schema
 */
const CategorySchema = new Schema({
  name: { type: String, required: true },
  isShowInHomePage: { type: Boolean, required: true, default: false },
  fontColor: { type: String, required: false },
  backgroundColor: { type: String, required: false },
  number: { type: Number, required: false }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */

/**
 * Statics
 */

/**
 * @typedef Category
 */
CategorySchema.plugin(mongoosePaginate);
module.exports = mongoose.model('Category', CategorySchema);
