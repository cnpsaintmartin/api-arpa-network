import logger from '../services/logger/logger';
import moment from 'moment';

const mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  mongoosePaginate = require('mongoose-paginate-v2');
/**
 * Notification Schema
 */
const NotificationSchema = new Schema({
  sender: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }, // Notification creator
  receiver: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }], // Ids of the receivers of the notification
  title: String,
  message: String, // any description of the notification message
  readBy: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    }
  ],
  displayReadBy: { type: Boolean, default: false },
  titleCode: String,
  messageCode: String,
  type: String,
  agreeMemberRequest: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }], // Ids of the persons who is agree with the request
  declineMemberRequest: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }], // Ids of the persons who isn't agree with the request
  resource: { type: mongoose.Schema.Types.ObjectId, ref: 'Resource' },
  comment: { type: mongoose.Schema.Types.ObjectId, ref: 'Comment' },
  createdAt: { type: Date, default: moment.utc() }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
/**
 * Methods
 */
NotificationSchema.statics = {
  async addVoteToCommitteeMemberRequest(idMemberRequest, accept, idUser) {
    const notification = await this.findById(idMemberRequest).exec();
    if (notification) {
      if (accept) {
        notification.agreeMemberRequest.push(idUser);
      } else {
        notification.declineMemberRequest.push(idUser);
      }
      try {
        await notification.save();
        let agreeLength = 0;
        let declineLength = 0;
        if (notification.agreeMemberRequest) {
          agreeLength = notification.agreeMemberRequest.length;
        }
        if (notification.declineMemberRequest) {
          declineLength = notification.declineMemberRequest.length;
        }
        return { agreeLength, declineLength };
      } catch (e) {
        logger.error(e);
        return 'error';
      }
    }
  },
  async checkIfContactRequestAlreadySent(idTarget, idCurrentUser) {
    const notification = await this.findOne({
      type: 'contactRequest',
      sender: idCurrentUser.toString(),
      receiver: { $in: idTarget.toString() }
    });
    return !!notification;
  },
  async checkIfCommitteeMemberRequestAlreadySent(idCurrentUser) {
    const notification = await this.findOne({
      type: 'committeeMemberRequest',
      sender: idCurrentUser.toString()
    });
    return !!notification;
  },
  async markAsReadByUser(id, idUser) {
    const notification = await this.findById(id);
    if (!notification) return;
    const tab = notification.readBy.toObject().map(el => el._id.toString());
    if (tab.includes(idUser.toString())) {
      return false;
    }
    notification.readBy.push(idUser);
    try {
      await notification.save();
      return true;
    } catch (e) {
      logger.error(e);
      return 'error';
    }
  },
  async deleteOneNotification(
    idResource = null,
    idComment = null,
    type = null
  ) {
    if (idResource !== null && idComment !== null) {
      try {
        const notification = await this.findOne({
          comment: idComment,
          resource: idResource,
          type
        });
        await this.deleteOne({
          _id: notification._id
        });
        return true;
      } catch (e) {
        logger.error(e);
        return 'error';
      }
    } else {
      try {
        const notification = await this.findOne({
          resource: idResource,
          type
        }).lean();
        await this.deleteOne({
          _id: notification._id
        });
        return true;
      } catch (e) {
        logger.error(e);
        return 'error';
      }
    }
  },
  async deleteMyNotification(id, idUser) {
    const notification = await this.findById(id);
    if (!notification) return 'nothingToDelete';
    // if it's a non group notification, we delete the notification,
    // else we remove the index in the array
    if (
      notification.receiver.length === 1 &&
      notification.receiver[0].toString() === idUser.toString()
    ) {
      try {
        await this.deleteOne({ _id: id });
        return true;
      } catch (e) {
        logger.error(e);
        return 'error';
      }
    } else if (notification.receiver.map(String).includes(idUser.toString())) {
      let tab = notification.receiver.map(String);
      const index = tab.indexOf(idUser.toString());
      tab.splice(index, 1);
      notification.set({ receiver: tab });
      try {
        await notification.save();
        return true;
      } catch (e) {
        logger.error(e);
        return 'error';
      }
    }
    return 'error';
  }
};
/**
 * Statics
 */
/**
 * @typedef Notification
 */
NotificationSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('Notification', NotificationSchema);
