const mongoose = require('mongoose'),
  Schema = mongoose.Schema;
/**
 * TermsOfService Schema
 */
const TermsOfServiceSchema = new Schema({
  content: { type: String, required: true }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */

/**
 * Statics
 */

/**
 * @typedef TermsOfService
 */
module.exports = mongoose.model('TermsOfService', TermsOfServiceSchema);
