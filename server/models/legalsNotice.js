const mongoose = require('mongoose'),
  Schema = mongoose.Schema;
/**
 * LegalsNotice Schema
 */
const LegalsNoticeSchema = new Schema({
  content: { type: String, required: true }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */

/**
 * Statics
 */

/**
 * @typedef LegalsNotice
 */
module.exports = mongoose.model('LegalsNotice', LegalsNoticeSchema);
