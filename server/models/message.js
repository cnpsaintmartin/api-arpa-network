import moment from 'moment';

const mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  mongoosePaginate = require('mongoose-paginate-v2');
/**
 * Message Schema
 */
const MessageSchema = new Schema({
  message: { type: String, required: true },
  user: { type: mongoose.Schema.ObjectId, ref: 'User', required: true },
  createdAt: { type: Date, default: moment.utc() }
});

MessageSchema.plugin(mongoosePaginate);
/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */

/**
 * Statics
 */

/**
 * @typedef Message
 */
module.exports = mongoose.model('Message', MessageSchema);
