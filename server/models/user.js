import moment from 'moment';
import logger from '../services/logger/logger';
import Role from './role';
import Notification from './notification';
const Promise = require('bluebird'),
  mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  bcrypt = require('bcryptjs'),
  SALT_WORK_FACTOR = 10,
  uniqueValidator = require('mongoose-unique-validator'),
  mongoosePaginate = require('mongoose-paginate-v2');
/**
 * User Schema
 */
const UserSchema = new Schema({
  firstname: { type: String, required: true },
  lastname: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  isAgreeAppearProfessionalContactBook: {
    type: Boolean,
    required: false,
    default: false
  },
  isAgreeWithContactRequest: { type: Boolean, required: false, default: false },
  description: { type: String, required: false },
  positionHeld: { type: String, required: false },
  country: { type: mongoose.Schema.ObjectId, ref: 'Country' },
  spokenLanguages: [{ type: mongoose.Schema.ObjectId, ref: 'Language' }],
  roles: [{ type: mongoose.Schema.ObjectId, ref: 'Role' }],
  profileImage: { type: String, required: false },
  forget: {
    code: { type: String, required: false },
    validity: { type: Date, required: false }
  },
  myNetwork: [{ type: mongoose.Schema.ObjectId, ref: 'User' }],
  // -> ban || waiting_professional_request || wainting_committee_request || cant comment
  status: [{ type: String }],
  professionalDocument: { type: String },
  // number of comment reported by this user
  numberCommentReport: { type: Number, default: 0 },
  // number of his comment who has been reported
  commentOfThisUserReported: { type: Number, default: 0 },
  // date since he his banned from comment (CRON job)
  isCommentBannedSince: { type: Date },
  // TODO for cron job to check (more than 3 commentOfThisUserReported) is (commentOfThisUserReported - 3 * 10days) + 15days
  // cron job need to remove status cantComment when it's ok and set isCommentBannedSine at undefined
  hasAnsweredToSatisfactionSurvey: { type: Boolean, default: false },
  lastAction: { type: Date, default: moment.utc() },
  createdAt: { type: Date, default: moment.utc() },
  updatedAt: { type: Date, default: moment.utc() }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
UserSchema.pre('save', function(next) {
  const user = this;

  // only hash the password if it has been modified (or is new)
  if (!user.isModified('password')) return next();

  // generate a salt
  bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
    if (err) return next(err);

    // hash the password using our new salt
    bcrypt.hash(user.password, salt, function(err, hash) {
      if (err) return next(err);

      // override the cleartext password with the hashed one
      user.password = hash;
      next();
    });
  });
});

UserSchema.plugin(uniqueValidator);

UserSchema.methods.comparePassword = function(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
    if (err) return cb(err);
    cb(null, isMatch);
  });
};
/**
 * Methods
 */
UserSchema.method({});

/**
 * Statics
 */
UserSchema.statics = {
  async updateLastAction(idUser) {
    const user = await this.findById(idUser);
    if (!user) return 'not found';
    user.hasAnsweredToSatisfactionSurvey = true;
    try {
      await user.save();
      return true;
    } catch (e) {
      return 'error';
    }
  },
  async satisfactionSurvey(idUser) {
    const user = await this.findById(idUser);
    if (!user) return 'not found';
    user.lastAction = moment.utc();
    try {
      await user.save();
      return true;
    } catch (e) {
      return 'error';
    }
  },
  async addGoodReport(idUser) {
    const user = await this.findById(idUser);
    if (!user) return 'notFound';
    let isPromoteAsModo = false;
    user.numberCommentReport = user.numberCommentReport + 1;
    if (user.numberCommentReport === 5) {
      const idModerator = await Role.getIdWithSlug(['moderator']);
      user.roles.push(idModerator);
      isPromoteAsModo = true;
      let notification = new Notification({
        createdAt: moment.utc(),
        title: "You've been promoted as moderator",
        receiver: idUser,
        message:
          'You have reported successfully 5 comments.If you report an other comment, it will automatically remove. If the committee reject your report, you will loose your moderator role',
        messageCode: '0021',
        titleCode: '0021',
        type: 'newModerator'
      });
      await notification.save();
    }
    try {
      await user.save();
      if (isPromoteAsModo) return 'newModo';
      return true;
    } catch (e) {
      logger.error(e);
      return 'error';
    }
  },
  async revokeModerator(idUser) {
    const user = await this.findById(idUser);
    if (!user) return 'notFound';
    let tab = user.roles.map(String);
    const idModerator = await Role.getIdWithSlug(['moderator']);
    const index = tab.indexOf(idModerator.toString());
    if (index > -1) {
      tab.splice(index, 1);
      user.set({ roles: tab });
      user.numberCommentReport = 0;
      try {
        await user.save();
        let notification = new Notification({
          createdAt: moment.utc(),
          title: 'You loose your moderator role',
          receiver: idUser,
          message:
            'The committee reject your report, you loose your moderator role.',
          messageCode: '0022',
          titleCode: '0022',
          type: 'looseModerator'
        });
        await notification.save();
        return true;
      } catch (e) {
        logger.error(e);
        return 'error';
      }
    }
  },
  async addOneReportedComment(idUser) {
    const user = await this.findById(idUser);
    if (!user) return { state: 'notFound' };
    user.commentOfThisUserReported = user.commentOfThisUserReported + 1;
    if (user.commentOfThisUserReported >= 3) {
      if (!user.status.includes('cantComment')) {
        user.status.push('cantComment');
      }
      user.isCommentBannedSince = new Date();
      const numberOfDay = (user.commentOfThisUserReported - 3) * 10 + 15;
      try {
        let notification = new Notification({
          createdAt: moment.utc(),
          title: "You can't comment",
          receiver: idUser,
          message:
            "Your comments has been reported. You can't add a comment during " +
            numberOfDay +
            ' days',
          messageCode: '0000',
          titleCode: '0017',
          type: 'userCommentBanned'
        });
        const p1 = notification.save();
        const p2 = user.save();
        await Promise.all([p1, p2]);
        return { state: 'userBan', numberOfDay };
      } catch (e) {
        logger.error(e);
        return { state: 'error' };
      }
    }
    try {
      await user.save();
      return { state: 'notBanYet' };
    } catch (e) {
      logger.error(e);
      return { state: 'error' };
    }
  },
  async isInMyNetwork(myID, idToCheck) {
    const user = await this.findById(myID);
    if (!user) return 'notFound';
    return user.myNetwork.map(String).includes(idToCheck);
  },
  async addMemberInMyNetwork(idMember, myID) {
    const user = await this.findById(myID);
    if (!user) return 'notFound';
    user.myNetwork.push(idMember);
    try {
      await user.save();
      return true;
    } catch (e) {
      logger.error(e);
      return 'error';
    }
  },
  async getUserByGroup(tabGroupId) {
    const users = await this.find({
      roles: { $in: tabGroupId }
    });
    if (!users) return 'notFound';
    return users.map(user => user._id);
  },
  async getEmailByGroup(tabGroupId) {
    const users = await this.find({
      roles: { $in: tabGroupId }
    });
    if (!users) return 'notFound';
    return users.map(user => user.email);
  }
};
/**
 * @typedef User
 */
UserSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('User', UserSchema);
