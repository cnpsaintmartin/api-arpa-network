const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const gridSchema = new Schema({}, { strict: false });
const files = mongoose.model('Grid', gridSchema, 'fs.files');
module.exports = files;
