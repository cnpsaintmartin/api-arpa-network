import moment from 'moment';

const mongoose = require('mongoose'),
  Schema = mongoose.Schema;
/**
 * Like Schema
 */
const LikeSchema = new Schema({
  author: { type: mongoose.Schema.ObjectId, ref: 'User' },
  resource: { type: mongoose.Schema.ObjectId, ref: 'Resource' },
  likeDate: { type: Date, default: moment.utc() }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */

/**
 * Statics
 */

/**
 * @typedef Like
 */
LikeSchema.index({ author: 1, resource: 1 }, { unique: true });
const Like = mongoose.model('Like', LikeSchema);
Like.on('index', function(error) {
  return error;
});
module.exports = mongoose.model('Like', LikeSchema);
