const mongoose = require('mongoose'),
  Schema = mongoose.Schema;
/**
 * Tag Role
 */
const RoleSchema = new Schema({
  name: { type: String, required: true },
  slug: { type: String, required: true }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */

RoleSchema.statics = {
  async getIdWithSlug(tabSlug) {
    const roles = await this.find({ slug: { $in: tabSlug } });
    if (roles) {
      return roles.map(el => el._id);
    }
    return false;
  }
};

/**
 * Statics
 */

/**
 * @typedef Role
 */
module.exports = mongoose.model('Role', RoleSchema);
