import DailyRotateFile from 'winston-daily-rotate-file';
import winston from 'winston';

const isProduction = process.env.NODE_ENV === 'production';

const dailyRotateConfig = {
  fileName: 'api-%DATE%.log',
  datePattern: 'YYYY-MM-DD-HH',
  zippedArchive: true,
  maxSize: '20m',
  maxFiles: '14d',
  dirname: './logs'
};

const config = {
  transports: isProduction
    ? [new DailyRotateFile(dailyRotateConfig)]
    : [
        new winston.transports.Console({
          format: winston.format.simple()
        })
      ]
};
export default winston.createLogger(config);
