import mailer from 'nodemailer';
import { htmlToText } from 'nodemailer-html-to-text';
import consola from 'consola';
import config from 'config';
import ejs from 'ejs';
import fs from 'fs';
import path from 'path';
import cliProgress from 'cli-progress';
import htmlMinifier from 'html-minifier';
import logger from '../logger/logger';

const fromMailer = 'ARPA Network <no-reply@arpa-ageing.eu>';

/**
 * Connect the mailer
 */

let transporter;

const verify = () =>
  transporter.verify(err => {
    if (err) return consola.error(err);
    // convert the html message to plain text
    transporter.use('compile', htmlToText());
    consola.success('ARPA mailer successfully connected');
  });

if (process.env.NODE_ENV !== 'production') {
  mailer.createTestAccount().then(account => {
    transporter = mailer.createTransport({
      host: 'smtp.ethereal.email',
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        user: account.user, // generated ethereal user
        pass: account.pass // generated ethereal password
      }
    });
    verify();
  });
} else {
  // mailer connector for production
  transporter = mailer.createTransport({
    host: config.hostMail,
    pool: true,
    port: 25,
    auth: {
      user: 'no-reply@arpa-ageing.eu',
      pass: process.env.MAIL_PASSWORD
    },
    tls: {
      rejectUnauthorized: false
    }
  });
  verify();
}

/**
 * Compile the templates
 */

const templates = [];
fs.readdir(`${__dirname}/templates`, async (err, items) => {
  if (err) throw err;
  const bar = new cliProgress.Bar({}, cliProgress.Presets.shades_classic);
  consola.info('Begin mail template compilation\n');
  bar.start(items.length, 0);
  await items.forEach(file => {
    try {
      const content = fs
        .readFileSync(path.join(`${__dirname}/templates/${file}`), 'utf8')
        .toString();

      templates.push({
        name: path.parse(file).name,
        content: ejs.compile(
          htmlMinifier.minify(content, {
            ignoreCustomFragments: [/<%[\s\S]*?%>/]
          })
        )
      });
      bar.increment(1);
    } catch (e) {
      logger.error(e);
    }
  });
  bar.stop();
  consola.info('Mail template compilation finished\n');
});

/**
 *
 * @param sendTo {Array<Object> | String}
 * @param subject { String }
 * @param params { Object }
 * @param templateName { String }
 * @return { Array }
 */
const send = async (sendTo, subject, params, templateName) => {
  const sendEmail = async ({ to, subject, templateName, params }) => {
    let mailObject;
    // the `to` parameter is the sender address
    if (typeof to === 'string') {
      if (!templates.some(t => t.name === templateName))
        throw new Error(`the template name '${templateName}' doesn't exist`);
      mailObject = {
        from: fromMailer,
        to,
        subject,
        html: templates.find(t => t.name === templateName).content(params)
      };
    } else {
      if (!templates.some(t => t.name === to.templateName))
        throw new Error(`the template name '${to.templateName}' doesn't exist`);
      mailObject = {
        from: fromMailer,
        to: to.to,
        subject: to.subject,
        html: templates.find(t => t.name === to.templateName).content(to.params)
      };
    }
    try {
      return await transporter.sendMail(mailObject);
    } catch (e) {
      logger.error(e);
    }
  };
  const senders = Array.isArray(sendTo) ? sendTo : [sendTo];
  try {
    const responses = await Promise.all(
      senders.map(to =>
        sendEmail({
          to,
          subject,
          templateName,
          params
        })
      )
    );
    return responses;
  } catch (e) {
    logger.error(e);
  }
};

export default {
  transporter,
  send
};
