import moment from 'moment';
import Resource from '../../../models/resource';
import Role from '../../../models/role';
import User from '../../../models/user';
import mailer from '../../mailer';

export default agenda => {
  const verify = async () => {
    const resources = await Resource.find({
      $and: [
        { publishedDate: null },
        { submittedDate: { $gte: moment().subtract(3, 'days') } }
      ]
    });
    if (!resources.length) return;
    const tabIdGroup = await Role.getIdWithSlug([
      'committee_member',
      'native_committee_member'
    ]);

    const users = await User.getEmailByGroup(tabIdGroup);
    await mailer.send(users, 'Some article are still unpublished', { name: 'test' }, 'welcome');
  };
  agenda.define('verify all unpublished resources', verify);
  agenda.every('0 10 * * *', 'verify all unpublished resources');
};
