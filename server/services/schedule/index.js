import Agenda from 'agenda';
import fs from 'fs';
import path from 'path';

let agenda;

const init = async db => {
  agenda = new Agenda({ mongo: db, collection: 'agendaJobs' });
  await agenda._ready;
};

const stop = () => agenda.stop();

const start = () => {
  const jobs = fs.readdirSync(path.join(__dirname, 'jobs'));
  jobs.forEach(file => {
    require(path.join(__dirname, 'jobs', file)).default(agenda);
  });
  agenda.start();
};

export default {
  init,
  start,
  stop,
  agenda
};
