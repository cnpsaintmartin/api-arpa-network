import logger from '../services/logger/logger';

const mongoose = require('mongoose');
const gridStream = require('gridfs-stream');
let grid;
mongoose.connection.once('connected', async () => {
  grid = gridStream(mongoose.connection.db, mongoose.mongo);
});
const removeFile = async link => {
  const idFile = link.substring(link.lastIndexOf('/') + 1);
  try {
    await grid.remove({ _id: idFile });
  } catch (e) {
    logger.error(e);
    return 'error';
  }
};

export { removeFile };
export default {
  removeFile
};
