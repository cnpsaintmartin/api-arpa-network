const responseType = {
  success: (codeError, message = null, docs = null) => {
    return {
      success: true,
      ...(message !== null ? { message } : {}),
      codeError,
      ...(docs !== null ? { ...docs } : {})
    };
  },
  information: (codeError, message = null, docs = null) => {
    return {
      success: true,
      ...(message !== null ? { message } : {}),
      codeError,
      ...(docs !== null ? { ...docs } : {})
    };
  },
  error: (codeError, message = null, docs = null) => {
    return {
      success: false,
      ...(message !== null ? { message } : {}),
      codeError,
      ...(docs !== null ? { ...docs } : {})
    };
  },
  warning: (codeError, message = null, docs = null) => {
    return {
      success: false,
      ...(message !== null ? { message } : {}),
      codeError,
      ...(docs !== null ? { ...docs } : {})
    };
  }
};
export { responseType };
export default {};
