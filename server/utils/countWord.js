const estimateReadindSpeed = 200; // 200 words/min
const countWords = str =>
  str
    .replace(/(^\s*)|(\s*$)/gi, '') // exclude  start and end white-space
    .replace(/[ ]{2,}/gi, ' ') // 2 or more space to 1
    .replace(/\n /, '\n') // exclude newline with a start spacing
    .split(' ')
    .filter(str => str !== '').length;

const countTime = str => Math.ceil(countWords(str) / estimateReadindSpeed);

export { countTime, countWords, estimateReadindSpeed };

export default {
  countWords,
  countTime,
  estimateReadindSpeed
};
