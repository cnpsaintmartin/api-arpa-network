import jwt from 'jsonwebtoken';
import config from 'config';
import User from '../models/user';
const useCookie = async req => {
  if (req.cookies['auth._token.local']) {
    await jwt.verify(
      req.cookies['auth._token.local'].substring(7),
      config.secretToken,
      async (err, decoded) => {
        if (err) return false;
        req.token = decoded;
        req.user = await User.find({ _id: decoded._id })
          .select('_id lastname firstname email roles spokenLanguages status')
          .populate({
            path: 'roles'
          })
          .populate({
            path: 'spokenLanguages'
          })
          .lean()
          .exec();
        if (!req.user) return false;
        const rolesSlug = req.user[0].roles.map(el => el.slug);
        req.user = req.user[0];
        req.user.roles = rolesSlug;
        return true;
      }
    );
  }
};
export { useCookie };
