import { useCookie } from './useCookie';

const isAuthorized = async (roleNeed, roleUser) => {
  let found = roleUser.some(r => roleNeed.includes(r));
  return !!found;
};

const itsMe = async (idToCheck, req) => {
  if (!req.user) {
    await useCookie(req);
    if (req.user) {
      return idToCheck.toString() === req.user._id.toString();
    }
    return false;
  }
  return idToCheck.toString() === req.user._id.toString();
};

const isBan = status => {
  if (!status || status.length === 0) return false;
  return status.includes('ban');
};
export { isAuthorized, itsMe, isBan };
