import { launch } from './models';
import swaggerUi from 'swagger-ui-express';
import { initialize } from 'express-openapi';
import { resolve } from 'path';
import express from 'express';
import multer from 'multer';
import helmet from 'helmet';
import bodyparser from 'body-parser';
import compression from 'compression';
import cors from 'cors';
import jwt from 'jsonwebtoken';
import cookieParser from 'cookie-parser';
import User from './models/user';
import config from 'config';
import apiDocV1 from './api-doc';

const app = express();

launch();

// Load configuration
app.use(compression());
app.use(helmet());
app.use(cors());
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));
app.use(cookieParser());

// const appDoc = fs.readFileSync(join(__dirname, './app.yml'), 'utf-8');
// const appDocJson = yamljs.parse(appDoc);
const Bearer = (req, scopes, definition, cb) => {
  const token =
    req.headers.authorization || req.cookies['auth._token.local'] || null;
  if (!token) return false;
  return jwt.verify(
    token.substring(7),
    config.secretToken,
    async (err, decoded) => {
      if (err) return false;
      req.token = decoded;
      if (scopes.length && !decoded.roles.some(u => scopes.includes(u)))
        return false;
      req.user = await User.find({ _id: decoded._id })
        .lean()
        .exec();
      if (!req.user) return false;
      req.user = req.user[0];
      req.user.roles = decoded.roles;
      if (
        decoded.roles.some(u =>
          ['committee_member', 'native_committee_member'].includes(u)
        )
      ) {
        await User.updateLastAction(req.user._id);
      }
      delete req.user.password;
      return true;
    }
  );
};

const consumesMiddleware = {
  'multipart/form-data': (req, res, next) => {
    multer().any()(req, res, err => {
      if (err) return next(err);
      if (!req.files || !req.files.length) return next();
      req.files.forEach(f => {
        req.body[f.fieldname] = f;
      });
      return next();
    });
  }
};

const throwError = (res, err, next) => {
  // eslint-disable-next-line
  if (!(process.env.NODE_ENV === 'production')) console.error(err);
  res.status(err.status).json(err);
  next();
};

// TODO create middleware to refresh cookie + add security if user is ban

initialize({
  app,
  apiDoc: apiDocV1,
  promiseMode: true,
  docsPath: '/api-docs',
  dependencies: {
    // doc: appDocJson,
    // dataprovider: mockDataProvider(),
    // geoservice: geoService
  },
  securityHandlers: {
    Bearer
  },
  consumesMiddleware,
  errorMiddleware(err, req, res, next) {
    throwError(res, err, next);
  },
  paths: resolve(__dirname, './src'),
  pathsIgnore: /\.spec$/
});

const options = {
  swaggerUrl: `/api/api-docs`
};

app.use('/app-docs', swaggerUi.serve, swaggerUi.setup(null, options));

export default {
  path: '/',
  handler: app,
  app
};
