import request from 'supertest';
import assert from 'assert';
import server from '../api';
describe('Countries', function() {
  it('sould retrieve countries list', done => {
    request(server.app)
      .get('/api/countries')
      .set('Accept', 'application/json')
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.notStrictEqual(res.body.docs.length, 0);
        return done();
      });
  });
});
