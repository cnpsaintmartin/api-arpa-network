import request from 'supertest';
import assert from 'assert';
import server from '../api';
describe('Categories', function() {
  it('sould retrieve categories list', done => {
    request(server.app)
      .get('/api/categories')
      .set('Accept', 'application/json')
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.notStrictEqual(res.body.docs.length, 0);
        return done();
      });
  });
});
