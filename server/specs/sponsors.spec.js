import request from 'supertest';
import assert from 'assert';
import server from '../api';
let token;
let idSponsor;

describe('Sponsor', function() {
  before(async () => {
    const user = await request(server.app)
      .post('/api/user/authentificate')
      .send({
        email: 'visitor@visitor.com',
        password: 'fizz'
      });
    token = `Bearer ${user.body.token}`;
  });
  it('should register a new sponsor', async () => {
    const result = await request(server.app)
      .post('/api/sponsor')
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .send({
        image: 'string',
        name: 'string',
        shortDescription: 'string',
        website: 'test'
      })
      .expect(200);
    idSponsor = result.body.docs._id;
    assert.strictEqual(result.body.docs.name, 'string');
  });
  it('should get a sponsor with id', done => {
    request(server.app)
      .get(`/api/sponsor/${idSponsor}`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.notStrictEqual(res.body.docs.length, 0);
        return done();
      });
  });
  it('should get the sponsors', done => {
    request(server.app)
      .get(`/api/sponsors`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.notStrictEqual(res.body.docs.length, 0);
        return done();
      });
  });
  it("shouldn't get a sponsor with id (not found)", done => {
    request(server.app)
      .get(`/api/sponsor/5c0d5da8fb123c276d502c8c`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(404)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(res.body.success, false);
        return done();
      });
  });
  it('should update a sponsor with id', done => {
    request(server.app)
      .put(`/api/sponsor/${idSponsor}`)
      .send({
        image: 'test',
        name: 'test',
        shortDescription: 'test'
      })
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(res.body.docs.name, 'test');
        return done();
      });
  });
  it('should delete one sponsor', done => {
    request(server.app)
      .delete(`/api/sponsor/${idSponsor}`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(res.body.message, 'Sponsor successfully deleted');
        return done();
      });
  });
});
