import request from 'supertest';
import assert from 'assert';
import server from '../api';
let token;

describe('Terms of service / Legals notice', function() {
  before(async () => {
    const user = await request(server.app)
      .post('/api/user/authentificate')
      .send({
        email: 'visitor@visitor.com',
        password: 'fizz'
      });
    token = `Bearer ${user.body.token}`;
  });
  it('should register the legals notice', async () => {
    const result = await request(server.app)
      .put('/api/legalsNotice')
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .send({
        content: 'This is the legals notice'
      })
      .expect(200);
    assert.strictEqual(result.body.docs.content, 'This is the legals notice');
  });
  it('should get the legals notice', done => {
    request(server.app)
      .get(`/api/legalsNotice`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(res.body.docs.content, 'This is the legals notice');
        return done();
      });
  });
  it('should register the terms of service', async () => {
    const result = await request(server.app)
      .put('/api/termsOfService')
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .send({
        content: 'This is the terms of service'
      })
      .expect(200);
    assert.strictEqual(
      result.body.docs.content,
      'This is the terms of service'
    );
  });
  it('should get the terms of service', done => {
    request(server.app)
      .get(`/api/termsOfService`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(
          res.body.docs.content,
          'This is the terms of service'
        );
        return done();
      });
  });
});
