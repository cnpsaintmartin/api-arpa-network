import request from 'supertest';
import assert from 'assert';
import server from '../api';
let token;
let idProjectReport;
let idProjectMember;

describe('ARPA project', function() {
  before(async () => {
    const user = await request(server.app)
      .post('/api/user/authentificate')
      .send({
        email: 'visitor@visitor.com',
        password: 'fizz'
      });
    token = `Bearer ${user.body.token}`;
  });
  it('should register a new description about arpa', async () => {
    const result = await request(server.app)
      .put('/api/arpaProject')
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .send({
        content: 'string'
      })
      .expect(200);
    assert.strictEqual(result.body.docs.content, 'string');
  });
  it('should get a arpa description', done => {
    request(server.app)
      .get(`/api/arpaProject`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.notStrictEqual(res.body.docs.length, 0);
        return done();
      });
  });
  it('should register a new project member', done => {
    request(server.app)
      .post(`/api/projectMember`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .send({
        institution: 'string',
        firstname: 'string',
        lastname: 'string',
        email: 'string',
        website: 'string'
      })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        idProjectMember = res.body.docs._id;
        assert.strictEqual(res.body.docs.firstname, 'string');
        return done();
      });
  });
  it('should get the project members', done => {
    request(server.app)
      .get(`/api/projectMembers`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.notStrictEqual(res.body.docs.length, 0);
        return done();
      });
  });
  it('should register a new project report', done => {
    request(server.app)
      .post(`/api/projectReport`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .send({
        title: 'string',
        mainContent: 'string',
        image: 'string'
      })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        idProjectReport = res.body.docs._id;
        assert.strictEqual(res.body.docs.title, 'string');
        return done();
      });
  });
  it('should get the project reports', done => {
    request(server.app)
      .get(`/api/projectReports`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.notStrictEqual(res.body.docs.length, 0);
        return done();
      });
  });
  it("shouldn't get a project report with id (not found)", done => {
    request(server.app)
      .get(`/api/projectReport/5c0d5da8fb123c276d502c8c`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(404)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(res.body.success, false);
        return done();
      });
  });
  it('should get a project report with id', done => {
    request(server.app)
      .get(`/api/projectReport/${idProjectReport}`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.notStrictEqual(res.body.docs.length, 0);
        return done();
      });
  });
  it('should update a project report with id', done => {
    request(server.app)
      .put(`/api/projectReport/${idProjectReport}`)
      .send({
        title: 'update string'
      })
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(res.body.docs.title, 'update string');
        return done();
      });
  });

  it("shouldn't get a project member with id (not found)", done => {
    request(server.app)
      .get(`/api/projectMember/5c0d5da8fb123c276d502c8c`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(404)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(res.body.success, false);
        return done();
      });
  });
  it('should get a project member with id', done => {
    request(server.app)
      .get(`/api/projectMember/${idProjectMember}`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.notStrictEqual(res.body.docs.length, 0);
        return done();
      });
  });
  it('should update a project member with id', done => {
    request(server.app)
      .put(`/api/projectMember/${idProjectMember}`)
      .send({
        institution: 'cnp'
      })
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(res.body.docs.institution, 'cnp');
        return done();
      });
  });

  it('should delete a project member', done => {
    request(server.app)
      .delete(`/api/projectMember/${idProjectMember}`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(
          res.body.message,
          'Project member successfully deleted'
        );
        return done();
      });
  });
  it('should delete a project report', done => {
    request(server.app)
      .delete(`/api/projectReport/${idProjectReport}`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(
          res.body.message,
          'Project report successfully deleted'
        );
        return done();
      });
  });
});
