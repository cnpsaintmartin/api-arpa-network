import request from 'supertest';
import assert from 'assert';
import server from '../api';
import Language from '../models/language';
import Role from '../models/role';

const randomMail =
  Math.random()
    .toString(36)
    .substring(2, 15) +
  Math.random()
    .toString(36)
    .substring(2, 15);
let idRole;
let token;
let idUser;
describe('User', () => {
  before(async () => {
    const roles = await Role.find({});
    idRole = roles[0]._id;
  });
  it('should create a user', async () => {
    const language = await Language.findOne({ name: 'French' });
    const result = await request(server.app)
      .post('/api/user')
      .send({
        lastname: 'Foo',
        firstname: 'Bar',
        email: randomMail,
        password: 'foo',
        isProfessional: true,
        isAgreeAppearProfessionalContactBook: true,
        isAgreeWithContactRequest: true,
        description: 'string',
        positionHeld: 'string',
        country: 'string',
        spokenLanguages: [language._id]
      })
      .set('Accept', 'application/json')
      .expect(200);
    assert.notStrictEqual(result.body.token, null);
  });
  it("souldn't create a new user, email is already used", done => {
    request(server.app)
      .post('/api/user')
      .send({
        lastname: 'Foo',
        firstname: 'Bar',
        email: randomMail,
        password: 'foo',
        isProfessional: true,
        isAgreeAppearProfessionalContactBook: true,
        isAgreeWithContactRequest: true,
        description: 'string',
        positionHeld: 'string',
        country: 'string',
        spokenLanguages: ['string']
      })
      .set('Accept', 'application/json')
      .expect(500)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(res.body.codeError, 14003);
        return done();
      });
  });
  it('sould login and get his token', done => {
    request(server.app)
      .post('/api/user/authentificate')
      .send({
        email: randomMail,
        password: 'foo'
      })
      .set('Accept', 'application/json')
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        token = `Bearer ${res.body.token}`;
        assert.strictEqual(res.body.codeError, 11002);
        return done();
      });
  });
  it("souldn't login with bad credentials", done => {
    request(server.app)
      .post('/api/user/authentificate')
      .send({
        email: randomMail,
        password: 'badCredentials'
      })
      .set('Accept', 'application/json')
      .expect(401)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(res.body.codeError, 14001);
        return done();
      });
  });
  it('sould retrieve his information with his token', done => {
    request(server.app)
      .get('/api/user')
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        idUser = res.body.docs._id;
        assert.strictEqual(res.body.codeError, 11005);
        return done();
      });
  });
  it("souldn't retrieve his information with bad token", done => {
    request(server.app)
      .get('/api/user')
      .set('Accept', 'application/json')
      .set('Authorization', 'tetetetet')
      .expect(401)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(res.body.status, 401);
        return done();
      });
  });
  it('should update an exisiting user', done => {
    request(server.app)
      .put(`/api/user/${idUser}`)
      .send({
        firstname: 'try to update my firstname'
      })
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(
          res.body.docs.firstname,
          'try to update my firstname'
        );
        return done();
      });
  });
  it('should get an exisiting user with it id', done => {
    request(server.app)
      .get(`/api/user/${idUser}`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(
          res.body.docs.firstname,
          'try to update my firstname'
        );
        return done();
      });
  });
  it('should get all roles', done => {
    request(server.app)
      .get(`/api/roles`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(res.body.codeError, 11018);
        return done();
      });
  });
});
