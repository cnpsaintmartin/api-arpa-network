import request from 'supertest';
import assert from 'assert';
import server from '../api';
import Category from '../models/category';
import Resource from '../models/resource';
import moment from 'moment';
let token;
let idLanguage = 'fr';
let idCategory;
let idTag;
let idResource;
let slug;
let idLockResource;
let idEvent;
let idUser;
let idSurvey;
let idAnswer;
const random = Math.random()
  .toString(36)
  .substring(2, 15);
describe('Resource', function() {
  before(async () => {
    const user = await request(server.app)
      .post('/api/user/authentificate')
      .send({
        email: 'visitor@visitor.com',
        password: 'fizz'
      });
    token = `Bearer ${user.body.token}`;
    idUser = user.body._id;
    const result = await request(server.app)
      .get('/api/user/')
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200);
    idUser = result.body.docs._id;
    const category = await Category.find({});
    idCategory = category[0]._id;
  });
  it('should register a new Tag for his resource', async () => {
    const result = await request(server.app)
      .post('/api/tag')
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .send({
        name: random
      })
      .expect(200);
    assert.strictEqual(result.body.docs.name, random);
  });
  it("shouldn't register a new Tag because it's already in the database", async () => {
    const result = await request(server.app)
      .post('/api/tag')
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .send({
        name: random
      })
      .expect(500);
    assert.strictEqual(result.body.codeError, 24006);
  });
  it('should register a new resource', done => {
    request(server.app)
      .post('/api/resource')
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .send({
        title: 'Unit Test',
        idCategory,
        tags: ['string'],
        shortDescription: 'unit test description firstone',
        mainContent: 'unit test main content',
        image: 'string',
        idLanguage,
        commentIsAuthorize: true,
        isDraft: false
      })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        idResource = res.body.docs._id;
        slug = res.body.docs.slug;
        assert.notStrictEqual(res.body.docs.length, 0);
        return done();
      });
  });
  it('should register a new resource without comment space', done => {
    request(server.app)
      .post('/api/resource')
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .send({
        title: 'Unit Test',
        idCategory,
        tags: ['string'],
        shortDescription: 'unit test description',
        mainContent: 'unit test main content',
        image: 'string',
        idLanguage,
        commentIsAuthorize: false,
        isDraft: false
      })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        idLockResource = res.body.docs._id;
        assert.notStrictEqual(res.body.docs.length, 0);
        return done();
      });
  });
  it('should get a resource with id', done => {
    request(server.app)
      .get(`/api/resource/${idResource}`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.notStrictEqual(res.body.docs.length, 0);
        return done();
      });
  });
  it("shouldn't get a resource with id (not found)", done => {
    request(server.app)
      .get(`/api/resource/5c0d5da8fb123c276d502c8c`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(404)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(res.body.success, false);
        return done();
      });
  });
  it('should get a resource with slug', done => {
    request(server.app)
      .get(`/api/resource/${slug}`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.notStrictEqual(res.body.docs.length, 0);
        return done();
      });
  });
  it("shouldn't get a resource with slug (not found)", done => {
    request(server.app)
      .get(`/api/resource/this-slug-didnt-found-because-it-didnt-exist`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(404)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(res.body.success, false);
        return done();
      });
  });
  it('should update a resource with id', done => {
    request(server.app)
      .put(`/api/resource/${idResource}`)
      .send({
        title: 'Unit Test',
        idCategory,
        tags: ['string'],
        shortDescription: 'This short description has been updated',
        mainContent: 'unit test main content',
        image: 'string',
        idLanguage,
        commentIsAuthorize: true,
        isDraft: false
      })
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(res.body.success, true);
        return done();
      });
  });
  it('should add a comment on the resource with id', done => {
    request(server.app)
      .post(`/api/resource/${idResource}/comment`)
      .send({
        text: 'Unit Test comment'
      })
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(res.body.success, true);
        return done();
      });
  });
  it("shouldn't add a comment on the resource who author disabled comment space", done => {
    request(server.app)
      .post(`/api/resource/${idLockResource}/comment`)
      .send({
        text: 'Unit Test comment'
      })
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(500)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(
          res.body.message,
          "Comment isn't authorize for this resource"
        );
        return done();
      });
  });
  it('should create a poll', done => {
    request(server.app)
      .post('/api/resource')
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .send({
        title: 'Unit Test',
        idCategory,
        tags: ['string'],
        shortDescription: 'unit test description',
        mainContent: 'unit test main content',
        image: 'string',
        idLanguage,
        commentIsAuthorize: true,
        isDraft: false,
        surveyTitle: 'Which computer is the best ?',
        surveyAnswers: ['This One', 'The other one']
      })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        idSurvey = res.body.docs._id;
        idAnswer = res.body.docs.survey.choices[0];
        assert.strictEqual(res.body.docs.survey.choices.length, 2);
        return done();
      });
  });
  it('should set an article in publised mode (unit-test feature)', async () => {
    await Resource.findById(idResource, async (err, resource) => {
      resource.set({
        publishedDate: moment.utc()
      });
      await resource.save();
    });
  });
  it('should search an article', async () => {
    const result = await request(server.app)
      .get(`/api/resources?page=1&limit=20&search=unit`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200);
    assert.notStrictEqual(result.body.docs.length, 0);
  });
  it('should search an article but not one found', async () => {
    const result = await request(server.app)
      .get(
        `/api/resources?page=1&limit=20&search=WeDontHaveAnyResourceWithThisName`
      )
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(204);
    assert.strictEqual(result.body.length, undefined);
  });
  it('should create an event', done => {
    request(server.app)
      .post('/api/resource')
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .send({
        title: 'Unit Test',
        idCategory,
        tags: ['string'],
        shortDescription: 'unit test description',
        mainContent: 'unit test main content',
        image: 'string',
        idLanguage,
        commentIsAuthorize: true,
        isDraft: false,
        startDate: moment().subtract(1, 'month'),
        endDate: moment().add(1, 'month')
      })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        idEvent = res.body.docs._id;
        assert.strictEqual(res.body.docs.resourceType, 3);
        return done();
      });
  });
  it('should update an event', done => {
    request(server.app)
      .put(`/api/resource/${idEvent}`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .send({
        title: 'Unit Test coucou',
        startDate: moment().subtract(2, 'month'),
        endDate: moment().add(1, 'month')
      })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(
          moment(res.body.docs.startDate).format('YYYY-MM-DD'),
          moment()
            .subtract(2, 'month')
            .format('YYYY-MM-DD')
        );
        return done();
      });
  });
  it('should set an event in publised mode (unit-test feature)', async () => {
    await Resource.findById(idEvent, async (err, resource) => {
      resource.set({
        publishedDate: moment.utc()
      });
      await resource.save();
    });
  });
  it('should get the events', done => {
    request(server.app)
      .get(`/api/resources?resourceType=3`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.notStrictEqual(moment(res.body.docs.length), 0);
        return done();
      });
  });
  it('should like the resource', done => {
    request(server.app)
      .post(`/api/resource/${idResource}/like`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(res.body.message, 'Like successfully added');
        return done();
      });
  });
  it("shouldn't like the resource because he has already like", done => {
    request(server.app)
      .post(`/api/resource/${idResource}/like`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(500)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(
          res.body.message,
          "You can't like the same article two time"
        );
        return done();
      });
  });
  it('should unlike the resource', done => {
    request(server.app)
      .delete(`/api/resource/${idResource}/like`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(res.body.message, 'Like successfully deleted');
        return done();
      });
  });
  it("shouldn't unlike the resource because there is nothing to delete", done => {
    request(server.app)
      .delete(`/api/resource/${idResource}/like`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.strictEqual(res.body.message, 'There is nothing to delete');
        return done();
      });
  });
  it('should get his resource', done => {
    request(server.app)
      .get(`/api/user/${idUser}/resources`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.notStrictEqual(res.body.totalDocs, 0);
        return done();
      });
  });
  it('should apply to a survey', done => {
    request(server.app)
      .post(`/api/resource/${idSurvey}/answer/${idAnswer}`)
      .set('Accept', 'application/json')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.notStrictEqual(res.body.totalDocs, 0);
        return done();
      });
  });
  // TODO add unit test get and uplaod file
});
