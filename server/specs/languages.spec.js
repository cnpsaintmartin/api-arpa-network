import request from 'supertest';
import assert from 'assert';
import server from '../api';
describe('Languages', function() {
  it('sould retrieve languages list', done => {
    request(server.app)
      .get('/api/languages')
      .set('Accept', 'application/json')
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.notStrictEqual(res.body.docs.length, 0);
        return done();
      });
  });
});
