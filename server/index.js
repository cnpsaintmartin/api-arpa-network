import express from 'express';
import consola from 'consola';
import mailer from './services/mailer';

const app = express();
const host = process.env.HOST || '127.0.0.1';
const port = process.env.PORT || 3030;

async function start() {
  const { default: api } = require('./api');
  api.handler.listen(port, host);
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  });
  return;
}

// Listen the server
start();

export default {
  app,
  close: () => app.close()
};
