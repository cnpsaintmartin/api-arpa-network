import { responseType } from '../../utils/responsesTemplate';
import Sponsor from '../../models/sponsor';
import logger from '../../services/logger/logger';
export default () => {
  const operations = {
    PUT,
    DELETE,
    GET
  };
  async function PUT(req, res) {
    const { id } = req.params;
    const { image, name, shortDescription, website } = req.body;
    Sponsor.findById(id, (err, sponsor) => {
      if (err) {
        return res
          .status(500)
          .json(responseType.error(44004, 'Unable to update sponsor'));
      }
      sponsor.set({
        ...(image ? { image } : ''),
        ...(name ? { name } : ''),
        ...(shortDescription ? { shortDescription } : ''),
        ...(website ? { website } : '')
      });
      sponsor.save((err, updatedSponsor) => {
        if (err) {
          return res
            .status(500)
            .json(responseType.error(44005, 'Unable to update sponsor'));
        }
        return res.status(200).json(
          responseType.success(41006, 'Sponsor successfully updated', {
            docs: updatedSponsor
          })
        );
      });
    });
  }
  async function DELETE(req, res) {
    const { id } = req.params;
    Sponsor.deleteOne({ _id: id }, function(err) {
      if (!err) {
        return res
          .status(200)
          .json(responseType.success(41007, 'Sponsor successfully deleted'));
      } else {
        return res
          .status(500)
          .json(responseType.error(44008, 'Unable to delete sponsor'));
      }
    });
  }
  async function GET(req, res) {
    const { id } = req.params;
    let docs;
    try {
      docs = await Sponsor.findOne({
        _id: id
      });
    } catch (e) {
      logger.error(e);
      if (e.kind === 'ObjectId') {
        return res
          .status(500)
          .json(
            responseType.error(
              44009,
              'One or some id you try to send is bad. Check the format of them and try again'
            )
          );
      }
    }
    if (!docs) {
      return res
        .status(404)
        .json(responseType.error(44010, 'Sponsor not found'));
    }
    return res.status(200).json(
      responseType.success(41011, 'Sponsor successfully found', {
        docs: docs.toObject()
      })
    );
  }
  operations.GET.apiDoc = {
    tags: ['Sponsor'],
    security: [],
    summary: 'Get one sponsor with it ID',
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  operations.PUT.apiDoc = {
    tags: ['Sponsor'],
    summary: 'Update one sponsor with it ID',
    security: [
      {
        Bearer: ['super_admin', 'admin']
      }
    ],
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      },
      {
        in: 'body',
        name: 'sponsor',
        schema: {
          type: 'object',
          properties: {
            image: {
              type: 'string'
            },
            name: {
              type: 'string'
            },
            shortDescription: {
              type: 'string'
            },
            website: {
              type: 'string'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  operations.DELETE.apiDoc = {
    tags: ['Sponsor'],
    summary: 'Delete one sponsor with it ID',
    security: [
      {
        Bearer: ['super_admin', 'admin']
      }
    ],
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
