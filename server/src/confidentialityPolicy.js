import { responseType } from '../utils/responsesTemplate';
import ConfidentialityPolicy from '../models/confidentialityPolicy';
export default () => {
  const operations = {
    PUT,
    GET
  };
  async function PUT(req, res) {
    const { content } = req.body;
    const confidentialityPolicy = await ConfidentialityPolicy.findOne({});
    if (confidentialityPolicy) {
      confidentialityPolicy.set({
        content
      });
      confidentialityPolicy.save((err, updatedPolicy) => {
        if (err) {
          return res
            .status(500)
            .json(
              responseType.error(
                34031,
                'Unable to update confidentiality policy'
              )
            );
        }
        return res.status(200).json(
          responseType.success(
            31032,
            'Confidentiality policy successfully updated',
            {
              docs: updatedPolicy
            }
          )
        );
      });
    } else {
      const newPolicy = new ConfidentialityPolicy({
        content
      });
      newPolicy.save((err, newConfidentialityPolicy) => {
        if (err) {
          return res
            .status(500)
            .json(
              responseType.error(34033, 'Unable to save confidentiality policy')
            );
        }
        return res.status(200).json(
          responseType.success(
            31034,
            'Confidentiality policy successfully saved',
            {
              docs: newConfidentialityPolicy
            }
          )
        );
      });
    }
  }
  async function GET(req, res) {
    const docs = await ConfidentialityPolicy.findOne({});
    if (!docs) {
      return res
        .status(204)
        .json(responseType.error(34035, 'Confidentiality policy not found'));
    }
    return res.status(200).json(
      responseType.success(31036, 'Confidentiality policy successfully found', {
        docs
      })
    );
  }
  operations.GET.apiDoc = {
    tags: ['Usage information'],
    security: [],
    summary: 'Get confidentiality policy',
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  operations.PUT.apiDoc = {
    tags: ['Usage information'],
    summary: 'Update confidentiality policy',
    security: [
      {
        Bearer: ['super_admin', 'admin']
      }
    ],
    parameters: [
      {
        in: 'body',
        name: 'resource',
        schema: {
          type: 'object',
          required: ['content'],
          properties: {
            content: {
              type: 'string'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
