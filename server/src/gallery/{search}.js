import fetch from 'node-fetch';

import { responseType } from '../../utils/responsesTemplate';

export default () => {
  const operations = {
    GET
  };

  async function GET(req, res) {
    const { search } = req.params;
    const result = await fetch(
      `https://api.pexels.com/v1/search?query=${search}&per_page=10&page=1`,
      {
        headers: {
          Authorization:
            '563492ad6f91700001000001f4364df2fa864953be8d34309010ba57'
        }
      }
    ).then(res => res.json());
    return res.status(200).json(
      responseType.success(21004, 'Resources successfully found', {
        docs: result
      })
    );
  }
  operations.GET.apiDoc = {
    tags: ['Resource'],
    security: [],
    summary: 'Get one resource with it ID',
    parameters: [
      {
        name: 'search',
        in: 'path',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
