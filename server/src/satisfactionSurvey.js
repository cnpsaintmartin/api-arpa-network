import { responseType } from '../utils/responsesTemplate';
import SatisfactionSurvey from '../models/satisfactionSurvey';
import User from '../models/user';
import logger from '../services/logger/logger';
import { useCookie } from '../utils/useCookie';
export default () => {
  const operations = {
    POST
  };
  async function POST(req, res) {
    const {
      lang,
      questionOne,
      questionTwo,
      questionThree,
      questionFour,
      questionFive
    } = req.body;
    await useCookie(req);
    if (req.user) {
      const survey = await SatisfactionSurvey.findOne({
        user: req.user._id.toString()
      });
      if (survey === null) {
        const newSatisfactionSurvey = new SatisfactionSurvey({
          lang,
          questionOne,
          questionTwo,
          questionThree,
          questionFour,
          questionFive,
          user: req.user._id.toString()
        });
        await User.updateLastAction(req.user._id.toString());
        newSatisfactionSurvey
          .save()
          .then(docs => {
            return res.status(200).json(
              responseType.success(
                51029,
                'Satisfaction survey successfully registred',
                {
                  docs
                }
              )
            );
          })
          .catch(e => {
            logger.error(e);
            return res
              .status(500)
              .json(responseType.error(54000, 'Internal Server Error'));
          });
      } else {
        return res
          .status(409)
          .json(
            responseType.error(54030, 'You already sent a satisfaction survey')
          );
      }
    } else {
      const newSatisfactionSurvey = new SatisfactionSurvey({
        lang,
        questionOne,
        questionTwo,
        questionThree,
        questionFour,
        questionFive
      });
      newSatisfactionSurvey
        .save()
        .then(docs => {
          return res.status(200).json(
            responseType.success(
              51029,
              'Satisfaction survey successfully registred',
              {
                docs
              }
            )
          );
        })
        .catch(e => {
          logger.error(e);
          return res
            .status(500)
            .json(responseType.error(54000, 'Internal Server Error'));
        });
    }
  }

  operations.POST.apiDoc = {
    tags: ['ARPA Project'],
    summary: 'Add a new satisfaction survey',
    security: [],
    parameters: [
      {
        required: true,
        in: 'body',
        name: 'satisfactionSurvey',
        schema: {
          type: 'object',
          required: [
            'lang',
            'questionOne',
            'questionTwo',
            'questionThree',
            'questionFour'
          ],
          properties: {
            lang: {
              type: 'string'
            },
            questionOne: {
              type: 'integer',
              minimum: 0,
              maximum: 10
            },
            questionTwo: {
              type: 'integer',
              minimum: 0,
              maximum: 10
            },
            questionThree: {
              type: 'boolean'
            },
            questionFour: {
              type: 'integer',
              minimum: 0,
              maximum: 10
            },
            questionFive: {
              type: 'string'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '409': {
        description: 'Already in database'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
