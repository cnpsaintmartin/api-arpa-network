import { responseType } from '../../utils/responsesTemplate';
import Comment from '../../models/comment';
import User from '../../models/user';
import Notification from '../../models/notification';
import logger from '../../services/logger/logger';
import Resource from '../../models/resource';
import { isAuthorized } from '../../utils/authorization';
export default () => {
  const operations = {
    DELETE
  };
  async function DELETE(req, res) {
    const { id } = req.params;
    const comment = await Comment.findById(id);
    if (!comment) {
      return res
        .status(404)
        .json(responseType.success(24061, 'Comment not found'));
    }
    if (comment.author.toString() === req.user._id.toString()) {
      const p1 = Comment.deleteOne({
        _id: id
      });
      const p2 = Resource.removeComment(comment.resource, id);
      await Promise.all([p1, p2]);
      return res
        .status(200)
        .json(responseType.success(21058, 'Comment successfully removed'));
    }
    const resource = await Resource.findById(comment.resource);
    if (!resource) {
      return res
        .status(404)
        .json(
          responseType.success(24062, 'Resource not found, it can be deleted')
        );
    }
    const authorized = await isAuthorized(
      ['committee_member', 'native_committee_member', 'super_admin', 'admin'],
      req.user.roles
    );
    if (resource.author.toString() === req.user._id.toString() || authorized) {
      try {
        const promiseResource = Resource.removeComment(comment.resource, id);
        const promiseComment = Comment.deleteOne({
          _id: id
        });
        const promiseUser = User.addOneReportedComment(comment.author);
        const [resultComment, user, resourceResult] = await Promise.all([
          promiseComment,
          promiseUser,
          promiseResource
        ]);
        if (user.state === 'userBan') {
          return res
            .status(200)
            .json(
              responseType.success(
                21059,
                'Comment successfully removed, the user has been banned'
              )
            );
        }
        if (user.state === 'notBanYet') {
          return res
            .status(200)
            .json(responseType.success(21058, 'Comment successfully removed'));
        }
        if (user.state === 'error') {
          return res
            .status(500)
            .json(
              responseType.error(
                24063,
                'The comment has been deleted but an error has occurred when we try to add one report comment to the user'
              )
            );
        }
      } catch (e) {
        logger.error(e);
        return res
          .status(500)
          .json(responseType.error(24060, 'Unable to delete this comment'));
      }
    }
    return res.status(401).json(responseType.error(24047, 'Access forbidden'));
  }
  operations.DELETE.apiDoc = {
    tags: ['Resource'],
    summary:
      "Remove one comment from resource (only for committee & more and the author or comment's author",
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
