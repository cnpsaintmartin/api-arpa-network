import moment from 'moment';
import { responseType } from '../../../utils/responsesTemplate';
import Resource from '../../../models/resource';
import Notification from '../../../models/notification';
import logger from '../../../services/logger/logger';
import Comment from '../../../models/comment';
import User from '../../../models/user';
export default () => {
  const operations = {
    POST
  };
  async function POST(req, res) {
    const { id } = req.params;
    const { accept, message } = req.body;
    const comment = await Comment.findById(id);
    if (
      !comment ||
      !comment.isReportedBy ||
      (comment.isReportedBy && comment.isReportedBy.length === 0)
    )
      return res
        .status(500)
        .json(
          responseType.error(24044, 'There is no report to accept or reject')
        );
    if (accept) {
      try {
        const authorId = comment.author;
        const p1 = User.addOneReportedComment(comment.author);
        const tabPromise = [];
        comment.isReportedBy.forEach(idUser => {
          const promise = User.addGoodReport(idUser);
          tabPromise.push(promise);
        });
        const promiseComment = Comment.deleteOne({
          _id: id
        });
        const p3 = Resource.removeComment(comment.resource, comment._id);
        await Promise.all([p1, tabPromise, p3, promiseComment]);

        const notificationAuthor = new Notification({
          createdAt: moment.utc(),
          receiver: authorId,
          title: 'Your comment has been removed',
          message,
          displayReadBy: false,
          titleCode: '0020',
          messageCode: '0000',
          type: 'commentDeleted'
        });
        const notificationReporter = new Notification({
          createdAt: moment.utc(),
          receiver: comment.isReportedBy,
          title: 'Your report has been accepted',
          message: 'Thank you for your report, the comment has been removed !',
          displayReadBy: false,
          titleCode: '0007',
          messageCode: '0018',
          type: 'reportAccepted'
        });
        const promise = Notification.deleteOneNotification(
          comment.resource,
          comment._id,
          'commentReported'
        );
        const notification1 = notificationAuthor.save();
        const notification2 = notificationReporter.save();
        await Promise.all([notification1, notification2, promise]);
        return res
          .status(200)
          .json(
            responseType.success(
              21064,
              'Comment successfully removed, a notification has been send to the author and reporter(s)'
            )
          );
      } catch (e) {
        logger.error(e);
        return res
          .status(500)
          .json(responseType.error(24000, 'Internal Server Error'));
      }
    }

    const tabPromise = [];
    comment.isReportedBy.forEach(idUser => {
      const promise = User.revokeModerator(idUser);
      tabPromise.push(promise);
    });
    await Promise.all([tabPromise]);
    comment.isReportedBy = undefined;
    comment.isReportedByModerator = undefined;

    const notificationReporter = new Notification({
      createdAt: moment.utc(),
      receiver: comment.isReportedBy,
      title: 'Your report has been rejected',
      message:
        'Thank you for your report, but the comment respect all our rules. If you are a moderator, your role has been revoked',
      displayReadBy: false,
      titleCode: '0008',
      messageCode: '0019',
      type: 'reportRejected'
    });
    try {
      const promise = Notification.deleteOneNotification(
        comment.resource,
        comment._id,
        'commentReported'
      );
      const prom1 = notificationReporter.save();
      const prom2 = comment.save();
      await Promise.all([prom1, prom2, promise]);
    } catch (e) {
      logger.error(e);
      return res
        .status(500)
        .json(responseType.error(24045, 'Unable to reject the report'));
    }
    return res
      .status(200)
      .json(
        responseType.error(21046, 'The report has been rejected successfully')
      );
  }
  operations.POST.apiDoc = {
    tags: ['Resource'],
    summary: 'Accept or reject one report',
    security: [
      {
        Bearer: ['native_committee_member', 'committee_member']
      }
    ],
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      },
      {
        in: 'body',
        name: 'validation comment report',
        schema: {
          required: ['accept'],
          type: 'object',
          properties: {
            accept: {
              type: 'boolean',
              default: true
            },
            message: {
              type: 'string',
              description:
                'this message will send to the author to explain why his comment has been deleted'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
