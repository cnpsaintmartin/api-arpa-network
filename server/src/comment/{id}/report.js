import moment from 'moment';
import { responseType } from '../../../utils/responsesTemplate';
import Comment from '../../../models/comment';
import User from '../../../models/user';
import Role from '../../../models/role';
import Notification from '../../../models/notification';
import logger from '../../../services/logger/logger';
export default () => {
  const operations = {
    POST
  };
  async function POST(req, res) {
    const { id } = req.params;
    const { message } = req.body;
    let isReportedByModerator = false;
    if (req.user.roles.includes('moderator')) {
      isReportedByModerator = true;
    }
    const result = await Comment.report(
      id,
      req.user._id,
      isReportedByModerator
    );
    if (result === 'sameAuthor') {
      return res
        .status(500)
        .json(responseType.error(24054, "You can't report your comment"));
    }
    if (result === 'error') {
      return res
        .status(500)
        .json(responseType.error(24055, 'Unable to report this comment'));
    }
    if (result === 'notFound') {
      return res
        .status(404)
        .json(responseType.error(24056, 'Unable to find this comment'));
    }
    if (result === 'alreadyReport') {
      return res
        .status(500)
        .json(responseType.error(24057, 'You already report this comment'));
    }
    const tabIdGroup = await Role.getIdWithSlug([
      'committee_member',
      'native_committee_member'
    ]);
    const idUsersPromise = User.getUserByGroup(tabIdGroup);
    const thisCommentPromise = Comment.findById(id);
    const [idUsers, thisComment] = await Promise.all([
      idUsersPromise,
      thisCommentPromise
    ]);
    const newNotification = new Notification({
      createdAt: moment.utc(),
      sender: req.user._id,
      receiver: idUsers,
      title: 'One comment has been reported',
      message,
      displayReadBy: true,
      titleCode: '0015',
      messageCode: '0000',
      type: 'commentReported',
      comment: id,
      resource: thisComment.resource
    });
    try {
      await newNotification.save();
    } catch (e) {
      logger.error(e);
    }
    return res
      .status(200)
      .json(responseType.success(21039, 'Report successfully added'));
  }
  operations.POST.apiDoc = {
    tags: ['Resource'],
    summary: 'Report an existing comment',
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      },
      {
        in: 'body',
        name: 'report message request',
        schema: {
          type: 'object',
          required: ['message'],
          properties: {
            message: {
              description: 'Explain why you report this comment',
              type: 'string'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
