import { responseType } from '../utils/responsesTemplate';
import PrivacyPolicy from '../models/privacyPolicy';
export default () => {
  const operations = {
    PUT,
    GET
  };
  async function PUT(req, res) {
    const { content } = req.body;
    const privacyPolicy = await PrivacyPolicy.findOne({});
    if (privacyPolicy) {
      privacyPolicy.set({
        content
      });
      privacyPolicy.save((err, updatedPrivacyPolicy) => {
        if (err) {
          return res
            .status(500)
            .json(
              responseType.error(
                34013,
                'Unable to update privacy policy notice'
              )
            );
        }
        return res.status(200).json(
          responseType.success(31014, 'Privacy policy successfully updated', {
            docs: updatedPrivacyPolicy
          })
        );
      });
    } else {
      const newPrivacy = new PrivacyPolicy({
        content
      });
      newPrivacy.save((err, newPrivacyPolicy) => {
        if (err) {
          return res
            .status(500)
            .json(responseType.error(34015, 'Unable to save privacy policy'));
        }
        return res.status(200).json(
          responseType.success(31016, 'Privacy policy successfully saved', {
            docs: newPrivacyPolicy
          })
        );
      });
    }
  }
  async function GET(req, res) {
    const docs = await PrivacyPolicy.findOne({});
    if (!docs) {
      return res
        .status(204)
        .json(responseType.error(34017, 'Privacy policy not found'));
    }
    return res.status(200).json(
      responseType.success(31018, 'Privacy policy successfully found', {
        docs
      })
    );
  }
  operations.GET.apiDoc = {
    tags: ['Usage information'],
    security: [],
    summary: 'Get privacy policy',
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  operations.PUT.apiDoc = {
    tags: ['Usage information'],
    summary: 'Update privacy policy',
    security: [
      {
        Bearer: ['super_admin', 'admin']
      }
    ],
    parameters: [
      {
        in: 'body',
        name: 'resource',
        schema: {
          type: 'object',
          required: ['content'],
          properties: {
            content: {
              type: 'string'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
