import Country from '../models/country';
import { responseType } from '../utils/responsesTemplate';

export default () => {
  const operations = {
    GET
  };
  async function GET(req, res) {
    const docs = await Country.find({});
    if (!docs) {
      return res
        .status(204)
        .json(responseType.error(14008, 'Countries not found'));
    }
    return res
      .status(200)
      .json(
        responseType.success(11007, 'Countries successfully found', { docs })
      );
  }
  operations.GET.apiDoc = {
    tags: ['Country'],
    security: [],
    summary: 'Get available countries',
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
