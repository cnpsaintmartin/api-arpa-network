import { responseType } from '../../utils/responsesTemplate';
import ProjectReport from '../../models/projectReport';
import logger from '../../services/logger/logger';
export default () => {
  const operations = {
    PUT,
    DELETE,
    GET
  };
  async function PUT(req, res) {
    const { id } = req.params;
    const { title, mainContent, image, isInNavbar, attachments } = req.body;
    ProjectReport.findById(id, (err, report) => {
      if (err) {
        return res
          .status(500)
          .json(responseType.error(54021, 'Unable to update project report'));
      }
      report.set({
        ...(title ? { title } : ''),
        ...(mainContent ? { mainContent } : ''),
        ...(image ? { image } : ''),
        ...(typeof isInNavbar === 'boolean' ? { isInNavbar } : ''),
        attachments
      });
      report.save((err, updatedReport) => {
        if (err) {
          return res
            .status(500)
            .json(responseType.error(54022, 'Unable to update project report'));
        }
        return res.status(200).json(
          responseType.success(51023, 'Project report successfully updated', {
            docs: updatedReport
          })
        );
      });
    });
  }
  async function DELETE(req, res) {
    const { id } = req.params;
    ProjectReport.deleteOne({ _id: id }, function(err) {
      if (!err) {
        return res
          .status(200)
          .json(
            responseType.success(51024, 'Project report successfully deleted')
          );
      } else {
        return res
          .status(500)
          .json(responseType.error(54025, 'Unable to delete Project report'));
      }
    });
  }
  async function GET(req, res) {
    const { id } = req.params;
    let docs;
    try {
      docs = await ProjectReport.findOne({
        _id: id
      });
    } catch (e) {
      logger.error(e);
      if (e.kind === 'ObjectId') {
        return res
          .status(500)
          .json(
            responseType.error(
              54026,
              'One or some id you try to send is bad. Check the format of them and try again'
            )
          );
      }
    }
    if (!docs) {
      return res
        .status(404)
        .json(responseType.error(54027, 'Project report not found'));
    }
    return res.status(200).json(
      responseType.success(51028, 'Project report successfully found', {
        docs: docs.toObject()
      })
    );
  }
  operations.GET.apiDoc = {
    tags: ['ARPA Project'],
    security: [],
    summary: 'Get one project report with it ID',
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  operations.PUT.apiDoc = {
    tags: ['ARPA Project'],
    summary: 'Update one project report with it ID',
    security: [
      {
        Bearer: ['super_admin', 'admin']
      }
    ],
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      },
      {
        in: 'body',
        name: 'project report',
        schema: {
          type: 'object',
          properties: {
            title: {
              type: 'string'
            },
            mainContent: {
              type: 'string'
            },
            image: {
              type: 'string'
            },
            isInNavbar: {
              type: 'boolean'
            },
            attachments: {
              type: 'array'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  operations.DELETE.apiDoc = {
    tags: ['ARPA Project'],
    summary: 'Delete one project report with it ID',
    security: [
      {
        Bearer: ['super_admin', 'admin']
      }
    ],
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
