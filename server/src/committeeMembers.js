import User from '../models/user';
import Role from '../models/role';
import { responseType } from '../utils/responsesTemplate';
import Resource from '../models/resource';

export default () => {
  const operations = {
    GET
  };
  async function GET(req, res) {
    const { page, limit } = req.query;
    const idRoles = await Role.getIdWithSlug([
      'committee_member',
      'native_committee_member'
    ]);
    const docs = await User.paginate(
      { roles: { $in: idRoles } },
      {
        select: '_id firstname lastname',
        limit,
        page
      }
    );
    if (docs.totalDocs === 0) {
      return res
        .status(204)
        .json(responseType.error(14029, "Committee's members not found"));
    }
    return res
      .status(200)
      .json(
        responseType.success(
          11030,
          "Committee's members successfully found",
          docs
        )
      );
  }
  operations.GET.apiDoc = {
    tags: ['User'],
    summary: "Get all committee's members",
    parameters: [
      {
        name: 'page',
        in: 'query',
        required: false,
        type: 'integer',
        default: 1
      },
      {
        name: 'limit',
        in: 'query',
        required: false,
        type: 'integer',
        default: 25
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
