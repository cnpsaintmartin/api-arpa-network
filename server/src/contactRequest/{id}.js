import moment from 'moment';
import { responseType } from '../../utils/responsesTemplate';
import Notification from '../../models/notification';
import User from '../../models/user';
import logger from '../../services/logger/logger';
export default () => {
  const operations = {
    POST
  };
  async function POST(req, res) {
    const { message } = req.body;
    const { id } = req.params;
    if (id.toString() === req.user._id) {
      return res
        .status(500)
        .json(
          responseType.success(14037, "Sorry, but you can't be your friend")
        );
    }
    const userWant = await User.findById(id);
    if (!userWant.isAgreeWithContactRequest) {
      return res
        .status(401)
        .json(
          responseType.success(
            74008,
            "This user didn't want to share his contact information"
          )
        );
    }
    const isInHisNetworkPromise = User.isInMyNetwork(req.user._id, id);
    const contactRequestAlreadySentPromise = Notification.checkIfContactRequestAlreadySent(
      id,
      req.user._id
    );
    const [isInHisNetwork, contactRequestAlreadySent] = await Promise.all([
      isInHisNetworkPromise,
      contactRequestAlreadySentPromise
    ]);
    if (isInHisNetwork) {
      return res
        .status(500)
        .json(
          responseType.error(74015, 'This person is already in your network')
        );
    }
    if (contactRequestAlreadySent) {
      return res
        .status(500)
        .json(
          responseType.error(
            74016,
            'You already sent a contact request to this person, please wait her answer'
          )
        );
    }
    let notification = new Notification({
      createdAt: moment.utc(),
      title: 'Contact request',
      sender: req.user._id,
      receiver: id,
      message,
      messageCode: '0000',
      titleCode: '0004',
      type: 'contactRequest'
    });
    notification
      .save()
      .then(docs => {
        return res.status(200).json(
          responseType.success(71001, 'Notification correctly send', {
            docs
          })
        );
      })
      .catch(e => {
        logger.error(e);
        return res
          .status(500)
          .json(responseType.error(74000, 'Internal Server Error'));
      });
  }
  operations.POST.apiDoc = {
    tags: ['Notification'],
    summary: 'Send a contact request',
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string',
        description: 'user ID'
      },
      {
        required: true,
        in: 'body',
        name: 'notification',
        schema: {
          type: 'object',
          required: ['message'],
          properties: {
            message: {
              type: 'string'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
