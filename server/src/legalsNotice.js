import { responseType } from '../utils/responsesTemplate';
import LegalsNotice from '../models/legalsNotice';
export default () => {
  const operations = {
    PUT,
    GET
  };
  async function PUT(req, res) {
    const { content } = req.body;
    const legalsNotice = await LegalsNotice.findOne({});
    if (legalsNotice) {
      legalsNotice.set({
        content
      });
      legalsNotice.save((err, updatedLegalsNotice) => {
        if (err) {
          return res
            .status(500)
            .json(responseType.error(34003, 'Unable to update legals notice'));
        }
        return res.status(200).json(
          responseType.success(31004, 'Legals notice successfully updated', {
            docs: updatedLegalsNotice
          })
        );
      });
    } else {
      const newLegals = new LegalsNotice({
        content
      });
      newLegals.save((err, newLegalsNotice) => {
        if (err) {
          return res
            .status(500)
            .json(responseType.error(34006, 'Unable to save legals notice'));
        }
        return res.status(200).json(
          responseType.success(31005, 'Legals notice successfully saved', {
            docs: newLegalsNotice
          })
        );
      });
    }
  }
  async function GET(req, res) {
    const docs = await LegalsNotice.findOne({});
    if (!docs) {
      return res
        .status(204)
        .json(responseType.error(34001, 'Legals notice not found'));
    }
    return res.status(200).json(
      responseType.success(31002, 'Legals notice successfully found', {
        docs
      })
    );
  }
  operations.GET.apiDoc = {
    tags: ['Usage information'],
    security: [],
    summary: 'Get legals notice',
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  operations.PUT.apiDoc = {
    tags: ['Usage information'],
    summary: 'Update legals notice',
    security: [
      {
        Bearer: ['super_admin', 'admin']
      }
    ],
    parameters: [
      {
        in: 'body',
        name: 'resource',
        schema: {
          type: 'object',
          required: ['content'],
          properties: {
            content: {
              type: 'string'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
