import { responseType } from '../utils/responsesTemplate';
import ProjectMember from '../models/projectMember';
export default () => {
  const operations = {
    GET
  };
  async function GET(req, res) {
    const docs = await ProjectMember.find({});
    if (docs.length === 0) {
      return res
        .status(204)
        .json(responseType.error(54007, 'Project members not found'));
    }
    return res.status(200).json(
      responseType.success(51008, 'Project members successfully found', {
        docs
      })
    );
  }
  operations.GET.apiDoc = {
    tags: ['ARPA Project'],
    security: [],
    summary: 'Get all project members',
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
