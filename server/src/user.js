import User from '../models/user';
import Role from '../models/role';
import { responseType } from '../utils/responsesTemplate';
import Notification from '../models/notification';
import logger from '../services/logger/logger';
import mailer from '../services/mailer';
import moment from 'moment';

export default () => {
  const operations = {
    POST,
    GET
  };
  async function GET(req, res) {
    const user = await User.findOne({
      _id: req.user._id
    })
      .select('-password')
      .populate('roles')
      .populate('spokenLanguages');
    return res
      .status(200)
      .json(responseType.success(11005, 'User correctly find', { docs: user }));
  }
  async function POST(req, res) {
    const {
      lastname,
      firstname,
      email,
      password,
      isProfessional,
      isAgreeAppearProfessionalContactBook,
      isAgreeWithContactRequest,
      description,
      positionHeld,
      country,
      spokenLanguages,
      professionalDocument
    } = req.body;
    if (
      isProfessional &&
      (!professionalDocument || professionalDocument === '')
    ) {
      return res
        .status(500)
        .json(
          responseType.error(
            14003,
            'You must upload one proof who show us you are a professional'
          )
        );
    }
    let docs;
    let user = new User({
      lastname,
      firstname,
      email,
      password,
      isAgreeAppearProfessionalContactBook,
      isAgreeWithContactRequest,
      description,
      positionHeld,
      country,
      spokenLanguages,
      professionalDocument
    });
    try {
      docs = await user.save();
      docs = docs.toObject();
      delete docs.password;
      if (isProfessional) {
        const tabIdGroup = await Role.getIdWithSlug([
          'committee_member',
          'native_committee_member'
        ]);
        const idUsers = await User.getUserByGroup(tabIdGroup);
        let notification = new Notification({
          createdAt: moment.utc(),
          title: 'Professional member request',
          sender: docs._id,
          receiver: idUsers,
          message:
            'One user wants to be a professional in ARPA ageing. Please look his proof and accept or reject his request',
          messageCode: '0012',
          titleCode: '0012',
          type: 'professionalMemberRequest'
        });
        await notification.save();
      }
      await mailer.send(
        user.email,
        'Welcome to ARPA Ageing',
        { name: user.firstname, customerEmail: user.email },
        'welcome'
      );
      return res
        .status(200)
        .json(
          responseType.success(11004, 'User correctly registered', { docs })
        );
    } catch (e) {
      logger.error(e);
      if (docs._id) {
        await User.deleteOne({ _id: docs._id.toString() });
      }
      if (e.errors && e.errors.email)
        return res
          .status(500)
          .json(responseType.error(14003, 'Email is already used'));
      else {
        return res
          .status(500)
          .json(responseType.error(14000, 'Internal Server Error'));
      }
    }
  }
  operations.POST.apiDoc = {
    security: [],
    tags: ['User'],
    summary: 'Add a new user',
    parameters: [
      {
        required: true,
        in: 'body',
        name: 'user',
        schema: {
          $ref: '#/definitions/User'
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  operations.GET.apiDoc = {
    tags: ['User'],
    // security: [
    //   {
    //     Bearer: ['admin']
    //   }
    // ],
    summary: "Get connected user's object",
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
