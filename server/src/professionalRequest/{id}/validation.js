import moment from 'moment';
import { responseType } from '../../../utils/responsesTemplate';
import Notification from '../../../models/notification';
import Role from '../../../models/role';
import User from '../../../models/user';
import logger from '../../../services/logger/logger';
import { removeFile } from '../../../utils/removeFile';

export default () => {
  const operations = {
    POST
  };
  async function POST(req, res) {
    const { id } = req.params;
    const { accept } = req.body;
    const professionalRequest = await Notification.findById(id);
    if (!professionalRequest) {
      return res
        .status(404)
        .json(
          responseType.error(74027, 'Professional member request not found')
        );
    }
    const user = await User.findById(professionalRequest.sender);
    if (!user.professionalDocument) {
      return res
        .status(500)
        .json(
          responseType.error(
            74028,
            "This user didn't upload professional proof"
          )
        );
    }
    if (!accept) {
      try {
        const result = await removeFile(user.professionalDocument);
        if (result === 'error') {
          return res
            .status(500)
            .json(
              responseType.error(
                64005,
                'Unable to remove professional document from database, try again'
              )
            );
        }
        const notification = new Notification({
          createdAt: moment.utc(),
          receiver: user._id,
          title: 'Professional member request refused',
          message:
            'Your professional member request has been refused by the committee. You can try to send an other proof in your profile page, if you want to become a professional in ARPA !',
          messageCode: '0014',
          titleCode: '0014',
          type: 'professionalMemberRefused'
        });
        user.professionalDocument = undefined;
        const promiseUser = user.save();
        const promiseNotification = notification.save();
        const notificationDeletePromise = Notification.deleteOne({
          _id: professionalRequest._id
        });
        await Promise.all([
          promiseUser,
          promiseNotification,
          notificationDeletePromise
        ]);
        return res
          .status(200)
          .json(
            responseType.error(
              11036,
              'Thank you, the member has been rejected. A notification has been send'
            )
          );
      } catch (e) {
        logger.error(e);
        return res
          .status(500)
          .json(
            responseType.error(14035, 'Unable to reject user as professional')
          );
      }
    }
    const idProfessional = await Role.getIdWithSlug(['professional']);
    user.roles.push(idProfessional);
    const notification = new Notification({
      createdAt: moment.utc(),
      receiver: user._id,
      title: 'Professional member request accepted',
      message:
        'Your professional member request has been accepted by the committee Thank you for reconnecting to fully benefit from your new role, professional',
      messageCode: '0013',
      titleCode: '0013',
      type: 'professionalMemberAccepted'
    });
    const result = await removeFile(user.professionalDocument);
    user.professionalDocument = undefined;
    if (result === 'error') {
      return res
        .status(500)
        .json(
          responseType.error(
            64004,
            'Unable to remove professional document from database but the member has been promoted professional and a notification has been sent'
          )
        );
    }
    try {
      const promiseUser = user.save();
      const promiseNotification = notification.save();
      const notificationDeletePromise = Notification.deleteOne({
        _id: professionalRequest._id
      });
      await Promise.all([
        promiseUser,
        promiseNotification,
        notificationDeletePromise
      ]);
    } catch (e) {
      logger.error(e);
      return res
        .status(500)
        .json(
          responseType.error(14033, 'Unable to promote user as professional')
        );
    }
    return res
      .status(200)
      .json(
        responseType.error(
          11034,
          'Thank you, the member has been promoted professional. A notification has been send'
        )
      );
  }
  operations.POST.apiDoc = {
    tags: ['User'],
    summary: 'Accept or reject one professional member request',
    security: [
      {
        Bearer: ['native_committee_member', 'committee_member']
      }
    ],
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      },
      {
        in: 'body',
        name: 'professional member request',
        schema: {
          required: ['accept'],
          type: 'object',
          properties: {
            accept: {
              type: 'boolean',
              default: true
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
