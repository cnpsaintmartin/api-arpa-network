import { responseType } from '../../utils/responsesTemplate';
import Resource from '../../models/resource';
import logger from '../../services/logger/logger';
export default () => {
  const operations = {
    GET
  };
  async function GET(req, res) {
    const { resourceStatus, isReported } = req.query;
    let { limit, page } = req.query;
    if (!limit) limit = 25;
    if (!page) page = 1;
    let query = {
      $or: [
        { deletedAt: { $exists: false } },
        { deletedAt: { $exists: true, $eq: null } }
      ],
      isDraft: false,
      ...(resourceStatus ? { resourceStatus } : ''),
      ...(isReported
        ? {
            'isReportedBy.0': { $exists: true }
          }
        : '')
    };
    let docs;
    try {
      docs = await Resource.paginate(
        {
          ...query
        },
        {
          lean: true,
          leanWithId: false,
          select: '-likes',
          sort: {
            updatedAt: 'desc'
          },
          limit,
          page,
          populate: [
            {
              path: 'author',
              select: ['firstname', 'lastname']
            },
            {
              path: 'category',
              select: ['name']
            },
            {
              path: 'tags'
            }
          ]
        }
      );
    } catch (e) {
      logger.error(e);
      if (e.kind === 'ObjectId') {
        return res
          .status(500)
          .json(
            responseType.error(
              24018,
              'One or some id you try to send is bad. Check the format of them and try again'
            )
          );
      }
      return res
        .status(500)
        .json(responseType.error(24000, 'Internal Server Error'));
    }
    if (docs.totalDocs === 0) {
      return res
        .status(204)
        .json(responseType.error(24016, 'Resources not found'));
    }
    return res
      .status(200)
      .json(responseType.success(21003, 'Resources successfully found', docs));
  }
  operations.GET.apiDoc = {
    tags: ['Resource', 'Committee'],
    security: [
      {
        Bearer: [
          'committee_member',
          'super_admin',
          'admin',
          'native_committee_member'
        ]
      }
    ],
    summary: 'Get all paginate resources',
    parameters: [
      {
        name: 'page',
        in: 'query',
        required: false,
        type: 'integer',
        default: 1
      },
      {
        name: 'limit',
        in: 'query',
        required: false,
        type: 'integer',
        default: 25,
        maximum: 250
      },
      {
        name: 'resourceStatus',
        in: 'query',
        required: false,
        type: 'integer'
      },
      {
        name: 'isReported',
        in: 'query',
        required: false,
        type: 'boolean'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
