import { responseType } from '../../utils/responsesTemplate';
import SatisfactionSurvey from '../../models/satisfactionSurvey';
import Comment from '../../models/comment';
import User from '../../models/user';
import Resource from '../../models/resource';
import Role from '../../models/role';
export default () => {
  const operations = {
    GET
  };
  async function GET(req, res) {
    const numberOfSurveys = await SatisfactionSurvey.countDocuments();
    const satisfactionSurveys = await SatisfactionSurvey.find();
    let sumQuestionOne = 0;
    let sumQuestionTwo = 0;
    let sumQuestionThreeTrue = 0;
    let sumQuestionThreeFalse = 0;
    let sumQuestionFour = 0;
    satisfactionSurveys.forEach(satisfaction => {
      sumQuestionOne = sumQuestionOne + satisfaction.questionOne;
      sumQuestionTwo = sumQuestionTwo + satisfaction.questionTwo;
      if (satisfaction.questionThree === true) {
        sumQuestionThreeTrue = sumQuestionThreeTrue + 1;
      } else {
        sumQuestionThreeFalse = sumQuestionThreeFalse + 1;
      }
      sumQuestionFour = sumQuestionFour + satisfaction.questionFour;
    });

    const numberOfCommentsPromise = Comment.countDocuments();
    const numberOfRegisteredMembersPromise = User.countDocuments();
    const numberOfUserMembersPromise = User.countDocuments({
      roles: { $size: 0 }
    });
    const numberOfResourcesPromise = Resource.countDocuments();

    // nombre d'article
    const numberOfResourcesArticlePromise = Resource.countDocuments({
      resourceType: 1
    });
    // nombre de sondage
    const numberOfResourcesSurveyPromise = Resource.countDocuments({
      resourceType: 2
    });
    // nombre d'évènement
    const numberOfResourcesEventPromise = Resource.countDocuments({
      resourceType: 3
    });

    // nombre de ressource en attente d'approval
    const numberOfResourcesWaitingCommitteeValidationPromise = Resource.countDocuments(
      {
        $or: [{ resourceStatus: 1 }, { resourceStatus: 4 }]
      }
    );
    // nombre de ressource rejected
    const numberOfResourcesRejectedPromise = Resource.countDocuments({
      $or: [{ resourceStatus: 2 }, { resourceStatus: 5 }]
    });
    // nombre de ressource approved
    const numberOfResourcesApprovedPromise = Resource.countDocuments({
      $or: [{ resourceStatus: 3 }]
    });
    const idUsersCommitteePromise = User.getUserByGroup(
      await Role.getIdWithSlug(['committee_member', 'native_committee_member'])
    );
    const idUsersProfessionalPromise = User.getUserByGroup(
      await Role.getIdWithSlug(['professional'])
    );

    const numberOfUsersProfessionalBookPromise = User.countDocuments({
      isAgreeAppearProfessionalContactBook: true
    });
    const numberOfUsersContactRequestPromise = User.countDocuments({
      isAgreeWithContactRequest: true
    });

    const [
      numberOfComments,
      numberOfRegisteredMembers,
      numberOfUserMembers,
      numberOfResources,
      numberOfResourcesArticle,
      numberOfResourcesSurvey,
      numberOfResourcesEvent,
      numberOfResourcesWaitingCommitteeValidation,
      numberOfResourcesRejected,
      numberOfResourcesApproved,
      idUsersCommittee,
      idUsersProfessional,
      numberOfUsersProfessionalBook,
      numberOfUsersContactRequest
    ] = await Promise.all([
      numberOfCommentsPromise,
      numberOfRegisteredMembersPromise,
      numberOfUserMembersPromise,
      numberOfResourcesPromise,
      numberOfResourcesArticlePromise,
      numberOfResourcesSurveyPromise,
      numberOfResourcesEventPromise,
      numberOfResourcesWaitingCommitteeValidationPromise,
      numberOfResourcesRejectedPromise,
      numberOfResourcesApprovedPromise,
      idUsersCommitteePromise,
      idUsersProfessionalPromise,
      numberOfUsersProfessionalBookPromise,
      numberOfUsersContactRequestPromise
    ]);

    return res.status(200).json(
      responseType.success(71000, 'Stats successfully retrieve', {
        docs: {
          numberOfSurveys,
          sumQuestionOne,
          sumQuestionTwo,
          sumQuestionThreeFalse,
          sumQuestionThreeTrue,
          sumQuestionFour,
          numberOfComments,
          numberOfRegisteredMembers,
          numberOfUserMembers,
          numberOfResources,
          numberOfCommitteeMembers: idUsersCommittee.length,
          numberOfProfessionalMembers: idUsersProfessional.length,
          numberOfResourcesWaitingCommitteeValidation,
          numberOfResourcesRejected,
          numberOfResourcesApproved,
          numberOfResourcesArticle,
          numberOfResourcesSurvey,
          numberOfResourcesEvent,
          numberOfUsersProfessionalBook,
          numberOfUsersContactRequest
        }
      })
    );
  }
  operations.GET.apiDoc = {
    tags: ['Committee'],
    summary: 'Get statistics about arpa-ageing.eu',
    security: [
      {
        Bearer: [
          'admin',
          'super_admin',
          'committee_member',
          'native_committee_member'
        ]
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      }
    }
  };
  return operations;
};
