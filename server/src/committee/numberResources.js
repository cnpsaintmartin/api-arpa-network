import { responseType } from '../../utils/responsesTemplate';
import Resource from '../../models/resource';
import logger from '../../services/logger/logger';
export default () => {
  const operations = {
    GET
  };
  async function GET(req, res) {
    const { status } = req.query;
    let numberOfResources;
    try {
      numberOfResources = await Resource.countDocuments({
        isDraft: false,
        ...(status ? { resourceStatus: { $in: status } } : ''),
        $or: [
          { deletedAt: { $exists: false } },
          { deletedAt: { $exists: true, $eq: null } }
        ]
      });
    } catch (e) {
      logger.error(e);
      return res
        .status(500)
        .json(responseType.error(24000, 'Internal Server Error'));
    }
    return res.status(200).json(
      responseType.success(21053, 'Number of resource successfully found', {
        docs: { numberOfResources }
      })
    );
  }
  operations.GET.apiDoc = {
    tags: ['Committee'],
    summary: 'Get number of resources with it status',
    security: [
      {
        Bearer: ['native_committee_member', 'committee_member']
      }
    ],
    parameters: [
      {
        name: 'status',
        in: 'query',
        required: false,
        type: 'array',
        items: {
          type: 'string'
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
