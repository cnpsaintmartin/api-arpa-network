import { responseType } from '../../utils/responsesTemplate';
import Comment from '../../models/comment';
import logger from '../../services/logger/logger';
export default () => {
  const operations = {
    GET
  };
  async function GET(req, res) {
    const { isReported } = req.query;
    let { limit, page } = req.query;
    if (!limit) limit = 25;
    if (!page) page = 1;
    let query = {
      ...(isReported
        ? {
            'isReportedBy.0': { $exists: true }
          }
        : '')
    };
    let docs;
    try {
      docs = await Comment.paginate(
        {
          ...query
        },
        {
          lean: true,
          sort: { updatedAt: 'asc' },
          limit,
          page,
          populate: [
            {
              path: 'author',
              select: ['firstname', 'lastname', 'roles'],
              populate: [
                {
                  path: 'roles'
                }
              ]
            },
            {
              path: 'tags'
            }
          ]
        }
      );
    } catch (e) {
      logger.error(e);
      return res
        .status(500)
        .json(responseType.error(24000, 'Internal Server Error'));
    }
    if (docs.totalDocs === 0) {
      return res
        .status(204)
        .json(responseType.error(24065, 'Comments not found'));
    }
    return res
      .status(200)
      .json(responseType.success(21066, 'Resources successfully found', docs));
  }
  operations.GET.apiDoc = {
    tags: ['Resource', 'Committee'],
    security: [
      {
        Bearer: [
          'committee_member',
          'super_admin',
          'admin',
          'native_committee_member'
        ]
      }
    ],
    summary: 'Get all paginate comments',
    parameters: [
      {
        name: 'page',
        in: 'query',
        required: false,
        type: 'integer',
        default: 1
      },
      {
        name: 'limit',
        in: 'query',
        required: false,
        type: 'integer',
        default: 25,
        maximum: 250
      },
      {
        name: 'isReported',
        in: 'query',
        required: false,
        type: 'boolean'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
