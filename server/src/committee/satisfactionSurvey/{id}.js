import { responseType } from '../../../utils/responsesTemplate';
import SatisfactionSurvey from '../../../models/satisfactionSurvey';
export default () => {
  const operations = {
    GET
  };
  async function GET(req, res) {
    let { id } = req.params;
    const docs = await SatisfactionSurvey.findOne({ _id: id });
    if (docs.length === 0) {
      return res
        .status(404)
        .json(responseType.error(74010, 'Satisfaction survey not found'));
    }
    return res.status(200).json(
      responseType.success(71009, 'Satisfaction survey successfully found', {
        docs
      })
    );
  }
  operations.GET.apiDoc = {
    tags: ['User'],
    summary: 'Get one satisfaction survey with its id',
    security: [
      {
        Bearer: ['native_committee_member', 'committee_member']
      }
    ],
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '404': {
        description: 'Resource not found'
      }
    }
  };
  return operations;
};
