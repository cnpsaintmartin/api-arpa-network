import { responseType } from '../../utils/responsesTemplate';
import Resource from '../../models/resource';
import Comment from '../../models/comment';
import logger from '../../services/logger/logger';
export default () => {
  const operations = {
    GET
  };
  async function GET(req, res) {
    const { type } = req.query;
    let numberOfResources = 0;
    if (type === 'comment') {
      try {
        numberOfResources = await Comment.countDocuments({
          'isReportedBy.0': { $exists: true },
          $or: [
            { deletedAt: { $exists: false } },
            { deletedAt: { $exists: true, $eq: null } }
          ]
        });
      } catch (e) {
        logger.error(e);
        return res
          .status(500)
          .json(responseType.error(24000, 'Internal Server Error'));
      }
    } else if (type === 'resource') {
      try {
        numberOfResources = await Resource.countDocuments({
          'isReportedBy.0': { $exists: true },
          $or: [
            { deletedAt: { $exists: false } },
            { deletedAt: { $exists: true, $eq: null } }
          ]
        });
      } catch (e) {
        logger.error(e);
        return res
          .status(500)
          .json(responseType.error(24000, 'Internal Server Error'));
      }
    } else {
      const numberResourcePromise = Resource.countDocuments({
        'isReportedBy.0': { $exists: true },
        $or: [
          { deletedAt: { $exists: false } },
          { deletedAt: { $exists: true, $eq: null } }
        ]
      });
      const numberCommentPromise = Comment.countDocuments({
        'isReportedBy.0': { $exists: true },
        $or: [
          { deletedAt: { $exists: false } },
          { deletedAt: { $exists: true, $eq: null } }
        ]
      });
      const [numberResource, numberComment] = await Promise.all([
        numberResourcePromise,
        numberCommentPromise
      ]);
      numberOfResources = numberResource + numberComment;
    }

    return res.status(200).json(
      responseType.success(21053, 'Number of report successfully found', {
        docs: { numberOfResources }
      })
    );
  }
  operations.GET.apiDoc = {
    tags: ['Committee'],
    summary: 'Get number of resources with it status',
    security: [
      {
        Bearer: ['native_committee_member', 'committee_member']
      }
    ],
    parameters: [
      {
        name: 'type',
        in: 'query',
        required: false,
        type: 'string',
        description: 'comment || resource'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
