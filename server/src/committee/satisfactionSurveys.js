import { responseType } from '../../utils/responsesTemplate';
import SatisfactionSurvey from '../../models/satisfactionSurvey';
export default () => {
  const operations = {
    GET
  };
  async function GET(req, res) {
    const { page, limit } = req.query;
    const docs = await SatisfactionSurvey.paginate(
      {},
      {
        lean: true,
        limit,
        page
      }
    );
    if (docs.totalDocs === 0) {
      return res
        .status(204)
        .json(responseType.error(71001, 'No satisfaction survey found'));
    }
    return res.status(200).json(
      responseType.success(71002, 'Satisfactions surveys successfully found', {
        docs
      })
    );
  }
  operations.GET.apiDoc = {
    tags: ['Committee'],
    summary: 'Get satisfaction survey result',
    security: [
      {
        Bearer: [
          'admin',
          'super_admin',
          'committee_member',
          'native_committee_member'
        ]
      }
    ],
    parameters: [
      {
        name: 'page',
        in: 'query',
        required: false,
        type: 'integer',
        default: 1
      },
      {
        name: 'limit',
        in: 'query',
        required: false,
        type: 'integer',
        maximum: 25,
        default: 25
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
