import { responseType } from '../utils/responsesTemplate';
import CommitteeCodeConduct from '../models/committeeCodeConduct';
export default () => {
  const operations = {
    PUT,
    GET
  };
  async function PUT(req, res) {
    const { content } = req.body;
    const committeeCodeConduct = await CommitteeCodeConduct.findOne({});
    if (committeeCodeConduct) {
      committeeCodeConduct.set({
        content
      });
      committeeCodeConduct.save((err, updatedCodeConduct) => {
        if (err) {
          return res
            .status(500)
            .json(
              responseType.error(
                34025,
                'Unable to update committee code conduct'
              )
            );
        }
        return res.status(200).json(
          responseType.success(
            31026,
            'Committee code conduct successfully updated',
            {
              docs: updatedCodeConduct
            }
          )
        );
      });
    } else {
      const newCommitteeCodeConduct = new CommitteeCodeConduct({
        content
      });
      newCommitteeCodeConduct.save((err, newCommitteeCode) => {
        if (err) {
          return res
            .status(500)
            .json(
              responseType.error(34027, 'Unable to save committee code conduct')
            );
        }
        return res.status(200).json(
          responseType.success(
            31028,
            'Committee code conduct successfully saved',
            {
              docs: newCommitteeCode
            }
          )
        );
      });
    }
  }
  async function GET(req, res) {
    const docs = await CommitteeCodeConduct.findOne({});
    if (!docs) {
      return res
        .status(204)
        .json(responseType.error(34029, 'Committee code conduct not found'));
    }
    return res.status(200).json(
      responseType.success(31030, 'Committee code conduct successfully found', {
        docs
      })
    );
  }
  operations.GET.apiDoc = {
    tags: ['Usage information'],
    security: [],
    summary: 'Get committee code conduct',
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  operations.PUT.apiDoc = {
    tags: ['Usage information'],
    summary: 'Update committee code conduct',
    security: [
      {
        Bearer: ['super_admin', 'admin']
      }
    ],
    parameters: [
      {
        in: 'body',
        name: 'resource',
        schema: {
          type: 'object',
          required: ['content'],
          properties: {
            content: {
              type: 'string'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
