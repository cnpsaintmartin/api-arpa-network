import { responseType } from '../utils/responsesTemplate';
import User from '../models/user';
import Role from '../models/role';
import Notification from '../models/notification';
import logger from '../services/logger/logger';
export default () => {
  const operations = {
    GET
  };
  async function GET(req, res) {
    let { limit, page } = req.query;
    if (!limit) limit = 25;
    if (!page) page = 1;
    let docs;
    const idProfessional = await Role.getIdWithSlug('professional');
    let query = {
      roles: { $in: idProfessional },
      isAgreeAppearProfessionalContactBook: true
    };
    try {
      docs = await User.paginate(
        {
          ...query
        },
        {
          lean: true,
          select:
            '_id isAgreeWithContactRequest firstname lastname spokenLanguages positionHeld country roles',
          sort: { lastname: 'asc' },
          limit,
          page,
          populate: [
            {
              path: 'spokenLanguages'
            },
            {
              path: 'country'
            },
            {
              path: 'roles'
            }
          ]
        }
      );
    } catch (e) {
      logger.error(e);
      return res
        .status(500)
        .json(responseType.error(14000, 'Internal Server Error'));
    }
    if (docs.totalDocs === 0) {
      return res
        .status(204)
        .json(responseType.error(14023, 'No professional found'));
    }
    // Delete user from this list
    let userObjectIndex = -1;
    userObjectIndex = docs.docs.findIndex(
      d => d._id.toString() === req.user._id.toString()
    );
    if (userObjectIndex !== -1) {
      docs.docs.splice(userObjectIndex, 1);
      docs.totalDocs -= 1;
    }
    for (let i = 0; i < docs.totalDocs; i++) {
      const isInHisNetwork = await User.isInMyNetwork(
        req.user._id.toString(),
        docs.docs[i]._id.toString()
      );
      if (isInHisNetwork) {
        docs.docs[i].isAlreadyInMyNetwork = true;
      } else {
        const contactRequestAlreadySent = await Notification.checkIfContactRequestAlreadySent(
          docs.docs[i]._id,
          req.user._id
        );
        if (contactRequestAlreadySent) {
          docs.docs[i].contactRequestAlreadySent = true;
        }
      }
    }
    return res
      .status(200)
      .json(
        responseType.success(11024, 'Professionals successfully found', docs)
      );
  }
  operations.GET.apiDoc = {
    tags: ['User'],
    security: [
      {
        Bearer: ['native_committee_member', 'committee_member', 'professional']
      }
    ],
    summary:
      'Get list of all profesionnal in Arpa network who accept to be in the professionnal book',
    parameters: [
      {
        name: 'page',
        in: 'query',
        required: false,
        type: 'integer',
        default: 1
      },
      {
        name: 'limit',
        in: 'query',
        required: false,
        type: 'integer',
        default: 25,
        maximum: 250
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
