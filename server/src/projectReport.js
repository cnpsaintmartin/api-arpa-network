import { responseType } from '../utils/responsesTemplate';
import ProjectReport from '../models/projectReport';
import ArpaProject from '../models/arpaProject';
import logger from '../services/logger/logger';

export default () => {
  const operations = {
    POST
  };
  async function POST(req, res) {
    const { title, mainContent, image, isInNavbar, attachments } = req.body;
    let newProjectReport = new ProjectReport({
      title,
      mainContent,
      image,
      isInNavbar,
      attachments
    });
    newProjectReport
      .save()
      .then(async docs => {
        const projectStructure = await ArpaProject.findOne({});
        await ArpaProject.addReport(projectStructure._id, docs);
        return res.status(200).json(
          responseType.success(51018, 'Project report successfully added', {
            docs
          })
        );
      })
      .catch(e => {
        logger.error(e);
        console.log(e);
        return res
          .status(500)
          .json(responseType.error(54000, 'Internal Server Error'));
      });
  }
  operations.POST.apiDoc = {
    tags: ['ARPA Project'],
    summary: 'Add a new project report',
    security: [
      {
        Bearer: ['super_admin', 'admin']
      }
    ],
    parameters: [
      {
        required: true,
        in: 'body',
        name: 'project report',
        schema: {
          type: 'object',
          required: ['title', 'mainContent', 'image'],
          properties: {
            title: {
              type: 'string'
            },
            mainContent: {
              type: 'string'
            },
            image: {
              type: 'string'
            },
            isInNavbar: {
              type: 'boolean',
              default: false
            },
            attachments: {
              type: 'array'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
