import parser from 'accept-language-parser';
import { responseType } from '../utils/responsesTemplate';
import Resource from '../models/resource';
import Language from '../models/language';
import Category from '../models/category';
import Tag from '../models/tag';
import Like from '../models/like';
import { useCookie } from '../utils/useCookie';
import logger from '../services/logger/logger';

const uniq = a => [...new Set(a)];

export default () => {
  const operations = {
    GET
  };
  async function GET(req, res) {
    let { search } = req.query;
    const {
      resourceType,
      categories,
      tags,
      includeCodeLanguage,
      excludeCodeLanguage,
      startDate,
      endDate,
      sortBy,
      authorId
    } = req.query;
    // TODO by default add req.cookies['auth._token.local'] spoken languages in filter if includeCodeLanguage & excludeCodeLanguage isn't present
    await useCookie(req);

    let acceptLanguage;
    try {
      acceptLanguage = req.user
        ? req.user.spokenLanguages.map(u => u.code)
        : uniq(parser.parse(req.headers['accept-language']).map(u => u.code));
    } catch (e) {
      logger.error(e);
    }
    let { limit, page } = req.query;
    let query = {
      $or: [
        { deletedAt: { $exists: false } },
        { deletedAt: { $exists: true, $eq: null } }
      ],
      publishedDate: { $exists: true, $ne: null },
      resourceStatus: { $gte: 3 },
      isDraft: false
    };
    if (authorId) {
      query = {
        ...query,
        author: authorId
      };
    }
    /*
      * Filter by date
      * */
    if ((startDate && startDate !== '') || (endDate && endDate !== '')) {
      query = {
        ...query,
        ...(startDate ? { endDate: { $gte: startDate } } : ''),
        ...(endDate ? { startDate: { $lte: endDate } } : '')
      };
      /*
      * Filter by tag
      * */
      if (tags && tags.filter(i => i.length).length !== 0) {
        const optRegexp = [];
        tags.forEach(opt => {
          optRegexp.push(new RegExp(opt, 'i'));
        });
        const result = await Tag.find({
          name: {
            $in: optRegexp
          }
        });
        query = {
          ...query,
          tags: { $in: result.map(el => el._id) }
        };
      }
    }
    /*
    * Filter by included language
    * */
    if (
      includeCodeLanguage &&
      includeCodeLanguage.filter(i => i.length).length !== 0
    ) {
      const result = await Language.find({
        code: {
          $in: includeCodeLanguage
        }
      });
      query = {
        ...query,
        language: { $in: result.map(el => el.code) }
      };
    }
    /*
    * Filter by excluded language
    * */
    if (
      excludeCodeLanguage &&
      excludeCodeLanguage.filter(i => i.length).length !== 0
    ) {
      const result = await Language.find({
        code: {
          $in: excludeCodeLanguage
        }
      });
      query = {
        ...query,
        language: { $nin: result.map(el => el.code) }
      };
    }

    /*
     * use headers language if no include or exclude code language
     * */
    if (acceptLanguage && !includeCodeLanguage && !excludeCodeLanguage) {
      query = {
        ...query,
        language: { $in: ['en', ...acceptLanguage] }
      };
    }
    /*
     * Filter by Categories
     * */
    if (categories && categories.filter(i => i.length).length !== 0) {
      const optRegexp = [];
      categories.forEach(opt => {
        optRegexp.push(new RegExp(opt, 'i'));
      });
      const result = await Category.find({
        name: {
          $in: optRegexp
        }
      });
      query = {
        ...query,
        category: { $in: result.map(el => el._id) }
      };
    }
    /*
      * Filter by type
      * */
    if (resourceType && resourceType !== '') {
      query = {
        ...query,
        resourceType
      };
    }
    /*
      * Filter by search
      * */
    if (search && search !== '') {
      search = decodeURI(search);
      query = {
        ...query,
        $or: [
          { title: { $regex: search, $options: 'i' } },
          { shortDescription: { $regex: search, $options: 'i' } },
          { mainContent: { $regex: search, $options: 'i' } }
        ]
      };
    }
    if (tags && tags.filter(i => i.length).length !== 0) {
      const optRegexp = [];
      tags.forEach(opt => {
        optRegexp.push(new RegExp(opt, 'i'));
      });
      const result = await Tag.find({
        name: {
          $in: optRegexp
        }
      });
      query = {
        ...query,
        tags: { $in: result.map(el => el._id) }
      };
    }
    if (resourceType === 3) delete query.language;
    let docs;
    try {
      docs = await Resource.paginate(
        {
          ...query
        },
        {
          lean: true,
          leanWithId: false,
          select: '-likes',
          sort:
            query.startDate || query.endDate
              ? { startDate: 'asc', textScore: 'asc' }
              : sortBy === 'popularity'
                ? { numberOfLikes: 'desc' }
                : { publishedDate: 'desc', textScore: 'asc' },
          limit,
          page,
          populate: [
            {
              path: 'author',
              select: ['firstname', 'lastname']
            },
            {
              path: 'category',
              select: ['name', 'fontColor', 'backgroundColor']
            },
            {
              path: 'tags'
            }
          ]
        }
      );
    } catch (e) {
      logger.error(e);
      if (e.kind === 'ObjectId') {
        return res
          .status(500)
          .json(
            responseType.error(
              24018,
              'One or some id you try to send is bad. Check the format of them and try again'
            )
          );
      }
      return res
        .status(500)
        .json(responseType.error(24000, 'Internal Server Error'));
    }
    if (docs.totalDocs === 0) {
      return res
        .status(204)
        .json(responseType.error(24016, 'Resources not found'));
    }

    if (req.user) {
      let resourcesIDs = docs.docs.map(el => el._id);
      const result = await Like.find({
        resource: { $in: resourcesIDs },
        author: req.user._id
      }).select('resource');
      result.map(idLike => {
        const tmp = docs.docs.findIndex(
          element => element._id.toString() === idLike.resource.toString()
        );
        docs.docs[tmp].isLikeByCurrentUser = true;
      });
    }
    return res
      .status(200)
      .json(responseType.success(21003, 'Resources successfully found', docs));
  }
  operations.GET.apiDoc = {
    tags: ['Resource'],
    security: [],
    summary: 'Get all paginate resources',
    parameters: [
      {
        name: 'page',
        in: 'query',
        required: false,
        type: 'integer',
        default: 1
      },
      {
        name: 'limit',
        in: 'query',
        required: false,
        type: 'integer',
        default: 50,
        maximum: 250
      },
      {
        name: 'search',
        in: 'query',
        required: false,
        type: 'string'
      },
      {
        name: 'authorId',
        in: 'query',
        required: false,
        type: 'string',
        description: 'if you want to retrieve resource from one author'
      },
      {
        name: 'resourceType',
        in: 'query',
        required: false,
        description: '1 -> article, 2 -> survey, 3 -> event',
        type: 'integer',
        minimum: 1,
        maximum: 3
      },
      {
        name: 'categories',
        in: 'query',
        required: false,
        type: 'array',
        items: {
          type: 'string'
        }
      },
      {
        name: 'tags',
        in: 'query',
        required: false,
        type: 'array',
        items: {
          type: 'string'
        }
      },
      {
        name: 'includeCodeLanguage',
        in: 'query',
        required: false,
        type: 'array',
        items: {
          type: 'string'
        }
      },
      {
        name: 'excludeCodeLanguage',
        in: 'query',
        required: false,
        type: 'array',
        items: {
          type: 'string'
        }
      },
      {
        name: 'startDate',
        in: 'query',
        required: false,
        type: 'string'
      },
      {
        name: 'endDate',
        in: 'query',
        required: false,
        type: 'string'
      },
      {
        name: 'sortBy',
        in: 'query',
        required: false,
        type: 'string',
        description: 'publishedDate / popularity'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
