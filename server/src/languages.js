import Language from '../models/language';
import { responseType } from '../utils/responsesTemplate';

export default () => {
  const operations = {
    GET
  };
  async function GET(req, res) {
    const docs = await Language.find({});
    if (!docs) {
      return res
        .status(204)
        .json(responseType.error(14009, 'Languages not found'));
    }
    return res
      .status(200)
      .json(
        responseType.success(11006, 'Languages successfully found', { docs })
      );
  }
  operations.GET.apiDoc = {
    tags: ['Language'],
    security: [],
    summary: 'Get available languages',
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
