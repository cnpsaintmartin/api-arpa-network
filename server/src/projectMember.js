import { responseType } from '../utils/responsesTemplate';
import ProjectMember from '../models/projectMember';
import ArpaProject from '../models/arpaProject';
import logger from '../services/logger/logger';

export default () => {
  const operations = {
    POST
  };
  async function POST(req, res) {
    const { institution, firstname, lastname, email, website } = req.body;
    const newProjectMember = new ProjectMember({
      institution,
      firstname,
      lastname,
      email,
      website
    });
    newProjectMember
      .save()
      .then(async docs => {
        const projectStructure = await ArpaProject.findOne({});
        await ArpaProject.addMember(projectStructure._id, docs);
        return res.status(200).json(
          responseType.success(51009, 'Project member successfully added', {
            docs
          })
        );
      })
      .catch(e => {
        logger.error(e);
        return res
          .status(500)
          .json(responseType.error(54000, 'Internal Server Error'));
      });
  }
  operations.POST.apiDoc = {
    tags: ['ARPA Project'],
    summary: 'Add a new project member',
    security: [
      {
        Bearer: ['super_admin', 'admin']
      }
    ],
    parameters: [
      {
        required: true,
        in: 'body',
        name: 'user',
        schema: {
          type: 'object',
          required: ['institution', 'firstname', 'lastname', 'email'],
          properties: {
            institution: {
              type: 'string'
            },
            firstname: {
              type: 'string'
            },
            lastname: {
              type: 'string'
            },
            email: {
              type: 'string'
            },
            website: {
              type: 'string'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
