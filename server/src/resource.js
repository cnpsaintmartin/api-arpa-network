import { responseType } from '../utils/responsesTemplate';
import Resource from '../models/resource';
import SurveyChoice from '../models/surveyChoice';
import Survey from '../models/survey';
import moment from 'moment';
import { countTime } from '../utils/countWord';
import Tag from '../models/tag';
import { logger } from '../services/logger/index';
import Role from '../models/role';
import User from '../models/user';
import Notification from '../models/notification';
import _ from 'lodash';
import mailer from '../services/mailer';

const generateTabTags = async tags => {
  const tab = [];
  await Promise.all(
    await tags.map(async el => {
      const result = await Tag.findOne({
        name: new RegExp('^' + el.toLowerCase() + '$', 'i')
      });
      if (!result) {
        let newTag = new Tag({
          name: el.toLowerCase()
        });
        let docs;
        try {
          docs = await newTag.save();
          tab.push(docs._id);
        } catch (e) {
          logger.error(e);
        }
      } else {
        await Tag.addUsage(result._id);
        tab.push(result._id);
      }
    })
  );
  return tab;
};
export { generateTabTags };
export default () => {
  const operations = {
    POST
  };
  async function POST(req, res) {
    const {
      title,
      idCategory,
      tags,
      mainContent,
      image,
      idLanguage,
      commentIsAuthorize,
      isDraft,
      surveyTitle,
      surveyAnswers,
      startDate,
      surveyDuration,
      attachments
    } = req.body;
    let { endDate } = req.body;
    let resourceType = 1;
    let survey;
    // if it's a survey we set special
    if (surveyTitle && surveyAnswers) {
      resourceType = 2;
      const surveyTmp = new Survey({
        topic: surveyTitle
      });
      await surveyAnswers.reduce(async (buffer, answer) => {
        const tmp = await buffer;
        const tmpAnswer = new SurveyChoice({
          answer
        });
        const result = await tmpAnswer.save();
        tmp.push(result._id);
        await surveyTmp.choices.push(result._id);
        return tmp;
      }, []);
      survey = await surveyTmp.save();
    }
    if (startDate || endDate) {
      if (startDate && endDate) {
        if (moment(endDate).isBefore(moment(startDate))) {
          return res
            .status(500)
            .json(
              responseType.error(24000, 'Start date must be before end date')
            );
        }
      } else {
        if (startDate && !endDate) {
          endDate = startDate;
        }
      }
      resourceType = 3;
    }
    let tagTab;
    if (tags && tags.filter(i => i.length).length !== 0) {
      tagTab = await generateTabTags(tags);
    }
    const newShortDescription = _.truncate(
      mainContent.replace(/<[^>]*>?/gm, ''),
      {
        length: 150
      }
    );
    const resource = new Resource({
      title,
      category: idCategory,
      shortDescription: newShortDescription,
      estimateReadingTime: countTime(mainContent),
      mainContent,
      image,
      language: idLanguage,
      tags: tagTab,
      commentIsAuthorize,
      author: req.user._id,
      isDraft,
      resourceType,
      survey,
      startDate,
      endDate,
      resourceStatus: 1,
      surveyDuration,
      attachments
    });
    let docs;
    try {
      docs = await resource.save();
    } catch (e) {
      logger.error(e);
      return res
        .status(500)
        .json(responseType.error(24000, 'Internal Server Error'));
    }
    const tabIdGroup = await Role.getIdWithSlug([
      'committee_member',
      'native_committee_member'
    ]);
    const users = await User.getEmailByGroup(tabIdGroup);
    users.map(async el => {
      await mailer.send(
        el,
        'An article has been published',
        {
          mailTitle: 'Dear committee member',
          message:
            'A user just published an article. Please validate or reject this resource as soon as possible. Thank you very much !',
          buttonLink: 'https://arpa-ageing.eu/committee/resources',
          button: 'Go to committee page',
          customerEmail: el
        },
        'general'
      );
    });
    const idUsers = await User.getUserByGroup(tabIdGroup);
    let notification = new Notification({
      title: 'Resource publish request',
      sender: req.user._id,
      receiver: idUsers,
      message:
        'Please, can you check my resource, I want to publish it on ARPA !',
      messageCode: '0016',
      titleCode: '0016',
      type: 'resourcePublishRequest',
      resource: docs._id,
      createdAt: moment.utc()
    });
    await notification.save();

    return res.status(200).json(
      responseType.success(
        21002,
        'Resource correctly registered, waiting for committee validation',
        {
          docs
        }
      )
    );
  }
  operations.POST.apiDoc = {
    tags: ['Resource'],
    summary: 'Add a new resource',
    parameters: [
      {
        required: true,
        in: 'body',
        name: 'resource',
        schema: {
          $ref: '#/definitions/Resource'
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
