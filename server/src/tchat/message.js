import { responseType } from '../../utils/responsesTemplate';
import Message from '../../models/message';
import { logger } from '../../services/logger';
export default () => {
  const operations = {
    POST
  };
  async function POST(req, res) {
    const { message } = req.body;
    const newMessage = new Message({
      user: req.user._id.toString(),
      message
    });
    let docs;
    try {
      docs = await newMessage.save();
    } catch (e) {
      logger.error(e);
      return res
        .status(500)
        .json(responseType.error(74006, 'Internal Server Error'));
    }
    return res.status(200).json(
      responseType.success(71005, 'Message correctly registered', {
        docs
      })
    );
  }
  operations.POST.apiDoc = {
    tags: ['Committee'],
    summary: 'Send message to administrators and committee members',
    security: [
      {
        Bearer: [
          'native_committee_member',
          'committee_member',
          'super_admin',
          'admin'
        ]
      }
    ],
    parameters: [
      {
        in: 'body',
        name: 'message',
        schema: {
          required: ['message'],
          type: 'object',
          properties: {
            message: {
              type: 'string'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
