import { responseType } from '../../utils/responsesTemplate';
import Notification from '../../models/notification';
import logger from '../../services/logger/logger';
export default () => {
  const operations = {
    GET
  };
  async function GET(req, res) {
    let numberOfNotificationsNotRead;
    try {
      numberOfNotificationsNotRead = await Notification.countDocuments({
        receiver: { $in: [req.user._id] },
        readBy: { $nin: [req.user._id] }
      });
    } catch (e) {
      logger.error(e);
      return res
        .status(500)
        .json(responseType.error(74000, 'Internal Server Error'));
    }
    return res.status(200).json(
      responseType.success(71010, 'Your notifications successfully found', {
        docs: { numberOfNotificationsNotRead }
      })
    );
  }
  operations.GET.apiDoc = {
    tags: ['Notification', 'User'],
    summary: 'Get all notification of one connected user',
    parameters: [],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
