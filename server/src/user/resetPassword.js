import moment from 'moment';
import cryptoRandomString from 'crypto-random-string';
import User from '../../models/user';
import mailer from '../../services/mailer';
import { responseType } from '../../utils/responsesTemplate';

export default () => {
  const operations = {
    POST
  };

  async function POST(req, res) {
    const { code, newPassword } = req.body;
    const user = await User.findOne({
      'forget.code': code,
      'forget.validity': {
        $gte: moment().subtract(1, 'day')
      }
    });
    if (!user)
      return res
        .status(400)
        .json(responseType.error(24000, 'Wrong code or time expired'));

    user.forget.code = cryptoRandomString(24);
    user.password = newPassword;

    await user.save();
    await mailer.send(
      user.email,
      'Password Changed For Arpa Account',
      {
        link: `https://arpa-ageing.eu/login/forgotPassword?confirmation=${
          user.forget.code
        }`
      },
      'passwordChanged'
    );
    return res
      .status(200)
      .json(responseType.success(21060, 'password successfully reset'));
  }

  operations.POST.apiDoc = {
    tags: ['User'],
    security: [],
    parameters: [
      {
        in: 'body',
        name: 'user',
        schema: {
          type: 'object',
          required: ['code', 'newPassword'],
          properties: {
            code: {
              type: 'string'
            },
            newPassword: {
              type: 'string'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '400': {
        description: 'Wrong email'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
