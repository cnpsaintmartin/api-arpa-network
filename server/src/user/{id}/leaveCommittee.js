import { responseType } from '../../../utils/responsesTemplate';
import User from '../../../models/user';
import Role from '../../../models/role';
export default () => {
  const operations = {
    PUT
  };
  async function PUT(req, res) {
    const { id } = req.params;
    if (id.toString() !== req.user._id.toString()) {
      return res
        .status(401)
        .json(responseType.error(14022, 'Access forbidden'));
    }
    const committeeMemberRoleIdPromise = Role.getIdWithSlug([
      'committee_member'
    ]);
    const committeeNatifRoleIdPromise = Role.getIdWithSlug([
      'native_committee_member'
    ]);

    const [committeeMemberRoleId, committeeNatifRoleId] = await Promise.all([
      committeeMemberRoleIdPromise,
      committeeNatifRoleIdPromise
    ]);
    User.findById(id, (err, user) => {
      if (err) {
        return res
          .status(500)
          .json(responseType.error(74003, 'Unable to leave the committee'));
      }
      const userRoles = user.roles;
      const indexCommitteeMemberId = userRoles.indexOf(
        committeeMemberRoleId.toString()
      );
      const indexNatifId = userRoles.indexOf(committeeNatifRoleId.toString());
      if (indexCommitteeMemberId > -1) {
        userRoles.splice(indexCommitteeMemberId, 1);
      }
      if (indexNatifId > -1) {
        userRoles.splice(indexNatifId, 1);
      }
      user.set({
        ...(userRoles ? { userRoles } : '')
      });
      user.save(err => {
        if (err) {
          return res
            .status(500)
            .json(responseType.error(74003, 'Unable to leave the committee'));
        }
        return res
          .status(200)
          .json(
            responseType.success(
              71004,
              'User has lived the committee successfully'
            )
          );
      });
    });
  }
  operations.PUT.apiDoc = {
    tags: ['User'],
    summary: 'To leave committee (this action is irreversible)',
    security: [
      {
        Bearer: ['native_committee_member', 'committee_member']
      }
    ],
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '401': {
        description: 'Access forbidden'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
