import { responseType } from '../../../utils/responsesTemplate';
import Comment from '../../../models/comment';
import Like from '../../../models/like';
import Notification from '../../../models/notification';
import Resource from '../../../models/resource';
import User from '../../../models/user';
import Survey from '../../../models/survey';
import SurveyChoice from '../../../models/survey';
const yazl = require('yazl');
export default () => {
  const operations = {
    DELETE,
    GET
  };
  async function DELETE(req, res) {
    const { id } = req.params;
    if (id.toString() !== req.user._id.toString()) {
      return res
        .status(401)
        .json(responseType.error(14022, 'Access forbidden'));
    }
    // ####################################
    /** Delete reported row from isReportedBy in comment **/
    const userComment = await Comment.find({
      author: id
    });
    const tabPromiseCommentDeleted = [];
    if (userComment.length > 0) {
      userComment.forEach(comment => {
        const promise = Resource.removeComment(comment.resource, comment._id);
        tabPromiseCommentDeleted.push(promise);
      });
    }
    const userCommentDeletePromise = Comment.deleteMany({
      author: id
    });
    // ####################################

    // ####################################
    /** Delete reported row from isReportedBy in comment **/
    const userCommentsReported = await Comment.find({
      isReportedBy: { $in: id }
    });
    const tabPromise = [];
    if (userCommentsReported.length > 0) {
      userCommentsReported.forEach(comment => {
        const promise = Comment.removeReport(comment._id, id);
        tabPromise.push(promise);
      });
    }
    // ####################################

    // ####################################
    /** Delete like **/
    const likeUser = await Like.find({
      author: id
    });
    const tabPromiseLike = [];
    if (likeUser.length > 0) {
      likeUser.forEach(like => {
        const promise = Resource.unlike(like.resource);
        tabPromiseLike.push(promise);
      });
    }
    const userDeleteLikePromise = Like.deleteMany({
      author: id
    });
    // ####################################

    // ####################################
    /** Delete notification **/

    const deleteNotificationUserSenderPromise = await Notification.deleteMany({
      sender: id
    });
    const notificationReceiver = await Notification.find({
      receiver: { $in: id }
    });
    // const tabNotificationReceiverPromise = [];
    if (notificationReceiver.length > 0) {
      notificationReceiver.map(async n => {
        const notification = await Notification.findById(n);
        if (!notification) return 'notFound';
        let tab = notification.receiver.map(String);
        const index = tab.indexOf(id.toString());
        if (index === -1) return 'noCommentToDelete';
        tab.splice(index, 1);
        notification.set({ receiver: tab });
        try {
          await notification.save();
        } catch (e) {}
      });
    }
    // ###################################

    // ####################################
    /** Delete resource and associated data**/
    const resourceUser = await Resource.find({
      author: id
    });
    if (resourceUser.length > 0) {
      await resourceUser.map(async r => {
        await Comment.deleteMany({
          resource: r._id
        });
        await Like.deleteMany({
          resource: r._id
        });
        await Resource.deleteOne({
          _id: r._id
        });
      });
    }
    // ###################################

    // ####################################
    /** Delete resource and associated data**/
    const networkUser = await User.find({
      myNetwork: { $in: id }
    });
    if (networkUser.length > 0) {
      networkUser.map(async n => {
        const user = await User.findById(n);
        if (!user) return 'notFound';
        let tab = user.myNetwork.map(String);
        const index = tab.indexOf(id.toString());
        if (index === -1) return 'noCommentToDelete';
        tab.splice(index, 1);
        user.set({ myNetwork: tab });
        try {
          await user.save();
        } catch (e) {}
      });
    }
    // ###################################

    const deleteUserPromise = User.deleteOne({
      _id: id
    });
    await Promise.all([
      userCommentDeletePromise,
      tabPromise,
      tabPromiseCommentDeleted,
      tabPromiseLike,
      userDeleteLikePromise,
      deleteUserPromise
    ]);
    return res
      .status(200)
      .json(responseType.success(11039, 'Your data has been removed'));
  }
  async function GET(req, res) {
    const { id } = req.params;
    if (id.toString() !== req.user._id.toString()) {
      return res
        .status(401)
        .json(responseType.error(14022, 'Access forbidden'));
    }
    // comments send by this user
    const userCommentsPromise = Comment.find({
      author: id
    }).select('publishedDate text resource');

    // comments reported by this user
    const userCommentsReportedPromise = Comment.find({
      isReportedBy: { $in: id }
    }).select('publishedDate text resource');

    // likes send by this user
    const userLikesPromise = Like.find({
      author: id
    }).select('likeDate resource');

    // notification received by this user
    const userNotificationsReceivePromise = Notification.find({
      receiver: { $in: id }
    }).select('comment resource type message');

    // notification send by this user
    const userNotificationsSendPromise = Notification.find({
      sender: id
    }).select('comment resource type message');

    // resource send by this user
    const userResourceSendPromise = Resource.find({
      author: id
    }).select(
      'title slug category shortDescription estimateReadingTime mainContent image language tags submittedDate publishedDate numberOfLikes commentIsAuthorize isDraft resourceType resourceStatus survey surveyDuration startDate endDate updatedAt deletedAt resourceTmp'
    );

    // data user
    const userDataPromise = User.find({ _id: id }).select(
      'firstname lastname email isAgreeAppearProfessionalContactBook isAgreeWithContactRequest description positionHeld country spokenLanguages roles profileImage updatedAt createdAt'
    );
    const [
      userComments,
      userCommentsReported,
      userLikes,
      userNotificationsReceive,
      userNotificationsSend,
      userResourceSend,
      userData
    ] = await Promise.all([
      userCommentsPromise,
      userCommentsReportedPromise,
      userLikesPromise,
      userNotificationsReceivePromise,
      userNotificationsSendPromise,
      userResourceSendPromise,
      userDataPromise
    ]);

    const zipFile = new yazl.ZipFile();
    res.setHeader('Content-Type', 'application/octet-stream');
    res.setHeader('Content-Disposition', `attachment;filename=data.zip`);
    zipFile.addBuffer(
      Buffer.from(JSON.stringify(userComments)),
      'data/comments.json'
    );
    zipFile.addBuffer(
      Buffer.from(JSON.stringify(userCommentsReported)),
      'data/commentsReported.json'
    );
    zipFile.addBuffer(
      Buffer.from(JSON.stringify(userLikes)),
      'data/likes.json'
    );
    zipFile.addBuffer(
      Buffer.from(JSON.stringify(userNotificationsReceive)),
      'data/notificationsReceive.json'
    );
    zipFile.addBuffer(
      Buffer.from(JSON.stringify(userNotificationsSend)),
      'data/notificationsSend.json'
    );
    zipFile.addBuffer(
      Buffer.from(JSON.stringify(userResourceSend)),
      'data/resourceSend.json'
    );
    zipFile.addBuffer(Buffer.from(JSON.stringify(userData)), 'data/user.json');
    zipFile.outputStream.pipe(res);
    zipFile.end({ forceZip64Format: true });
  }
  operations.GET.apiDoc = {
    tags: ['User'],
    summary: 'Get all personal data that you have registered in arpa-ageing',
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  operations.DELETE.apiDoc = {
    tags: ['User'],
    summary: 'Delete all personal data that you have registered in arpa-ageing',
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
