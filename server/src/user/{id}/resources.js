import { responseType } from '../../../utils/responsesTemplate';
import Resource from '../../../models/resource';
// import { useCookie } from '../../../utils/useCookie';
export default () => {
  const operations = {
    GET
  };
  async function GET(req, res) {
    // TODO add role filter
    // await useCookie(req);
    const { id } = req.params;
    let { page, limit } = req.query;
    if (!page) page = 1;
    if (!limit) limit = 25;
    let docs = await Resource.paginate(
      {
        author: id,
        $or: [
          { deletedAt: { $exists: false } },
          { deletedAt: { $exists: true, $eq: null } }
        ]
      },
      {
        select: '-likes',
        sort: { submittedDate: 'desc' },
        limit,
        page,
        populate: [
          {
            path: 'tags'
          },
          {
            path: 'author',
            select: ['firstname', 'lastname']
          }
        ]
      }
    );
    if (docs.totalDocs === 0) {
      return res
        .status(204)
        .json(responseType.error(24024, 'Resource not found'));
    }
    return res
      .status(200)
      .json(responseType.success(21003, 'Resources successfully found', docs));
  }
  operations.GET.apiDoc = {
    tags: ['User'],
    security: [],
    summary: "Get all user's resources",
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      },
      {
        name: 'page',
        in: 'query',
        required: false,
        type: 'integer',
        default: 1
      },
      {
        name: 'limit',
        in: 'query',
        required: false,
        type: 'integer',
        default: 25
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
