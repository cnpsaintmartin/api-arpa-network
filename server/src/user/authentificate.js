import jwt from 'jsonwebtoken';
import config from 'config';
import User from '../../models/user';
import Role from '../../models/role';
import { responseType } from '../../utils/responsesTemplate';
import { isBan } from '../../utils/authorization';

const generateToken = (
  id,
  lastname,
  firstname,
  email,
  roles = [],
  spokenLanguages = [],
  status = []
) => {
  const rolesSlug = roles.map(el => el.slug);
  const token = jwt.sign(
    {
      _id: id,
      lastname,
      firstname,
      email,
      roles: rolesSlug,
      spokenLanguages,
      status
    },
    config.secretToken,
    { expiresIn: '10h' }
  );
  return token;
};

export { generateToken };

export default () => {
  const operations = {
    POST
  };
  async function POST(req, res) {
    const { email, password } = req.body;
    User.findOne({
      email
    })
      .populate({ path: 'roles', model: Role })
      .exec((err, user) => {
        if (!user) {
          return res
            .status(401)
            .json(
              responseType.error(
                14001,
                "Credentials are wrong, email don't exist or password is wrong"
              )
            );
        }
        user.comparePassword(password, function(err, isMatch) {
          if (err) throw err;
          if (!isMatch)
            return res
              .status(401)
              .json(
                responseType.error(
                  14001,
                  "Credentials are wrong, email don't exist or password is wrong"
                )
              );
          if (isBan(user.status)) {
            return res
              .status(401)
              .json(responseType.error(14038, 'You have been banned'));
          }
          // WARNING, when we add something here add in useCookie.js too
          const token = generateToken(
            user._id,
            user.lastname,
            user.firstname,
            user.email,
            user.roles,
            user.spokenLanguages,
            user.status
          );
          res.set('authorization', token);
          return res.status(200).json(
            responseType.success(11002, 'Login successfully', {
              token
            })
          );
        });
      });
  }
  operations.POST.apiDoc = {
    security: [],
    tags: ['User'],
    summary: 'Login user',
    operationId: 'registerUser',
    parameters: [
      {
        in: 'body',
        name: 'user',
        schema: {
          type: 'object',
          required: ['email', 'password'],
          properties: {
            email: {
              type: 'string'
            },
            password: {
              type: 'string'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
