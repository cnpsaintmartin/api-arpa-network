import cryptoRandomString from 'crypto-random-string';
import User from '../../models/user';
import mailer from '../../services/mailer';
import { responseType } from '../../utils/responsesTemplate';
import moment from 'moment';

export default () => {
  const operations = {
    POST
  };

  async function POST(req, res) {
    const { email } = req.body;
    const user = await User.findOne({
      email
    });
    if (!user)
      return res.status(400).json(responseType.error(24000, 'Wrong ID'));
    user.forget = {
      code: cryptoRandomString(24),
      validity: moment.utc()
    };
    await user.save();
    await mailer.send(
      user.email,
      'Reset your password on ARPA Network',
      {
        link: `https://arpa-ageing.eu/login/forgotPassword?confirmation=${
          user.forget.code
        }`
      },
      'forgot'
    );
    return res
      .status(200)
      .json(responseType.success(21060, 'email reset sent'));
  }

  operations.POST.apiDoc = {
    tags: ['User'],
    security: [],
    parameters: [
      {
        in: 'body',
        name: 'user',
        schema: {
          type: 'object',
          required: ['email'],
          properties: {
            email: {
              type: 'string'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '400': {
        description: 'Wrong email'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
