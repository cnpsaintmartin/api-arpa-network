import moment from 'moment';

import { responseType } from '../../utils/responsesTemplate';
import User from '../../models/user';
import { isAuthorized, itsMe } from '../../utils/authorization';
import Notification from '../../models/notification';
import { logger } from '../../services/logger/index';
import Role from '../../models/role';

export default () => {
  const operations = {
    PUT,
    GET
  };
  async function PUT(req, res) {
    const { id } = req.params;
    const authorized = await isAuthorized(
      ['admin', 'super_admin'],
      req.user.roles
    );
    const isHisProfile = await itsMe(id, req);
    if (!authorized && !isHisProfile) {
      return res
        .status(401)
        .json(responseType.error(14022, 'Access forbidden'));
    }
    const {
      lastname,
      firstname,
      email,
      password,
      professionalDocument,
      isAgreeAppearProfessionalContactBook,
      isAgreeWithContactRequest,
      description,
      positionHeld,
      country,
      spokenLanguages
    } = req.body;
    let { roles } = req.body;
    let isAuthorizeToUpdateRoles = false;
    let isAlreadyProfessional = false;
    if (roles) {
      if (!authorized) {
        roles = null;
      } else {
        isAuthorizeToUpdateRoles = true;
      }
    }
    let user;
    try {
      user = await User.findById(id);
    } catch (e) {
      logger.error(e);
      return res
        .status(500)
        .json(responseType.error(14015, 'Unable to update user'));
    }
    if (professionalDocument && professionalDocument !== '') {
      const idProfessional = await Role.getIdWithSlug(['professional']);
      // if isn't already a professional user
      if (!user.roles.map(String).includes(idProfessional[0].toString())) {
        const tabIdGroup = await Role.getIdWithSlug([
          'committee_member',
          'native_committee_member'
        ]);
        const idUsers = await User.getUserByGroup(tabIdGroup);
        let notification = new Notification({
          createdAt: moment.utc(),
          title: 'Professional member request',
          sender: user._id,
          receiver: idUsers,
          message:
            'One user wants to be a professional in ARPA ageing. Please look his proof and accept or reject his request',
          messageCode: '0012',
          titleCode: '0012',
          type: 'professionalMemberRequest'
        });
        await notification.save();
      } else {
        isAlreadyProfessional = true;
      }
    }
    user.set({
      ...(lastname ? { lastname } : {}),
      ...(firstname ? { firstname } : {}),
      ...(email ? { email } : {}),
      ...(password ? { password } : {}),
      ...(description ? { description } : {}),
      ...(positionHeld ? { positionHeld } : {}),
      ...(country ? { country } : {}),
      ...(spokenLanguages ? { spokenLanguages } : {}),
      ...(isAuthorizeToUpdateRoles ? { roles } : {}),
      ...(typeof isAgreeAppearProfessionalContactBook === 'boolean'
        ? { isAgreeAppearProfessionalContactBook }
        : {}),
      ...(typeof isAgreeWithContactRequest === 'boolean'
        ? { isAgreeWithContactRequest }
        : {}),
      ...(professionalDocument && !isAlreadyProfessional
        ? { professionalDocument }
        : {}),
      updatedAt: moment.utc()
    });
    try {
      let updatedUser = await user.save();
      updatedUser = updatedUser.toObject();
      delete updatedUser.password;
      return res.status(200).json(
        responseType.success(11016, 'User successfully updated', {
          docs: updatedUser
        })
      );
    } catch (e) {
      logger.error(e);
      return res
        .status(500)
        .json(responseType.error(14015, 'Unable to update user'));
    }
  }
  async function GET(req, res) {
    const { id } = req.params;
    const isHisProfile = await itsMe(id, req);
    let selector;
    let isInHisNetwork = false;
    let contactRequestIsAlreadySent = false;
    if (isHisProfile) {
      selector =
        'email profileImage lastname firstname description country spokenLanguages roles isAgreeWithContactRequest isAgreeAppearProfessionalContactBook positionHeld';
    } else {
      if (req.user) {
        if (
          req.user.roles.includes('super_admin') ||
          req.user.roles.includes('committee_member') ||
          req.user.roles.includes('native_committee_member')
        ) {
          isInHisNetwork = await User.isInMyNetwork(req.user._id, id);
          contactRequestIsAlreadySent = await Notification.checkIfContactRequestAlreadySent(
            id,
            req.user._id
          );
          selector =
            'email profileImage lastname firstname description country spokenLanguages roles isAgreeWithContactRequest isAgreeAppearProfessionalContactBook positionHeld professionalDocument';
        } else {
          isInHisNetwork = await User.isInMyNetwork(req.user._id, id);
          contactRequestIsAlreadySent = await Notification.checkIfContactRequestAlreadySent(
            id,
            req.user._id
          );
          // public but we check if this person is in the current user network
          if (isInHisNetwork) {
            selector =
              'email profileImage lastname firstname description country spokenLanguages roles isAgreeWithContactRequest positionHeld';
          } else {
            selector =
              'profileImage lastname firstname description country spokenLanguages roles isAgreeWithContactRequest positionHeld';
          }
        }
      } else {
        // public without connected person
        selector =
          'profileImage lastname firstname description country spokenLanguages roles isAgreeWithContactRequest positionHeld';
      }
    }
    let docs = await User.findOne({
      _id: id
    })
      .select(selector)
      .populate({
        path: 'roles'
      })
      .populate({
        path: 'country'
      })
      .populate({
        path: 'spokenLanguages'
      });
    if (!docs) {
      return res.status(404).json(responseType.error(14013, 'User not found'));
    }
    docs = docs.toObject();
    delete docs.password;
    if (isInHisNetwork) docs.isAlreadyInMyNetwork = true;
    if (contactRequestIsAlreadySent) docs.contactRequestAlreadySent = true;
    return res.status(200).json(
      responseType.success(11014, 'User successfully found', {
        docs
      })
    );
  }
  operations.GET.apiDoc = {
    tags: ['User'],
    security: [],
    summary: 'Get one user with it ID',
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  operations.PUT.apiDoc = {
    tags: ['User'],
    summary: 'Update one user with it ID',
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      },
      {
        required: true,
        in: 'body',
        name: 'user',
        schema: {
          type: 'object',
          properties: {
            lastname: {
              type: 'string'
            },
            firstname: {
              type: 'string'
            },
            email: {
              type: 'string'
            },
            password: {
              type: 'string'
            },
            professionalDocument: {
              type: 'string'
            },
            isAgreeAppearProfessionalContactBook: {
              type: 'boolean',
              default: false
            },
            isAgreeWithContactRequest: {
              type: 'boolean',
              default: false
            },
            description: {
              type: 'string'
            },
            positionHeld: {
              type: 'string'
            },
            country: {
              type: 'string'
            },
            spokenLanguages: {
              type: 'array',
              items: {
                type: 'string'
              }
            },
            roles: {
              type: 'array',
              items: {
                type: 'string'
              }
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
