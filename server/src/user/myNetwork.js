import { responseType } from '../../utils/responsesTemplate';
import User from '../../models/user';
import logger from '../../services/logger/logger';
export default () => {
  const operations = {
    GET
  };
  async function GET(req, res) {
    let { limit, page } = req.query;
    if (!limit) limit = 50;
    if (!page) page = 1;
    let docs;
    const currentUser = await User.findById(req.user._id);
    const hisNetwork = currentUser.myNetwork;
    const query = {
      _id: { $in: hisNetwork }
    };
    try {
      docs = await User.paginate(
        {
          ...query
        },
        {
          select:
            '_id firstname lastname spokenLanguages positionHeld roles email description country',
          sort: { lastname: 'asc' },
          limit,
          page,
          populate: [
            {
              path: 'spokenLanguages'
            },
            {
              path: 'country'
            },
            {
              path: 'roles'
            }
          ]
        }
      );
    } catch (e) {
      logger.error(e);
      return res
        .status(500)
        .json(responseType.error(14000, 'Internal Server Error'));
    }
    if (docs.totalDocs === 0) {
      return res
        .status(204)
        .json(responseType.error(14025, 'My network is empty'));
    }
    return res
      .status(200)
      .json(responseType.success(11026, 'My network successfully found', docs));
  }
  operations.GET.apiDoc = {
    tags: ['User'],
    security: [
      {
        Bearer: [
          'professional',
          'committee_member',
          'super_admin',
          'admin',
          'native_committee_member'
        ]
      }
    ],
    summary: 'Get all users in your network',
    parameters: [
      {
        name: 'page',
        in: 'query',
        required: false,
        type: 'integer',
        default: 1
      },
      {
        name: 'limit',
        in: 'query',
        required: false,
        type: 'integer',
        default: 50,
        maximum: 250
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
