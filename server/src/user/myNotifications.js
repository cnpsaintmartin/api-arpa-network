import { responseType } from '../../utils/responsesTemplate';
import Notification from '../../models/notification';
import logger from '../../services/logger/logger';
export default () => {
  const operations = {
    GET
  };
  async function GET(req, res) {
    let { limit, page } = req.query;
    if (!limit) limit = 10;
    if (!page) page = 1;
    let docs;
    const query = {
      receiver: { $in: req.user._id }
    };
    try {
      docs = await Notification.paginate(
        {
          ...query
        },
        {
          lean: true,
          select:
            'sender title message readBy createdAt displayReadBy type titleCode messageCode agreeMemberRequest declineMemberRequest resource comment',
          sort: { createdAt: 'desc' },
          limit,
          page,
          populate: [
            {
              path: 'readBy',
              select: 'firstname lastname'
            },
            {
              path: 'sender',
              select: 'firstname lastname profileImage'
            }
          ]
        }
      );
    } catch (e) {
      logger.error(e);
      return res
        .status(500)
        .json(responseType.error(74000, 'Internal Server Error'));
    }
    if (docs.totalDocs === 0) {
      return res
        .status(204)
        .json(responseType.error(74009, "You don't have any notification"));
    }

    for (let i = 0; i < docs.docs.length; i++) {
      // if the request is 'committeeMemberRequest' we check if the user has already vote
      if (docs.docs[i].type && docs.docs[i].type === 'committeeMemberRequest') {
        if (
          docs.docs[i].agreeMemberRequest &&
          docs.docs[i].agreeMemberRequest
            .map(String)
            .includes(req.user._id.toString())
        ) {
          docs.docs[i].isAlreadyVoteByCurrentUser = true;
        } else if (
          docs.docs[i].declineMemberRequest &&
          docs.docs[i].declineMemberRequest
            .map(String)
            .includes(req.user._id.toString())
        ) {
          docs.docs[i].isAlreadyVoteByCurrentUser = true;
        }
      }
      // if it's a committee request we can keep the name of the person who has read this notification
      if (!docs.docs[i].displayReadBy) {
        const lengthReader = docs.docs[i].readBy.length;
        for (let t = 0; t < lengthReader; t++) {
          delete docs.docs[i].readBy[t]['lastname'];
          delete docs.docs[i].readBy[t]['firstname'];
        }
      }
      // if the notifications display isn't already read by the current user, we add his id into the array readBy
      if (!docs.docs[i].readBy.includes(req.user._id)) {
        await Notification.markAsReadByUser(docs.docs[i]._id, req.user._id);
      }
    }
    return res
      .status(200)
      .json(
        responseType.success(
          71010,
          'Your notifications successfully found',
          docs
        )
      );
  }
  operations.GET.apiDoc = {
    tags: ['Notification', 'User'],
    summary: 'Get all notification of one connected user',
    parameters: [
      {
        name: 'page',
        in: 'query',
        required: false,
        type: 'integer',
        default: 1
      },
      {
        name: 'limit',
        in: 'query',
        required: false,
        type: 'integer',
        default: 10,
        maximum: 25
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
