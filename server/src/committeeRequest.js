import moment from 'moment';
import { responseType } from '../utils/responsesTemplate';
import Notification from '../models/notification';
import User from '../models/user';
import Role from '../models/role';
export default () => {
  const operations = {
    POST
  };
  async function POST(req, res) {
    const { message } = req.body;
    if (
      req.user.roles.includes('committee_member') ||
      req.user.roles.includes('native_committee_member')
    ) {
      return res
        .status(500)
        .json(
          responseType.success(14028, 'You already are a committee member')
        );
    }
    const committeeMemberRequestAlreadySent = await Notification.checkIfCommitteeMemberRequestAlreadySent(
      req.user._id
    );
    if (committeeMemberRequestAlreadySent) {
      return res
        .status(500)
        .json(
          responseType.error(
            74017,
            'You already sent a contact request to the committee, please wait his answer'
          )
        );
    }
    const tabIdGroup = await Role.getIdWithSlug([
      'committee_member',
      'native_committee_member'
    ]);
    const idUsers = await User.getUserByGroup(tabIdGroup);
    let notification = new Notification({
      createdAt: moment.utc(),
      title: 'Committee member request',
      sender: req.user._id,
      receiver: idUsers,
      message,
      messageCode: '0000',
      titleCode: '0009',
      type: 'committeeMemberRequest'
    });
    notification.save(err => {
      if (err)
        return res
          .status(500)
          .json(
            responseType.error(74019, 'Unable to save committee member request')
          );
      return res
        .status(200)
        .json(
          responseType.error(
            71018,
            'Committee member request send successfully'
          )
        );
    });
  }
  operations.POST.apiDoc = {
    tags: ['User'],
    summary: 'Send a request to be a committee user',
    security: [
      {
        Bearer: [
          'professional',
          'committee_member',
          'super_admin',
          'admin',
          'native_committee_member'
        ]
      }
    ],
    parameters: [
      {
        required: true,
        in: 'body',
        name: 'notification',
        schema: {
          type: 'object',
          required: ['message'],
          properties: {
            message: {
              type: 'string'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
