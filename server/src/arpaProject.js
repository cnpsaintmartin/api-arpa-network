import { responseType } from '../utils/responsesTemplate';
import ArpaProject from '../models/arpaProject';
export default () => {
  const operations = {
    PUT,
    GET
  };
  async function PUT(req, res) {
    const { content } = req.body;
    const projectDescription = await ArpaProject.findOne({});
    if (projectDescription) {
      projectDescription.set({
        content
      });
      projectDescription.save((err, updatedProjectDescription) => {
        if (err) {
          return res
            .status(500)
            .json(
              responseType.error(54001, 'Unable to update project description')
            );
        }
        return res.status(200).json(
          responseType.success(
            51002,
            'Project description successfully updated',
            {
              docs: updatedProjectDescription
            }
          )
        );
      });
    } else {
      const newProjectDescription = new ArpaProject({
        content
      });
      newProjectDescription.save((err, newProjectDescription) => {
        if (err) {
          return res
            .status(500)
            .json(
              responseType.error(54003, 'Unable to update project description')
            );
        }
        return res.status(200).json(
          responseType.success(
            51004,
            'Project description successfully saved',
            {
              docs: newProjectDescription
            }
          )
        );
      });
    }
  }
  async function GET(req, res) {
    const docs = await ArpaProject.findOne({})
      .populate('members')
      .populate({
        path: 'reports',
        options: {
          sort: { createdAt: -1 }
        }
      });
    if (!docs) {
      return res
        .status(204)
        .json(responseType.error(54005, 'Project description not found'));
    }
    return res.status(200).json(
      responseType.success(51006, 'Project description successfully found', {
        docs
      })
    );
  }
  operations.GET.apiDoc = {
    tags: ['ARPA Project'],
    security: [],
    summary: 'Get arpa description, members, news',
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  operations.PUT.apiDoc = {
    tags: ['ARPA Project'],
    summary: 'Update ARPA description',
    description: 'Available only for administrators',
    security: [
      {
        Bearer: ['super_admin', 'admin']
      }
    ],
    parameters: [
      {
        in: 'body',
        name: 'arpaDescription',
        schema: {
          type: 'object',
          required: ['content'],
          properties: {
            content: {
              type: 'string'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
