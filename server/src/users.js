import { responseType } from '../utils/responsesTemplate';
import User from '../models/user';

export default () => {
  const operations = {
    GET
  };
  async function GET(req, res) {
    let { limit, page, search } = req.query;
    if (!limit) limit = 25;
    if (!page) page = 1;
    const options = {
      limit,
      page,
      select: '-password',
      populate: [
        {
          path: 'roles'
        },
        {
          path: 'spokenLanguages'
        }
      ]
    };
    const docs = await User.paginate(
      {
        ...(search
          ? {
              $or: [
                { firstname: { $regex: search, $options: 'i' } },
                { lastname: { $regex: search, $options: 'i' } },
                { email: { $regex: search, $options: 'i' } }
              ]
            }
          : {})
      },
      options
    );
    if (!docs) {
      return res.status(204).json(responseType.error(14020, 'Users not found'));
    }
    return res
      .status(200)
      .json(responseType.success(11021, 'Users successfully found', docs));
  }
  operations.GET.apiDoc = {
    tags: ['User'],
    summary: 'Get all users',
    description: 'Only available for super admin & admin',
    security: [
      {
        Bearer: ['super_admin', 'admin']
      }
    ],
    parameters: [
      {
        name: 'page',
        in: 'query',
        required: false,
        type: 'integer',
        default: 1
      },
      {
        name: 'limit',
        in: 'query',
        required: false,
        type: 'integer',
        default: 25
      },
      {
        name: 'search',
        in: 'query',
        required: false,
        type: 'string'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
