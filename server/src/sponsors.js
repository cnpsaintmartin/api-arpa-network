import { responseType } from '../utils/responsesTemplate';
import Sponsor from '../models/sponsor';
export default () => {
  const operations = {
    GET
  };
  async function GET(req, res) {
    const docs = await Sponsor.find({});
    if (docs.length === 0) {
      return res
        .status(204)
        .json(responseType.error(44002, 'Sponsor not found'));
    }
    return res
      .status(200)
      .json(
        responseType.success(41003, 'Sponsors successfully found', { docs })
      );
  }
  operations.GET.apiDoc = {
    tags: ['Sponsor'],
    security: [],
    summary: 'Get all sponsors',
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
