import streamifier from 'streamifier';
// import ternaryStream from 'ternary-stream';
import mongoose from 'mongoose';
// import sharp from 'sharp';
import { responseType } from '../utils/responsesTemplate';
import logger from '../services/logger/logger';
import { grid } from '../models';

const slugify = require('@sindresorhus/slugify');

export default () => {
  const operations = {
    POST
  };
  async function POST(req, res) {
    const { file } = req.body;
    const isPdf = file.mimetype === 'application/pdf';
    const idFile = new mongoose.mongo.ObjectID();
    const stream = grid.openUploadStreamWithId(idFile, {
      filename: slugify(file.originalname),
      content_type: isPdf ? 'application/pdf' : 'image/jpeg'
    });
    streamifier
      .createReadStream(file.buffer)
      .on('end', function() {
        // noinspection JSAnnotator
        const docs = {
          _id: idFile,
          name: stream.name,
          url: `/api/file/${idFile}`
        };
        return res.status(200).json(
          responseType.success(61002, 'File successfully upload', {
            docs
          })
        );
      })
      .on('Error', error => {
        logger.error(error);
        console.log(error);
        return res
          .status(500)
          .json(
            responseType.error(
              64003,
              'An error occurred during the file upload'
            )
          );
      })
      .pipe(stream);
  }
  operations.POST.apiDoc = {
    tags: ['File'],
    summary: 'Upload a new file',
    security: [],
    consumes: ['multipart/form-data'],
    parameters: [
      {
        name: 'file',
        in: 'formData',
        required: true,
        type: 'file'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
