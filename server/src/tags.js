import { responseType } from '../utils/responsesTemplate';
import Tag from '../models/tag';
import logger from '../services/logger/logger';
export default () => {
  const operations = {
    GET
  };
  async function GET(req, res) {
    let { page, limit } = req.query;
    const { search } = req.query;
    if (!page) page = 1;
    if (!limit) limit = 25;
    let docs;
    const query = {
      ...(search ? { name: { $regex: '.*' + search + '.*' } } : '')
    };
    try {
      docs = await Tag.paginate(query, {
        sort: { numberOfUsage: 'desc' },
        limit,
        page
      });
      if (docs.length === 0) {
        return res
          .status(204)
          .json(responseType.error(24017, 'Tags not found'));
      }
      return res
        .status(200)
        .json(responseType.success(21004, 'Tags successfully found', docs));
    } catch (e) {
      logger.error(e);
    }
  }
  operations.GET.apiDoc = {
    tags: ['Resource'],
    security: [],
    summary: "Get all resource's tags",
    parameters: [
      {
        name: 'page',
        in: 'query',
        required: false,
        type: 'integer',
        default: 1
      },
      {
        name: 'limit',
        in: 'query',
        required: false,
        type: 'integer',
        default: 25
      },
      {
        name: 'search',
        in: 'query',
        required: false,
        type: 'string'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
