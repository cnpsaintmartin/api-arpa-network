import moment from 'moment';
import { responseType } from '../../../utils/responsesTemplate';
import Notification from '../../../models/notification';
import Role from '../../../models/role';
import User from '../../../models/user';
import logger from '../../../services/logger/logger';
export default () => {
  const operations = {
    POST
  };
  async function POST(req, res) {
    const { id } = req.params;
    const { accept } = req.body;
    const memberRequest = await Notification.findById(id);
    if (!memberRequest) {
      return res
        .status(404)
        .json(responseType.error(74020, 'Committee member request not found'));
    }
    // if the user already vote
    if (
      memberRequest.agreeMemberRequest
        .map(String)
        .includes(req.user._id.toString()) ||
      memberRequest.declineMemberRequest
        .map(String)
        .includes(req.user._id.toString())
    ) {
      return res
        .status(500)
        .json(
          responseType.error(
            74020,
            'You already take part of the vote for this member'
          )
        );
    }
    const result = await Notification.addVoteToCommitteeMemberRequest(
      id,
      accept,
      req.user._id
    );
    if (result === 'error') {
      return res
        .status(500)
        .json(
          responseType.error(
            74021,
            'Unable to add vote on the committee member request'
          )
        );
    }
    const tabIdGroup = await Role.getIdWithSlug([
      'committee_member',
      'native_committee_member'
    ]);
    const idUsers = await User.getUserByGroup(tabIdGroup);
    const nbrVote = result.agreeLength + result.declineLength;
    if (nbrVote >= idUsers.length / 2) {
      // the member is instantly accepted
      const user = await User.findById(memberRequest.sender);

      const roles = await Role.getIdWithSlug(['committee_member']);
      user.roles.push(roles);
      try {
        await user.save();
      } catch (e) {
        logger.error(e);
        return res
          .status(500)
          .json(responseType.error(74024, 'Unable to promote user'));
      }
      await Notification.findOne({
        _id: id
      })
        .remove()
        .exec();
      const notificationBack = new Notification({
        createdAt: moment.utc(),
        receiver: user._id,
        title: 'Committee member request accepted',
        message:
          'Your request to join committee has been accepted.  You have now full access to the committee page !',
        type: 'committeeMemberAccepted',
        messageCode: '0011',
        titleCode: '0011'
      });
      notificationBack.save(err => {
        if (err)
          return res
            .status(500)
            .json(
              responseType.error(
                74025,
                'Unable to send a notification but the member has been promoted'
              )
            );
        return res
          .status(200)
          .json(
            responseType.error(
              71022,
              'Thank you for your vote, the member has been promoted. A notification has been send'
            )
          );
      });
    } else {
      return res
        .status(200)
        .json(
          responseType.error(
            71023,
            'Thank you for your vote, you can find the current vote here',
            { docs: result }
          )
        );
    }
  }
  operations.POST.apiDoc = {
    tags: ['User'],
    summary: 'Accept or reject one committee member request',
    security: [
      {
        Bearer: ['native_committee_member', 'committee_member']
      }
    ],
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      },
      {
        in: 'body',
        name: 'committee member request',
        schema: {
          required: ['accept'],
          type: 'object',
          properties: {
            accept: {
              type: 'boolean',
              default: true
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
