import mailer from '../services/mailer';
import User from '../models/user';
import { responseType } from '../utils/responsesTemplate';
import Role from '../models/role';
import logger from '../services/logger/logger';

export default () => {
  const operations = {
    POST
  };
  async function POST(req, res) {
    const { content, group, subject, template } = req.body;

    let users;
    if (group) {
      const roles = await Role.getIdWithSlug(group);
      users = User.find({
        roles: {
          $in: roles
        }
      });
    } else {
      users = await User.find({});
    }

    try {
      await Promise.all(
        users.map(({ email }) => mailer.send(email, subject, content, template))
      );
      return res
        .status(200)
        .json(responseType.success(21050, 'Emails successfully sent'));
    } catch (e) {
      logger.error(e);
      return res
        .status(500)
        .json(responseType.error(24000, 'Internal Server Error'));
    }
  }

  operations.POST.apiDoc = {
    tags: ['Mail'],
    summary: 'Send a mail',
    security: [
      {
        Bearer: ['super_admin']
      }
    ],
    parameters: [
      {
        required: true,
        in: 'body',
        name: 'resource',
        schema: {
          $ref: '#/definitions/Mail'
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
