import moment from 'moment';

import { responseType } from '../../utils/responsesTemplate';
import Resource from '../../models/resource';
import Tag from '../../models/tag';
import { generateTabTags } from '../resource';
import Like from '../../models/like';
import { useCookie } from '../../utils/useCookie';
import SurveyChoice from '../../models/surveyChoice';
import Survey from '../../models/survey';
import { countTime } from '../../utils/countWord';
import User from '../../models/user';
import Notification from '../../models/notification';
import { isAuthorized } from '../../utils/authorization';
import mongoose from 'mongoose';
import _ from 'lodash';
const Schema = mongoose.Schema;
const ModelFile = mongoose.model('fs.files', new Schema({}));
export default () => {
  const operations = {
    PUT,
    DELETE,
    GET
  };
  async function PUT(req, res) {
    const { id } = req.params;
    const {
      title,
      idCategory,
      tags,
      mainContent,
      image,
      idLanguage,
      commentIsAuthorize,
      startDate,
      endDate,
      attachments
    } = req.body;
    const resource = await Resource.findById(id);
    if (!resource) {
      return res
        .status(404)
        .json(
          responseType.error(24062, 'Resource not found, it can be deleted')
        );
    }
    const authorized = await isAuthorized(
      ['admin', 'super_admin', 'committee_member', 'native_committee_member'],
      req.user.roles
    );
    if (!authorized && req.user._id.toString() === resource.author.toString()) {
      return res
        .status(401)
        .json(responseType.error(24047, 'Access forbidden'));
    }
    if (resource.tags && resource.tags.length !== 0) {
      await Promise.all(
        resource.tags.map(el => {
          Tag.removeUsage(el);
        })
      );
    }
    let tagsTmp;
    if (tags && tags.length !== 0) {
      tagsTmp = await generateTabTags(tags);
    }

    // if it's the first publication of this resource
    if (resource.resourceStatus === 1 || resource.resourceStatus === 2) {
      resource.set({
        ...(title ? { title } : {}),
        ...(idCategory ? { category: idCategory } : {}),
        ...(tagsTmp ? { tags: tagsTmp } : {}),
        ...(mainContent
          ? {
              mainContent,
              shortDescription: _.truncate(
                mainContent.replace(/<[^>]*>?/gm, ''),
                {
                  length: 150
                }
              ),
              estimateReadingTime: countTime(mainContent)
            }
          : {}),
        ...(idLanguage ? { language: idLanguage } : {}),
        ...(image ? { image } : {}),
        ...(commentIsAuthorize
          ? { commentIsAuthorize }
          : { commentIsAuthorize: false }),
        ...(startDate ? { startDate } : {}),
        ...(endDate ? { endDate } : {}),
        resourceStatus: 1,
        submittedDate: new Date(),
        attachments
      });
    } else {
      resource.set({
        resourceStatus: 4,
        resourceTmp: {
          ...(title ? { title } : {}),
          ...(idCategory ? { category: idCategory } : {}),
          ...(tagsTmp ? { tags: tagsTmp } : {}),
          ...(mainContent
            ? {
                mainContent,
                shortDescription: _.truncate(
                  mainContent.replace(/<[^>]*>?/gm, ''),
                  {
                    length: 150
                  }
                ),
                estimateReadingTime: countTime(mainContent)
              }
            : {}),
          ...(image ? { image } : {}),
          ...(idLanguage ? { language: idLanguage } : {}),
          ...(commentIsAuthorize
            ? { commentIsAuthorize }
            : { commentIsAuthorize: false }),
          ...(startDate ? { startDate } : {}),
          ...(endDate ? { endDate } : {}),
          attachments
        }
      });
    }
    resource.save((err, updatedResource) => {
      if (err) {
        return res
          .status(500)
          .json(responseType.error(24011, 'Unable to update resource'));
      }
      return res.status(200).json(
        responseType.success(
          21012,
          'Resource successfully updated, waiting for committee validation',
          {
            docs: updatedResource
          }
        )
      );
    });
  }
  async function DELETE(req, res) {
    const { id } = req.params;
    const resource = await Resource.findById(id);
    const authorized = await isAuthorized(
      ['admin', 'super_admin', 'committee_member', 'native_committee_member'],
      req.user.roles
    );
    if (!authorized && req.user._id.toString() !== resource.author.toString()) {
      return res
        .status(401)
        .json(responseType.error(24047, 'Access forbidden'));
    }
    const result = await Resource.hide(id);
    await Notification.deleteOneNotification(
      resource._id,
      null,
      'resourcePublishRequest'
    );
    if (result === 'error')
      return res
        .status(500)
        .json(responseType.error(24007, 'Unable to delete resource'));
    return res
      .status(200)
      .json(responseType.success(21008, 'Resource successfully deleted'));
  }
  async function GET(req, res) {
    const { id } = req.params;
    const { edit } = req.query;

    let docs = await Resource.findOne({
      ...(id.match(/^[0-9a-fA-F]{24}$/) ? { _id: id } : { slug: id }),
      isDraft: false
    })
      .populate({
        path: 'comments',
        populate: {
          path: 'author',
          select: ['firstname', 'lastname']
        },
        options: { sort: { submittedDate: -1 } }
      })
      .populate({
        path: 'author',
        select: [
          'firstname',
          'lastname',
          '_id',
          'isAgreeWithContactRequest',
          'roles'
        ]
      })
      .populate({
        path: 'survey',
        select: '-vote',
        populate: {
          path: 'choices'
        }
      })
      .populate({
        path: 'category'
      })
      .populate({
        path: 'tags'
      });
    if (!docs) {
      return res
        .status(404)
        .json(responseType.error(24010, 'Resource not found'));
    }
    await useCookie(req);
    docs = docs.toObject();
    let isInHisNetwork = false;
    let contactRequestAlreadySent = false;
    if (edit) {
      const idFile = docs.image.substring(docs.image.lastIndexOf('/') + 1);
      docs.fileInfos = await ModelFile.findOne({ _id: idFile });
    }
    if (req.user) {
      const result = await Like.find({
        resource: docs._id,
        author: req.user._id
      }).select('resource');
      if (result.length > 0) docs.isLikeByCurrentUser = true;
      const isAProfessional = await isAuthorized(
        ['professional'],
        req.user.roles
      );
      if (isAProfessional) {
        isInHisNetwork = await User.isInMyNetwork(
          req.user._id.toString(),
          docs.author._id.toString()
        );
        if (isInHisNetwork) docs.author.isAlreadyInMyNetwork = true;
        contactRequestAlreadySent = await Notification.checkIfContactRequestAlreadySent(
          docs.author._id,
          req.user._id
        );
        if (contactRequestAlreadySent) {
          docs.author.contactRequestAlreadySent = true;
        }
      }
    }
    if (docs.resourceType === 2) {
      // check which answer like the user
      if (req.user) {
        const choicesLike = await Survey.find({
          vote: req.user._id,
          _id: docs.survey._id
        }).select('_id');
        docs.survey.isVoteByCurrentUser = choicesLike.length > 0;
      }
      //calcul of the vote pourcentage for each answers
      let totalVote = 0;
      docs.survey.choices.map(el => {
        if (el.numberOfVote) {
          totalVote = totalVote + el.numberOfVote;
        }
      });
      docs.survey.totalVote = totalVote;
      for (let i = 0; i < docs.survey.choices.length; i++) {
        if (totalVote !== 0 && docs.survey.choices[i].numberOfVote) {
          docs.survey.choices[i].percentage = (
            (docs.survey.choices[i].numberOfVote / totalVote) *
            100
          ).toFixed(2);
        } else {
          docs.survey.choices[i].percentage = 0;
        }
      }
    }
    return res.status(200).json(
      responseType.success(21003, 'Resources successfully found', {
        docs
      })
    );
  }
  operations.GET.apiDoc = {
    tags: ['Resource'],
    security: [],
    summary: 'Get one resource with it ID',
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      },
      {
        name: 'edit',
        in: 'query',
        default: false,
        type: 'boolean'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  operations.PUT.apiDoc = {
    tags: ['Resource'],
    summary: 'Update one resource with it ID',
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      },
      {
        in: 'body',
        name: 'resource',
        schema: {
          type: 'object',
          properties: {
            title: {
              type: 'string'
            },
            idCategory: {
              type: 'string'
            },
            tags: {
              type: 'array',
              items: {
                type: 'string'
              }
            },
            mainContent: {
              type: 'string'
            },
            image: {
              type: 'string'
            },
            idLanguage: {
              type: 'string'
            },
            commentIsAuthorize: {
              type: 'boolean',
              default: true
            },
            startDate: {
              type: 'string'
            },
            endDate: {
              type: 'string'
            },
            attachments: {
              type: 'array'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  operations.DELETE.apiDoc = {
    tags: ['Resource'],
    summary: 'Delete one resource with it ID',
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
