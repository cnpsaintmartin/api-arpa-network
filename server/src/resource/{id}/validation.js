import moment from 'moment';
import { responseType } from '../../../utils/responsesTemplate';
import Resource from '../../../models/resource';
import Notification from '../../../models/notification';
import { useCookie } from '../../../utils/useCookie';
import { countTime } from '../../../utils/countWord';
import logger from '../../../services/logger/logger';
import { isAuthorized } from '../../../utils/authorization';
export default () => {
  const operations = {
    POST
  };
  async function POST(req, res) {
    const { id } = req.params;
    const { accept, reject, rejectMessage } = req.body;
    const resource = await Resource.findById(id);
    const authorId = resource.author;
    const authorized = await isAuthorized(
      ['admin', 'super_admin'],
      req.user.roles
    );
    if (!authorized && authorId.toString() === req.user._id.toString()) {
      return res
        .status(401)
        .json(
          responseType.error(24067, "You can't accept or reject your resource")
        );
    }
    if (accept) {
      if (resource.resourceStatus === 4 || resource.resourceStatus === 5) {
        resource.set({
          ...(resource.resourceTmp.title
            ? { title: resource.resourceTmp.title }
            : {}),
          ...(resource.resourceTmp.category
            ? { category: resource.resourceTmp.category }
            : {}),
          ...(resource.resourceTmp.tags
            ? { tags: resource.resourceTmp.tags }
            : {}),
          ...(resource.resourceTmp.shortDescription
            ? { shortDescription: resource.resourceTmp.shortDescription }
            : {}),
          ...(resource.resourceTmp.mainContent
            ? {
                mainContent: resource.resourceTmp.mainContent,
                estimateReadingTime: countTime(resource.resourceTmp.mainContent)
              }
            : {}),
          ...(resource.resourceTmp.language
            ? { language: resource.resourceTmp.language }
            : {}),
          ...(resource.resourceTmp.image
            ? { image: resource.resourceTmp.image }
            : {}),
          ...(resource.resourceTmp.commentIsAuthorize
            ? { commentIsAuthorize: resource.resourceTmp.commentIsAuthorize }
            : { commentIsAuthorize: false }),
          ...(resource.resourceTmp.startDate
            ? { startDate: resource.resourceTmp.startDate }
            : {}),
          ...(resource.resourceTmp.endDate
            ? { endDate: resource.resourceTmp.endDate }
            : {}),
          resourceStatus: 3
        });
        resource.resourceTmp = undefined;
        resource.save(err => {
          logger.error(err);
          if (err) {
            return res
              .status(500)
              .json(responseType.error(24036, 'Unable to accept the resource'));
          }
        });
      } else {
        if (resource.resourceType === 2) {
          const result = await Resource.surveyAddOneAgree(
            resource._id,
            req.user._id.toString()
          );
          if (result === 'error') {
            return res
              .status(500)
              .json(responseType.error(24032, 'Unable to accept the resource'));
          }
          if (result === 'needMoreValidation') {
            return res
              .status(200)
              .json(
                responseType.error(
                  21068,
                  'Vote successfully registered, we will wait until other committee member accept this survey'
                )
              );
          }
          if (result === 'alreadyAccepted') {
            return res
              .status(204)
              .json(
                responseType.error(
                  21069,
                  'You have already accepted this resource'
                )
              );
          }
        }
        const result = await Resource.validation(id, 3);
        if (result === 'error') {
          return res
            .status(500)
            .json(responseType.error(24032, 'Unable to accept the resource'));
        }
      }
      await useCookie(req);
      const notification = new Notification({
        createdAt: moment.utc(),
        receiver: authorId,
        title: 'Resource published',
        message:
          'Congratulation, your resource has been published successfully!',
        messageCode: '0001',
        titleCode: '0001',
        type: 'resourcePublished'
      });
      try {
        const p1 = Notification.deleteOneNotification(
          resource._id,
          null,
          'resourcePublishRequest'
        );
        const p2 = notification.save();
        await Promise.all([p1, p2]);
      } catch (e) {
        logger.error(e);
      }
      return res
        .status(200)
        .json(responseType.success(21034, 'Resource successfully accepted'));
    } else if (reject) {
      if (resource.resourceStatus === 4 || resource.resourceStatus === 5) {
        resource.set({
          resourceStatus: 5,
          rejectMessage
        });
        resource.save(err => {
          if (err) {
            logger.error(err);
            return res
              .status(500)
              .json(responseType.error(24037, 'Unable to reject the resource'));
          }
        });
      } else {
        const result = await Resource.validation(id, 2);
        if (result === 'error') {
          return res
            .status(500)
            .json(responseType.error(24033, 'Unable to reject the resource'));
        }
      }
      const notification = new Notification({
        createdAt: moment.utc(),
        receiver: authorId,
        title: 'Resource rejected',
        message: rejectMessage,
        messageCode: '0002',
        titleCode: '0000',
        type: 'resourceRejected'
      });
      try {
        const p1 = Notification.deleteOneNotification(
          resource._id,
          null,
          'resourcePublishRequest'
        );
        const p2 = notification.save();
        await Promise.all([p1, p2]);
      } catch (e) {
        logger.error(e);
      }
      return res
        .status(200)
        .json(responseType.success(21035, 'Resource successfully rejected'));
    }
  }
  operations.POST.apiDoc = {
    tags: ['Resource'],
    summary: 'Accept or reject one resource',
    security: [
      {
        Bearer: ['native_committee_member', 'committee_member']
      }
    ],
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      },
      {
        in: 'body',
        name: 'project report',
        schema: {
          required: ['accept', 'reject'],
          type: 'object',
          properties: {
            accept: {
              type: 'boolean',
              default: true
            },
            reject: {
              type: 'boolean',
              default: false
            },
            rejectMessage: {
              type: 'string'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
