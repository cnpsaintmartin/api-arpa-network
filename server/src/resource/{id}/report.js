import moment from 'moment';
import { responseType } from '../../../utils/responsesTemplate';
import Resource from '../../../models/resource';
import User from '../../../models/user';
import Role from '../../../models/role';
import Notification from '../../../models/notification';
import logger from '../../../services/logger/logger';
export default () => {
  const operations = {
    POST,
    DELETE
  };
  async function POST(req, res) {
    const { id } = req.params;
    const { message } = req.body;
    const result = await Resource.report(id, req.user._id);
    if (result === 'sameAuthor') {
      return res
        .status(500)
        .json(responseType.error(24052, "You can't report your resource"));
    }
    if (result === 'error') {
      return res
        .status(500)
        .json(responseType.error(24038, 'Unable to report this resource'));
    }
    if (result === 'notFound') {
      return res
        .status(404)
        .json(responseType.error(24042, 'Unable to find this resource'));
    }
    if (result === 'alreadyReport') {
      return res
        .status(500)
        .json(responseType.error(24043, 'You already report this resource'));
    }
    const tabIdGroup = await Role.getIdWithSlug([
      'committee_member',
      'native_committee_member'
    ]);
    const idUsers = await User.getUserByGroup(tabIdGroup);
    const newNotification = new Notification({
      createdAt: moment.utc(),
      sender: req.user._id,
      receiver: idUsers,
      title: 'One resource has been reported',
      message,
      displayReadBy: true,
      titleCode: '0005',
      messageCode: '0000',
      type: 'resourceReported',
      resource: id
    });
    try {
      await newNotification.save();
    } catch (e) {
      logger.error(e);
    }
    return res
      .status(200)
      .json(responseType.success(21039, 'Report successfully added'));
  }
  async function DELETE(req, res) {
    const { id } = req.params;
    const result = await Resource.removeReport(id, req.user._id);
    if (result === 'error') {
      return res
        .status(500)
        .json(
          responseType.error(24040, 'Unable to remove report to this resource')
        );
    } else if (result === 'notFound') {
      return res
        .status(404)
        .json(responseType.error(24042, 'Unable to find this resource'));
    }
    return res
      .status(200)
      .json(responseType.success(21041, 'Report successfully removed'));
  }
  operations.POST.apiDoc = {
    tags: ['Resource'],
    summary: 'Report an existing resource',
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      },
      {
        in: 'body',
        name: 'report message request',
        schema: {
          type: 'object',
          required: ['message'],
          properties: {
            message: {
              description: 'Explain why you report this resource',
              type: 'string'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  operations.DELETE.apiDoc = {
    tags: ['Resource'],
    summary: 'Remove report to an existing resource',
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
