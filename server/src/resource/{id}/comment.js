import { responseType } from '../../../utils/responsesTemplate';
import Resource from '../../../models/resource';
import Comment from '../../../models/comment';
import logger from '../../../services/logger/logger';

export default () => {
  const operations = {
    POST
  };
  async function POST(req, res) {
    const { text } = req.body;
    const { id } = req.params;
    if (req.user.status && req.user.status.includes('cantComment')) {
      return res
        .status(401)
        .json(responseType.error(24047, 'Access forbidden'));
    }
    let resource = null;
    try {
      resource = await Resource.findById(id);
      if (!resource)
        return res
          .status(404)
          .json(responseType.error(24010, 'Resource not found'));
    } catch (e) {
      logger.error(e);
      return res
        .status(500)
        .json(responseType.error(24000, 'Internal Server Error'));
    }
    if (!resource.commentIsAuthorize)
      return res
        .status(500)
        .json(
          responseType.error(24009, "Comment isn't authorize for this resource")
        );
    const newComment = await new Comment({
      text,
      author: req.user._id,
      resource: id
    }).save();
    const result = await Resource.comment(resource._id, newComment._id);
    if (result === 'error')
      return res
        .status(500)
        .json(
          responseType.error(24014, 'Unable to add comment on the resource')
        );
    return res.status(200).json(
      responseType.success(
        21013,
        'Comment successfully added, waiting for committee validation',
        {
          docs: {
            _id: newComment._id,
            text: newComment.text,
            submittedDate: newComment.submittedDate
          }
        }
      )
    );
  }
  operations.POST.apiDoc = {
    tags: ['Resource'],
    summary: 'Add one comment to an existing resource',
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      },
      {
        required: true,
        in: 'body',
        name: 'user',
        schema: {
          properties: {
            text: {
              type: 'string'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
