import { responseType } from '../../../utils/responsesTemplate';
import Resource from '../../../models/resource';
import Like from '../../../models/like';

export default () => {
  const operations = {
    POST,
    DELETE
  };
  async function POST(req, res) {
    const { id } = req.params;
    const result = await Resource.like(id);
    if (result === 'error') {
      await Resource.unlike(id);
      return res
        .status(500)
        .json(responseType.error(24019, 'Unable to add like on the resource'));
    }
    await new Like({
      author: req.user._id,
      resource: id
    }).save(async err => {
      if (err) {
        await Resource.unlike(id);
        if (err.code === 11000) {
          return res
            .status(500)
            .json(
              responseType.error(
                24021,
                "You can't like the same article two time"
              )
            );
        }
        return res
          .status(500)
          .json(
            responseType.error(24019, 'Unable to add like on the resource')
          );
      }
      return res
        .status(200)
        .json(responseType.success(21020, 'Like successfully added'));
    });
  }
  async function DELETE(req, res) {
    const { id } = req.params;
    const removeLike = await Like.deleteOne({
      author: req.user._id,
      resource: id
    });
    if (removeLike.ok === 1 && removeLike.n !== 0) {
      await Resource.unlike(id);
      return res
        .status(200)
        .json(responseType.success(21023, 'Like successfully deleted'));
    } else if (removeLike.ok === 1 && removeLike.n === 0) {
      return res
        .status(200)
        .json(responseType.success(21022, 'There is nothing to delete'));
    } else {
      return res
        .status(200)
        .json(responseType.error(24000, 'Internal Server Error'));
    }
  }
  operations.POST.apiDoc = {
    tags: ['Resource'],
    summary: 'Add one like to an existing resource',
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  operations.DELETE.apiDoc = {
    tags: ['Resource'],
    summary: 'Remove one like to an existing resource',
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
