import moment from 'moment';
import { responseType } from '../../../utils/responsesTemplate';
import Resource from '../../../models/resource';
import Notification from '../../../models/notification';
import logger from '../../../services/logger/logger';
export default () => {
  const operations = {
    POST
  };
  async function POST(req, res) {
    const { id } = req.params;
    const { accept, reject, message } = req.body;
    const resource = await Resource.findById(id);
    if (
      !resource.isReportedBy ||
      (resource.isReportedBy && resource.isReportedBy.length === 0)
    )
      return res
        .status(500)
        .json(
          responseType.error(24044, 'There is no report to accept or reject')
        );

    if (accept) {
      const authorId = resource.author;
      const result = await Resource.hide(resource._id);
      if (result === 'error')
        return res
          .status(500)
          .json(responseType.error(24007, 'Unable to delete resource'));
      const notificationAuthor = new Notification({
        createdAt: moment.utc(),
        receiver: authorId,
        title: 'Your resource has been removed',
        message,
        displayReadBy: false,
        titleCode: '0006',
        messageCode: '0000',
        type: 'resourceDeleted'
      });
      const notificationReporter = new Notification({
        createdAt: moment.utc(),
        receiver: resource.isReportedBy,
        title: 'Your report has been accepted',
        message: 'Thank you for your report, the resource has been removed !',
        displayReadBy: false,
        titleCode: '0007',
        messageCode: '0007',
        type: 'reportAccepted'
      });
      try {
        const promise = Notification.deleteOneNotification(
          resource._id,
          null,
          'resourceReported'
        );
        const notification1 = notificationAuthor.save();
        const notification2 = notificationReporter.save();
        await Promise.all([notification1, notification2, promise]);
      } catch (e) {
        logger.error(e);
      }
      return res
        .status(200)
        .json(
          responseType.success(
            21008,
            'Resource successfully removed, a notification has been send to the author and reporter'
          )
        );
    } else if (reject) {
      const notificationReporter = new Notification({
        createdAt: moment.utc(),
        receiver: resource.isReportedBy,
        title: 'Your report has been rejected',
        message:
          'Thank you for your report, but the resource respect all our rules.',
        displayReadBy: false,
        titleCode: '0008',
        messageCode: '0008',
        type: 'reportRejected'
      });
      try {
        const p1 = Notification.deleteOneNotification(
          resource._id,
          null,
          'resourceReported'
        );
        const p2 = notificationReporter.save();
        await Promise.all([p1, p2]);
      } catch (e) {
        logger.error(e);
      }
      resource.isReportedBy = undefined;
      resource.save(err => {
        if (err) {
          return res
            .status(500)
            .json(responseType.error(24045, 'Unable to reject the report'));
        }
      });
      return res
        .status(200)
        .json(
          responseType.error(21046, 'The report has been rejected successfully')
        );
    }
  }
  operations.POST.apiDoc = {
    tags: ['Resource'],
    summary: 'Accept or reject one report',
    security: [
      {
        Bearer: ['native_committee_member', 'committee_member']
      }
    ],
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      },
      {
        in: 'body',
        name: 'report resource',
        schema: {
          required: ['accept', 'reject'],
          type: 'object',
          properties: {
            accept: {
              type: 'boolean',
              default: true
            },
            reject: {
              type: 'boolean',
              default: false
            },
            acceptMessage: {
              type: 'string',
              description:
                'this message will send to the author to explain why his resource has been deleted'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
