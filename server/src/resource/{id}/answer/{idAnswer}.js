import { responseType } from '../../../../utils/responsesTemplate';
import Resource from '../../../../models/resource';
import SurveyChoice from '../../../../models/surveyChoice';
import Survey from '../../../../models/survey';
import moment from 'moment';

export default () => {
  const operations = {
    POST
  };
  async function POST(req, res) {
    const { id, idAnswer } = req.params;
    const survey = await Resource.findOne({ _id: id })
      .populate({
        path: 'survey',
        populate: {
          path: 'choices',
          select: '-vote'
        }
      })
      .lean();
    if (survey.resourceType !== 2) {
      return res
        .status(500)
        .json(
          responseType.error(
            24025,
            'You must apply a vote to one survey (not available for article or event)'
          )
        );
    }
    if (!survey.publishedDate) {
      return res
        .status(500)
        .json(
          responseType.error(
            24051,
            'The survey must be published before you can vote'
          )
        );
    }
    if (moment(survey.endDate).isBefore(moment())) {
      return res
        .status(500)
        .json(responseType.error(24026, 'This survey was closed'));
    }
    const vote = await Survey.vote(survey.survey, req.user._id);
    console.log(vote);
    if (vote === 'error' || vote === 'two_votes' || vote === 'notFound') {
      return res
        .status(500)
        .json(responseType.error(24027, 'Unable to add vote on the survey'));
    }
    const result = await SurveyChoice.vote(idAnswer);
    if (result === 'error') {
      return res
        .status(500)
        .json(responseType.error(24027, 'Unable to add vote on the survey'));
    }
    if (result === 'two_votes') {
      return res
        .status(500)
        .json(
          responseType.error(
            24028,
            "You can't apply two times at the same survey"
          )
        );
    }
    // get of idAnswers
    const idAnswers = survey.survey.choices.map(el => el._id);
    // check which answer like the user
    const choicesLikeByUser = await SurveyChoice.find({
      vote: req.user._id,
      _id: { $in: idAnswers }
    }).select('_id');
    if (choicesLikeByUser.length !== 0) {
      choicesLikeByUser.map(el => {
        const result = survey.survey.choices.findIndex(
          choice => choice._id.toString() === el._id.toString()
        );
        //add indicator if the connected user like the answer
        survey.survey.choices[result].isVoteByCurrentUser = true;
        survey.survey.choices[result].numberOfVote =
          survey.survey.choices[result].numberOfVote + 1;
      });
    }
    //calcul of the vote pourcentage for each answers
    let totalVote = 0;
    survey.survey.choices.map(el => {
      if (el.numberOfVote) {
        totalVote = totalVote + el.numberOfVote;
      }
    });
    survey.survey.totalVote = totalVote;
    for (let i = 0; i < survey.survey.choices.length; i++) {
      if (totalVote !== 0 && survey.survey.choices[i].numberOfVote) {
        survey.survey.choices[i].percentage = (
          (survey.survey.choices[i].numberOfVote / totalVote) *
          100
        ).toFixed(2);
      } else {
        survey.survey.choices[i].percentage = 0;
      }
    }
    return res.status(200).json(
      responseType.success(21029, 'Vote successfully registred', {
        docs: survey.survey.choices
      })
    );
  }
  operations.POST.apiDoc = {
    tags: ['Resource'],
    summary: 'Add one reply to an existing survey',
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      },
      {
        name: 'idAnswer',
        in: 'path',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
