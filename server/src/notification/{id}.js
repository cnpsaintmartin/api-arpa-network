import { responseType } from '../../utils/responsesTemplate';
import Notification from '../../models/notification';
export default () => {
  const operations = {
    DELETE
  };
  async function DELETE(req, res) {
    const { id } = req.params;
    const result = await Notification.deleteMyNotification(id, req.user._id);
    if (result === 'error')
      return res
        .status(500)
        .json(responseType.error(74011, 'Unable to delete notification'));
    if (result === 'nothingToDelete') {
      return res
        .status(500)
        .json(responseType.error(74012, 'There is no notification to delete'));
    }
    return res
      .status(200)
      .json(responseType.success(71013, 'Notification successfully deleted'));
  }
  operations.DELETE.apiDoc = {
    tags: ['Notification'],
    summary: 'Delete one notification with it ID',
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
