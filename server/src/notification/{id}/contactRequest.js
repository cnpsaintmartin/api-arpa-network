import moment from 'moment';
import { responseType } from '../../../utils/responsesTemplate';
import Notification from '../../../models/notification';
import User from '../../../models/user';
import logger from '../../../services/logger/logger';
export default () => {
  const operations = {
    POST
  };
  async function POST(req, res) {
    const { id } = req.params;
    const { accept, reject, message } = req.body;
    try {
      const notification = await Notification.findOne({ _id: id });
      if (!notification) {
        return res
          .status(404)
          .json(responseType.error(74014, 'Contact request not found'));
      }
      if (req.user._id.toString() !== notification.receiver[0].toString()) {
        return res.status(401).json(responseType.error(74002, 'Access denied'));
      }
      if (accept) {
        const promiseNewMember = User.addMemberInMyNetwork(
          notification.sender,
          req.user._id
        );
        const promiseReverseNewMember = User.addMemberInMyNetwork(
          req.user._id,
          notification.sender
        );
        const removeContactRequestFromNotification = Notification.findOne({
          _id: id
        })
          .remove()
          .exec();
        await Promise.all([
          promiseNewMember,
          promiseReverseNewMember,
          removeContactRequestFromNotification
        ]);
        const notificationBack = new Notification({
          createdAt: moment.utc(),
          sender: req.user._id,
          receiver: notification.sender,
          title: 'Welcome in my network !',
          message: 'You can contact me if you have any question !',
          type: 'contactRequestAccepted',
          messageCode: '0003',
          titleCode: '0003'
        });
        notificationBack.save(err => {
          if (err)
            return res
              .status(500)
              .json(
                responseType.error(
                  74006,
                  'Unable to send a notification but the user has been added into your network'
                )
              );
          return res
            .status(200)
            .json(
              responseType.error(71004, 'Contact request accepted successfully')
            );
        });
      } else if (reject) {
        const notificationBack = new Notification({
          createdAt: moment.utc(),
          sender: req.user._id,
          receiver: notification.sender,
          title: 'Contact request refused',
          message,
          type: 'contactRequestRefused',
          messageCode: '0000',
          titleCode: '0010'
        });
        await Notification.findOne({
          _id: id
        })
          .remove()
          .exec();
        notificationBack.save(err => {
          if (err)
            return res
              .status(500)
              .json(
                responseType.error(
                  74007,
                  'Unable to send a notification but we have refused the contact request'
                )
              );
          return res
            .status(200)
            .json(
              responseType.error(71005, 'Contact request refused successfully')
            );
        });
      } else {
        return res
          .status(500)
          .json(responseType.error(74003, 'No choice between accept / reject'));
      }
    } catch (e) {
      logger.error(e);
      return res
        .status(500)
        .json(responseType.error(74000, 'Internal Server Error'));
    }
  }
  operations.POST.apiDoc = {
    tags: ['Notification'],
    summary: 'Accept or decline contact request with it ID',
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      },
      {
        in: 'body',
        name: 'contact request',
        schema: {
          type: 'object',
          required: ['accept', 'reject'],
          properties: {
            accept: {
              type: 'boolean',
              default: true
            },
            reject: {
              type: 'boolean',
              default: false
            },
            message: {
              description: 'If you want to send a message to your new contact',
              type: 'string'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
