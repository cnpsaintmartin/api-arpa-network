import { responseType } from '../utils/responsesTemplate';
import Category from '../models/category';

export default () => {
  const operations = {
    GET
  };
  async function GET(req, res) {
    const { limit, page, isShowInHomePage } = req.query;
    const options = {
      limit,
      page
    };
    let docs = await Category.paginate(
      { ...(isShowInHomePage !== undefined ? { isShowInHomePage } : {}) },
      options
    );
    docs.docs = docs.docs.sort(
      (a, b) => (a.number > b.number ? 1 : b.number > a.number ? -1 : 0)
    );
    if (!docs) {
      return res
        .status(204)
        .json(responseType.error(24015, 'Categories not found'));
    }
    return res
      .status(200)
      .json(responseType.success(21001, 'Categories successfully found', docs));
  }
  operations.GET.apiDoc = {
    tags: ['Resource'],
    security: [],
    summary: 'Get available categories',
    parameters: [
      {
        name: 'page',
        in: 'query',
        required: false,
        type: 'integer',
        default: 1
      },
      {
        name: 'limit',
        in: 'query',
        required: false,
        type: 'integer',
        default: 10
      },
      {
        name: 'isShowInHomePage',
        in: 'query',
        required: false,
        type: 'boolean'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
