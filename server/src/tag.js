import { responseType } from '../utils/responsesTemplate';
import Tag from '../models/tag';
import logger from '../services/logger/logger';

export default () => {
  const operations = {
    POST
  };
  async function POST(req, res) {
    const { name } = req.body;
    let newTag = new Tag({
      name: name.toLowerCase()
    });
    newTag
      .save()
      .then(docs => {
        return res.status(200).json(
          responseType.success(21005, 'Tags successfully added', {
            docs
          })
        );
      })
      .catch(e => {
        logger.error(e);
        if (e.errors.name)
          return res
            .status(500)
            .json(responseType.error(24006, 'This tag is already register'));
        else {
          return res
            .status(500)
            .json(responseType.error(24000, 'Internal Server Error'));
        }
      });
  }
  operations.POST.apiDoc = {
    tags: ['Resource'],
    summary: 'Add a new tag',
    parameters: [
      {
        required: true,
        in: 'body',
        name: 'tag',
        schema: {
          type: 'object',
          required: ['name'],
          properties: {
            name: {
              type: 'string'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
