import { responseType } from '../utils/responsesTemplate';
import AuthorCodeConduct from '../models/authorCodeConduct';
export default () => {
  const operations = {
    PUT,
    GET
  };
  async function PUT(req, res) {
    const { content } = req.body;
    const authorCodeConduct = await AuthorCodeConduct.findOne({});
    if (authorCodeConduct) {
      authorCodeConduct.set({
        content
      });
      authorCodeConduct.save((err, updatedCodeConduct) => {
        if (err) {
          return res
            .status(500)
            .json(
              responseType.error(34019, 'Unable to update author code conduct')
            );
        }
        return res.status(200).json(
          responseType.success(
            31020,
            'Author code conduct successfully updated',
            {
              docs: updatedCodeConduct
            }
          )
        );
      });
    } else {
      const newAuthorCodeConduct = new AuthorCodeConduct({
        content
      });
      newAuthorCodeConduct.save((err, newAuthorCode) => {
        if (err) {
          return res
            .status(500)
            .json(
              responseType.error(34021, 'Unable to save author code conduct')
            );
        }
        return res.status(200).json(
          responseType.success(
            31022,
            'Author code conduct successfully saved',
            {
              docs: newAuthorCode
            }
          )
        );
      });
    }
  }
  async function GET(req, res) {
    const docs = await AuthorCodeConduct.findOne({});
    if (!docs) {
      return res
        .status(204)
        .json(responseType.error(34023, 'Author code conduct not found'));
    }
    return res.status(200).json(
      responseType.success(31024, 'Author code conduct successfully found', {
        docs
      })
    );
  }
  operations.GET.apiDoc = {
    tags: ['Usage information'],
    security: [],
    summary: 'Get author code conduct',
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  operations.PUT.apiDoc = {
    tags: ['Usage information'],
    summary: 'Update author code conduct',
    security: [
      {
        Bearer: ['super_admin', 'admin']
      }
    ],
    parameters: [
      {
        in: 'body',
        name: 'resource',
        schema: {
          type: 'object',
          required: ['content'],
          properties: {
            content: {
              type: 'string'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
