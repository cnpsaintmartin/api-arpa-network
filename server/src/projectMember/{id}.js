import { responseType } from '../../utils/responsesTemplate';
import ProjectMember from '../../models/projectMember';
import logger from '../../services/logger/logger';
export default () => {
  const operations = {
    PUT,
    DELETE,
    GET
  };
  async function PUT(req, res) {
    const { id } = req.params;
    const { institution, firstname, lastname, email, website } = req.body;
    ProjectMember.findById(id, (err, member) => {
      if (err) {
        return res
          .status(500)
          .json(responseType.error(54010, 'Unable to update project member'));
      }
      member.set({
        ...(institution ? { institution } : ''),
        ...(firstname ? { firstname } : ''),
        ...(lastname ? { lastname } : ''),
        ...(website ? { website } : ''),
        ...(email ? { email } : '')
      });
      member.save((err, updatedMember) => {
        if (err) {
          return res
            .status(500)
            .json(responseType.error(54011, 'Unable to update project member'));
        }
        return res.status(200).json(
          responseType.success(51012, 'Project member successfully updated', {
            docs: updatedMember
          })
        );
      });
    });
  }
  async function DELETE(req, res) {
    const { id } = req.params;
    ProjectMember.deleteOne({ _id: id }, function(err) {
      if (!err) {
        return res
          .status(200)
          .json(
            responseType.success(51013, 'Project member successfully deleted')
          );
      } else {
        return res
          .status(500)
          .json(responseType.error(54014, 'Unable to delete Project member'));
      }
    });
  }
  async function GET(req, res) {
    const { id } = req.params;
    let docs;
    try {
      docs = await ProjectMember.findOne({
        _id: id
      });
    } catch (e) {
      logger.error(e);
      if (e.kind === 'ObjectId') {
        return res
          .status(500)
          .json(
            responseType.error(
              54015,
              'One or some id you try to send is bad. Check the format of them and try again'
            )
          );
      }
    }
    if (!docs) {
      return res
        .status(404)
        .json(responseType.error(54016, 'Project member not found'));
    }
    return res.status(200).json(
      responseType.success(51017, 'Project member successfully found', {
        docs: docs.toObject()
      })
    );
  }
  operations.GET.apiDoc = {
    tags: ['ARPA Project'],
    security: [],
    summary: 'Get one project member with it ID',
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  operations.PUT.apiDoc = {
    tags: ['ARPA Project'],
    summary: 'Update one project member with it ID',
    security: [
      {
        Bearer: ['super_admin', 'admin']
      }
    ],
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      },
      {
        in: 'body',
        name: 'project member',
        schema: {
          type: 'object',
          properties: {
            institution: {
              type: 'string'
            },
            firstname: {
              type: 'string'
            },
            lastname: {
              type: 'string'
            },
            email: {
              type: 'string'
            },
            website: {
              type: 'string'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  operations.DELETE.apiDoc = {
    tags: ['ARPA Project'],
    summary: 'Delete one project member with it ID',
    security: [
      {
        Bearer: ['super_admin', 'admin']
      }
    ],
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
