import { responseType } from '../../utils/responsesTemplate';
import files from '../../models/files';
import { grid, database } from '../../models';
import mongoose from 'mongoose';

const { ObjectID } = mongoose.mongo;

export default () => {
  const operations = {
    GET
  };
  async function GET(req, res) {
    const { id } = req.params;

    const result = await database
      .collection('images.files')
      .findOne({ _id: new ObjectID(id) });

    if (!result) {
      return res.status(404).json(responseType.error(64001, 'File not found'));
    }

    const file = await grid.openDownloadStream(new mongoose.mongo.ObjectID(id));

    res.setHeader(
      'Content-Type',
      `${result.filename.content_type}; charset=utf-8`
    );
    res.setHeader(
      'Content-Disposition',
      `attachment;filename=${result.filename.filename}.${
        result.filename.content_type.split('/')[1]
      }`
    );
    file.pipe(res);
  }
  operations.GET.apiDoc = {
    tags: ['File'],
    summary: 'Get one file',
    security: [],
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
