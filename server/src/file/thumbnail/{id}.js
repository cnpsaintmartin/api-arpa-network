import streamifier from 'streamifier/lib';
import sharp from 'sharp';
import mongoose from 'mongoose';
import logger from '../../../services/logger/logger';
import { responseType } from '../../../utils/responsesTemplate';
import files from '../../../models/files';
import { database, grid } from '../../../models';

const { ObjectID } = mongoose.mongo;

export default () => {
  const operations = {
    GET
  };

  const getThumbnail = id =>
    database
      .collection('images.files')
      .findOne({ filename: `thumbnail_${id}` });

  async function GET(req, res) {
    const { id } = req.params;
    let thumbnail = await getThumbnail(id);

    if (!thumbnail) {
      const original = await database
        .collection('images.files')
        .findOne({ _id: new ObjectID(id) });
      if (!original)
        return res
          .status(404)
          .json(responseType.error(64001, 'File not found'));

      const stream = grid.openUploadStream(`thumbnail_${id}`, {
        contentType: 'image/jpeg'
      });

      /*
      * thumbnail generator
      * */
      const transformer = sharp().resize(225, 300, {
        kernel: sharp.kernel.cubic,
        fit: 'cover',
        gravity: 'center'
      });

      const uploadThumbnail = new Promise((resolve, reject) => {
        return grid
          .openDownloadStream(new ObjectID(original._id))
          .on('end', function() {
            setTimeout(() => resolve(), 200);
          })
          .on('Error', error => {
            logger.error(error);
            reject(error);
            return res
              .status(500)
              .json(
                responseType.error(
                  64003,
                  'An error occurred during the thumbnail creation'
                )
              );
          })
          .pipe(transformer)
          .pipe(stream);
      });
      await uploadThumbnail;
      thumbnail = await getThumbnail(id);
    }
    res.setHeader('Content-Type', `${thumbnail.contentType}; charset=utf-8`);
    res.setHeader(
      'Content-Disposition',
      `attachment;filename=${thumbnail.filename}.${
        thumbnail.contentType.split('/')[1]
      }`
    );
    return await grid.openDownloadStreamByName(`thumbnail_${id}`).pipe(res);
  }
  operations.GET.apiDoc = {
    tags: ['File'],
    summary: 'Get one thumbnail file',
    security: [],
    parameters: [
      {
        name: 'id',
        in: 'path',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
