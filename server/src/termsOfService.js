import { responseType } from '../utils/responsesTemplate';
import TermsOfService from '../models/termsOfService';
export default () => {
  const operations = {
    PUT,
    GET
  };
  async function PUT(req, res) {
    const { content } = req.body;
    const tos = await TermsOfService.findOne({});
    if (tos) {
      tos.set({
        content
      });
      tos.save((err, updatedTos) => {
        if (err) {
          return res
            .status(500)
            .json(
              responseType.error(34009, 'Unable to update terms of service')
            );
        }
        return res.status(200).json(
          responseType.success(31010, 'Terms of service successfully updated', {
            docs: updatedTos
          })
        );
      });
    } else {
      const newTos = new TermsOfService({
        content
      });
      newTos.save((err, newTermsOfService) => {
        if (err) {
          return res
            .status(500)
            .json(responseType.error(34012, 'Unable to save terms of service'));
        }
        return res.status(200).json(
          responseType.success(31011, 'Terms of service successfully saved', {
            docs: newTermsOfService
          })
        );
      });
    }
  }
  async function GET(req, res) {
    const docs = await TermsOfService.findOne({});
    if (!docs) {
      return res
        .status(204)
        .json(responseType.error(34007, 'Terms of service not found'));
    }
    return res.status(200).json(
      responseType.success(31008, 'Terms of service successfully found', {
        docs
      })
    );
  }
  operations.GET.apiDoc = {
    tags: ['Usage information'],
    security: [],
    summary: 'Get terms of service',
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  operations.PUT.apiDoc = {
    tags: ['Usage information'],
    summary: 'Update terms of service',
    security: [
      {
        Bearer: ['super_admin', 'admin']
      }
    ],
    parameters: [
      {
        in: 'body',
        name: 'resource',
        schema: {
          type: 'object',
          required: ['content'],
          properties: {
            content: {
              type: 'string'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
