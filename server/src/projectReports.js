import { responseType } from '../utils/responsesTemplate';
import ProjectReport from '../models/projectReport';
export default () => {
  const operations = {
    GET
  };
  async function GET(req, res) {
    const { isInNavbar } = req.query;
    const docs = await ProjectReport.find({
      ...(typeof isInNavbar === 'boolean' ? { isInNavbar } : {})
    });
    if (docs.length === 0) {
      return res
        .status(204)
        .json(responseType.error(54019, 'Project report not found'));
    }
    return res.status(200).json(
      responseType.success(51020, 'Project report successfully found', {
        docs
      })
    );
  }
  operations.GET.apiDoc = {
    tags: ['ARPA Project'],
    security: [],
    summary: 'Get all project reports',
    parameters: [
      {
        name: 'isInNavbar',
        in: 'query',
        required: false,
        type: 'boolean'
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '204': {
        description: 'Successful operation, but return docs with no content'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
