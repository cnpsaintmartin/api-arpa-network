import { responseType } from '../utils/responsesTemplate';
import Sponsor from '../models/sponsor';
import logger from '../services/logger/logger';
export default () => {
  const operations = {
    POST
  };
  async function POST(req, res) {
    const { image, name, shortDescription, website } = req.body;
    let newSponsor = new Sponsor({
      image,
      name,
      shortDescription,
      website
    });
    newSponsor
      .save()
      .then(docs => {
        return res.status(200).json(
          responseType.success(41001, 'Sponsor correctly registred', {
            docs
          })
        );
      })
      .catch(e => {
        logger.error(e);
        return res
          .status(500)
          .json(responseType.error(44000, 'Internal Server Error'));
      });
  }
  operations.POST.apiDoc = {
    tags: ['Sponsor'],
    summary: 'Add a new sponsor',
    security: [
      {
        Bearer: ['super_admin', 'admin']
      }
    ],
    parameters: [
      {
        required: true,
        in: 'body',
        name: 'sponsor',
        schema: {
          type: 'object',
          required: ['image', 'name', 'shortDescription'],
          properties: {
            image: {
              type: 'string'
            },
            name: {
              type: 'string'
            },
            shortDescription: {
              type: 'string'
            },
            website: {
              type: 'string'
            }
          }
        }
      }
    ],
    responses: {
      '200': {
        description: 'Successful operation'
      },
      '500': {
        description: 'Internal Server Error'
      }
    }
  };
  return operations;
};
